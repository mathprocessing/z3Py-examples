"""
Tutotial + tests at the same time (Thanks to creators of doctest)

Travel advisory: Hope after reading this you can become a real yachtsman... brrr... real Backtracker, SuperBacktracker, SpiderBacktracker... do you like spiders? spiderladies?
If you like spiderladies this tutorial helps you a lot.

Create instance of class Field
>>> f = Field(width=4, height=3, value=1, name='f')

What do you think about this nice shorthands?
>>> f.w, f.h
(4, 3)
>>> assert f.size == (f.w, f.h)

This not holds because of outer values
> assert f[f.h - 1, f.w - 1] == f[-1, -1]

Last value (in right-down corner)
TODO: f.corners.right_down may help users

I think might be better to use dictinct variable names like: {x, y, z}.
But now `x` is just a denotation of any variable joker '?'

>>> x = 2
>>> f.setrow([0, 1, 1, 0])
>>> f.setrow([1, x, x, x])
>>> f.setrow([0, 1, 1, 0])

To print Field you can use two options:
>>> print(f)
.oo.
o???
.oo.

or
>>> f.pp
.oo.
o???
.oo.

or
>>> f.pp2
. o o .
o ? ? ?
. o o .

>>> assert f.inside_bounds(2, 3)

TODO:
> f.pp(detailed=True)
  j= 0123
i=0: .oo.
i=1: o???
i=2: .oo.

Try to check sum around cells by yourself:
>>> [f.cell_sum_tuple(0, 1), f.cell_sum_tuple(0, 3), f.cell_sum_tuple(1, 1)]
[(1, 2, 4), (0, 1, 3), (2, 5, 6)]

We can get neighbors of cells:
>>> [list(f.get_cells_around(0, 0, inside=True)), list(f.get_cells_around(2, 3, inside=True))]
[[1, 1, 2], [2, 2, 1]]

> f.pp(detailed=True, var_labels='xyz')
  j= 0123
i=0: .oo.
i=1: oxyz
i=2: .oo.

Variable is just index of cell (index of row, index of column)
>>> x, y, z = f.unknowns; (x, y, z)
((1, 1), (1, 2), (1, 3))

Check that all cells are valid (this helps to view concrete examples of not valid cells)
>>> [(i, j) for i, j, _ in f.cells() if not f.is_valid_cell(i, j)]
[]

Another way to do same thing:
>>> f.not_valid_cells()
[]

Short version of check (True means unknown i.e. True or False, and remember you still not a Backtracker, SuperBacktracer, SpiderBacktracker, ...)
>>> f.check()
True

Just aliases for "false", "true" and "unknown":
>>> F, T, U = 0, 1, 2

`f.cuteq(index2d, value)` Returns cutted copy of the field:

Just for better understanding where the variables x, y and z inhabit:
.oo.
oxyz <<< they here
.oo.

>>> f.cuteq(x, F).get_model_status(as_str=True)
'unknown'

>>> f.cuteq(x, F).pp # if x == F we have unknown (unknown number of solutions)
.oo.
o.??
.oo.

^^^ This model is might be true, but what about `T` case?
>>> f.cuteq(x, T).get_model_status(as_str=True)
'all_false'

>>> f.cuteq(x, T).pp # if x == T we have unsat (0 solutions)
.oo.
oo??
.oo.
unsat from (0, 0) == 0 and sum(0, 0) == 3

Fails! cool! Hmm... after this fail we know that case `x == T` is False.
What we do next?
* May be derive from experience above hypothesis `x != T` i.e. `x == F` for field `f` (field == 2d grid in my mind, it's not a mathematical field)

We can show list of failing cells:
>>> f.cuteq(x, T).not_valid_cells()
[(0, 0), (1, 1), (2, 0)]

Also possible to show info (sum_low, sum_high) about each cell:
>>> f_u0T = f.cuteq(x, T)
>>> [((i, j), f_u0T.cell_sum_tuple(i, j)) for i, j in f_u0T.not_valid_cells()]
[((0, 0), (0, 3, 3)), ((1, 1), (1, 5, 6)), ((2, 0), (0, 3, 3))]

Good info view? Or not? May be you want some names?

>>> f_u0T.show_info_about_cell(0, 0)
'(0, 0) == 0 and sum(0, 0) == 3'

Let's use pplist "Pretty print list" to show info about all not valid cells

>>> pplist(f_u0T.show_info_about_cell(i, j) for i, j in f_u0T.not_valid_cells())
(0, 0) == 0 and sum(0, 0) == 3
(1, 1) == 1 and 5 <= sum(1, 1) <= 6
(2, 0) == 0 and sum(2, 0) == 3

TODO: implement f.show_with_digits()

I think better name is `describe_field(row_numbers=True, columns_numbers=True)`

> f_u0T.describe_field()
  j= 0123
i=0: .oo. -> 0110
i=1: oo?? -> 1122
i=2: .oo. -> 0110

Above ^^^ you can see two nice corresponding views of same object `f_u0T`
Note: f_u0T means f with hypothesis u0 == T
      Possible generalization: f_u0T_u1F_u2T_...
      But it's a commutative: f_u0T_u1F <==> f_u1F_u0T
      And associative? (Check it by yourself)

>>> f.cuteq(y, T).pp
.oo.
o?o?
.oo.
unsat from (1, 2) == 1 and 4 <= sum(1, 2) <= 6

Interesting non-trivial case:
>>> f.cuteq(z, F).pp
.oo.
o??.
.oo.

Still checks... but we can do more assumptions:

case y == T:
  >>> f.cuteq(z, F).cuteq(y, T).pp
  .oo.
  o?o.
  .oo.
  unsat from (1, 2) == 1 and 4 <= sum(1, 2) <= 5
  
case y == F:
  >>> f.cuteq(z, F).cuteq(y, F).pp
  .oo.
  o?..
  .oo.

  Still checks! He's a hard nut to crack.

  But backtrack!
  case x == F:
    >>> f.cuteq(z, F).cuteq(y, F).cuteq(x, F).pp
    .oo.
    o...
    .oo.
    unsat from (0, 2) == 1 and sum(0, 2) == 1

  May be x can be True? Still holding out hope...
  case x == T:
    >>> f.cuteq(z, F).cuteq(y, F).cuteq(x, T).pp
    .oo.
    oo..
    .oo.
    unsat from (0, 0) == 0 and sum(0, 0) == 3

    Unsatisfiable? It's boring...
    If two cutted parts are unsat then the whole model is unsat.
    I.e. we have z == F ==> unsat
    This means that we can derive from experience above
      that z == T
  backtracking depth dec
backtracking depth dec

At this step we know: only possible case is z == T
>>> f.cuteq(z, T).pp
.oo.
o??o
.oo.

You can see symmetry?
It really can help us to find a shorter proof.
How?
For any cuteq: if result is unsat then other is unsat.
We don't need to check second case.

>>> f.cuteq(z, T).c(y, T).pp # doctest: +ELLIPSIS
.oo.
o?oo
.oo.
unsat ...

From unsat above we have 'y != T', and from
symmetry 'x != T' i.e.
>>> f.cuteq(z, T).c(y, F).c(x, F).pp
.oo.
o..o
.oo.
valid

We can use relax to solve main state immediately,
but relax used backtracking only by depth=1.
>>> f.relax().pp
.oo.
o..o
.oo.
valid

Test all .relax contradiction cases:

>>> f.cuteq(z, F).relax().pp
.oo.
o.o.
.oo.
unsat from (1, 2) == 1 and sum(1, 2) == 4

>>> f.cuteq(y, T).relax().pp
.oo.
o?o?
.oo.
unsat from (1, 2) == 1 and 4 <= sum(1, 2) <= 6

>>> f.cuteq(x, T).relax().pp
.oo.
oo??
.oo.
unsat from (0, 0) == 0 and sum(0, 0) == 3

>>> count(f.unknowns)
3

>>> f.pp
.oo.
o???
.oo.

>>> f2 = f.copy()
>>> f[0,-1] = '?'
>>> f2[0,-1] = 2
>>> assert f == f2

  > f.assign_name_to_cell('x', f.unknowns_list[0])
  equiv to: f[f.unknowns_list[0]] = Bool('x')
  TODO: '?' can be replaced by Joker('Bool'), Hole('Bool')

We can "grab" variables from future:

>>> f = Field(3, 2, 2)
>>> f.setrow([0, 0, 0])
>>> print('Do you like spiderladies?'); f.pp # doctest_: -ELLIPSIS not works but print helps
Do you like spiderladies?
...
???
>>> f[0, -1] = '?'; f[0, 0] = 'o'; f.pp
o.?
???

>>> f.relax(max_iters=1).pp
o.?
oo.

>>> f.relax(max_iters=1).relax(max_iters=1).pp # user want to know that next relax is do nothing
o.o
oo.
unsat from (0, 2) == 1 and sum(0, 2) == 1

>>> f.relax().pp # default 100 iterations
o.o
oo.
unsat from (0, 2) == 1 and sum(0, 2) == 1

Interesting: this is last '.o?' char to test __setitem__
TODO: explain line above
>>> f[0, 0] = '.'; f.pp
..?
???

Grab variables x,y,z from Now:
>>> x, y, z, _ = f.unknowns

Grab variables x,y,z from Future:
>>> x, y, z = f.relax().unknowns

>>> f[x], f[y], f[z]
(2, 2, 2)

>>> f.cuteq(x, 1).pp
..o
???

>>> f.cuteq(x, 1).relax().pp
..o
.oo
unsat from (0, 1) == 0 and sum(0, 1) == 3

hhhhh
v???v
v???v
hhhhh
* Outer values must be equal to each other?

Outer conditions:
>>> g = Field(5, 3, 2)
>>> x, y, z = ((i, 0) for i in range(3))
>>> g[x]=g[y]=1
>>> g.pp
o????
o????
?????
>>> g.relax().pp
o????
o????
.????
>>> g[-1,-1] = 1; g.pp
o????
o????
????o
>>> g[z]
2

We can get value of z from relaxed field:
>>> g.relax()[z]
0

>>> w, q, r = ((0, j+1) for j in range(3))

relax must stop if g.check() is false

>>> g.cuteq(w,0).cuteq(q,0).relax().pp # doctest: +ELLIPSIS
o..??
ooo??
..o?o
unsat from (1, 1) ...

explanation of E (why we have empty_set in position (1, 1)):
<=>
explanation of contradiction in cell(1, 1):
case f[1, 1] == 0:
  o..??
  o.o??
  ..o?o
  unsat from ... sum(0, 0) == 1

case f[1, 1] == 1:
  o..??
  ooo??
  ..o?o
  unsat from ... sum(1, 1) == 4

>>> g.c(w,0).c(q,1).c(r,0).relax().pp
o.o.?
oo.o?
...oo
unsat from (2, 2) == 0 and sum(2, 2) == 3

>>> g.c(w,0).c(q,1).c(r,2).relax().pp
o.o??
oo.??
...?o

>>> g.c(w,0).c(q,1).c(r,1).relax().pp
o.oo.
oo.??
...?o

low gain: relax opens only one cell (0, -1)
low gain can implies in general long proofs

>>> u = (1, 3)
>>> g.c(w,0).c(q,1).c(u,0).relax().pp
o.oo.
oo..o
...oo
valid

>>> g2 = g.c(w,0).c(q,1).c(u,1).relax(); g2.pp
o.oo.
oo.oo
....o
unsat from (0, 4) == 0 and sum(0, 4) == 3

>>> g2[0,4]=2
>>> g2[0,3]=2
>>> g2[1,4]=2
>>> g2.pp
o.o??
oo.o?
....o

>>> g2.c((1,4),0).pp; g2.c((1,4),1).pp
o.o??
oo.o.
....o
unsat from (2, 4) == 1 and sum(2, 4) == 1
o.o??
oo.oo
....o
unsat from (2, 3) == 0 and sum(2, 3) == 3

>>> for i in bools():
...   g2.c((0,3), i).pp
o.o.?
oo.o?
....o
unsat from (1, 2) == 0 and sum(1, 2) == 3
o.oo?
oo.o?
....o

>>> for i in bools():
...   g2.c((0,3), 1).c((0,4), i).pp
o.oo.
oo.o?
....o
o.ooo
oo.o?
....o
unsat from (-1, 3) == 0 and sum(-1, 3) == 3

relax can found that state
>>> g2.c((0,4), 0).relax().pp
o.oo.
oo.oo
....o
unsat from (0, 4) == 0 and sum(0, 4) == 3

>>> g2.c((0,4), 1).c((0,3), 0).pp
o.o.o
oo.o?
....o
unsat from (1, 2) == 0 and sum(1, 2) == 3


Bug relax give error:
  list index out of range
>>> f = Field(5,5,2)
>>> f.setrow([2,0,0,2,2], row_index=1)
>>> f.setrow([1,1,1,1,1])
>>> f.setrow([2,0,0,2,2])
>>> h=(0,2)
>>> f.c(h,1).pp
??o??
?..??
ooooo
?..??
?????
>>> g = f.c(h,1).relax()
>>> g.check()
False
>>> g.not_valid_cells()
[(-1, 2)]

^^^^^^^^^ This list can't be empty if check is false.
>>> if not g.check():
...   assert len(g.not_valid_cells()) > 0

Anti case: (it's redundant, but can be generalized for any fields using quickcheck|pytest)
> if field.check():
...   assert len(field.not_valid_cells()) == 0

>>> g.relax().pp
?ooo?
?...?
ooooo
?..??
?????
unsat from (-1, 2) == 0 and sum(-1, 2) == 3

If we know value of variable `u` for main state we can store it immediately
by using `f[u] = value`
or we can reduce bugs in proof if we store it automatically
> f.store(u)

Or store all info that we found for all vars:
> f.store_all()

Example from cmd prompt:
> f.cuteq(x, 1).cuteq(y, 1).cuteq(z, 1).cuteq(u, 1).relax()
.oo
.oo
> f.cuteq(x, 1).cuteq(y, 1).cuteq(z, 1).cuteq(u, 0).relax()
..o
ooo
unsat from cell(i=1, j=0): (val=1, sum_lo=1, sum_hi=1)
> f[u]=1
> f.cuteq(x, 1).cuteq(y, 1).cuteq(z, 1).relax()

"""



"""
General case:

What if Field already have 0 solutions, but f.check() don't show it.
Why? It just check constraints only superficially.
How to prove that it have 0 solutions?
1. Just use backtracking.

> f_wrong = f.cuteq(y, T).cuteq(x, F)

> [(f'z = {i}', f_wrong.cuteq(z, i).check()) for i in [F, T]]
[('z = 0', False), ('z = 1', False)]

2. Use some theorems.
  + add new theorems in context
    To add new we need to instantiate some "more general thorems with forall"
  + Create some super hard object, that we can't understand in general
    (they usually helps to create/find "general thorems with forall")

> TODO: add example of using theorems

3. Use something interesting ... symmetries, 

> TODO: add examples of using something interesting ...

Proof is a function :: state -> "cut-action".
State = (context="set of hyps", goal)
goal is optional when we search for meta-variables (see eapply in Coq)

Goal can be undetermined when we search solution instead of proof.
* When we search proof:
  We just eliminate false states.
* When we search solution:
  We just eliminate states with `solutions_count == 0`.

Abstract cutting:
if we have hyp(i.e. information) about possible values of `f.solution_count` for 
given field `f` then we can select actions from:
* let f.solution_count = 0, let f.solution_count = 1, ...
* let f.solution_count = subset()
* let other_useful_related_to_solutions_count_prop = True
* ...

Cutting(=subst=add_hyp=...):
> f.cut(f.var('x') == true)
> f.cut(f.var('x'))
> f.cut('x = true')
> f.cut('x')
[f.add('x = true'), f.add('x = false')]

Proving like in Coq:
> f.cut('x'); [x = false: f.obviously | x = true: f.idtac]
> f
..?
.oo
.oo

Comment: left case is proved, but right is stay untouched because `f.idtac` do nothing
"""

def example_chess_pattern():
  """
  How to prove this lemma (in Coq for example):
    chess_pattern: in field(2, n) number of enabled cells is < 5
  >>> f = Field(8, 2, 2)
  >>> g = f.copy()
  >>> for N in range(6):
  ...   g[N % 2, N] = 1
  ...   status = g.relax().check()
  ...   print(f'{N+1} cells possible? Answer:', status)
  ...   if not status:
  ...     print(g.relax())
  ...     print('-----')
  ...     print('state before:')
  ...     g[N % 2, N] = 2
  ...     print(g.relax())
  ...     break
  1 cells possible? Answer: True
  2 cells possible? Answer: True
  3 cells possible? Answer: True
  4 cells possible? Answer: True
  5 cells possible? Answer: False
  o.o.o???
  oooo????
  unsat from (2, 1) == 0 and sum(2, 1) == 3
  -----
  state before:
  o.oo.???
  oo.o????
  """

def example_optimize_relax():
  """
  For example we have some big square matrix:
  >>> n = 10 # n might be >> 10
  >>> f = Field(n, n, 2)
  >>> f.pp
  ??????????
  ??????????
  ??????????
  ??????????
  ??????????
  ??????????
  ??????????
  ??????????
  ??????????
  ??????????
  
  You can see that almost all positions equivalent to each other.
  Equivalence classes:
  * corner neighboring
  > f.corners
    ??~
    ??~
    ~~~
  [(0, 0), (0, f.w - 1), (f.h - 1, 0), (f.h - 1, f.w - 1)]
           ^^^^^^^^^^^^  ^^^^^^^^^^^^
                diagonal elements also can be grouped
  * bottom/left/right/top i.e. inner border neighboring
    ???
    ???
    ~~~
  * center neighboring
    ???
    ???
    ???

  Additional:
  * outer border
    ???
    ~~~ can be squashed to '?~~'
    ~~~
  * outer corner
    ?~~
    ~~~ can be squashed to '?~' or maybe '?~'
    ~~~                    '~~'          '*~'
  * trivial case
    ~~~
    ~~~
    ~~~
  Lemma relax_return_same_value:
    if we call relax on one element of class `C` then relax return same value on other elements of `C`.

  """

def bools():
  yield 0
  yield 1

def example_enumerate_solutions_2by2():
  """
  Task: enumerate all possible 2x2 fields with "as minimal as possible number of cases".
  or "list all models using the minimum possible number of cases".
  >>> f = Field(2, 2, 2); f.pp
  ??
  ??

  >>> a, b, c, d = f.unknowns

  ab
  cd
  >>> assert b == (0, 1)
  
  forall val :: f.c(a, val) <=> f.c(b, val) <=> f.c(c, val) <=> f.c(d, val) by symmetry of square

  lemma optimal_first_action: first cut-action in {f.c(a, 0), f.c(a, 1)}
    by wlog (without loss of generality)
  Note: What to select f.c(a, 0) or f.c(b, 0) if they are equivalent?
       Same question: What to eat Apple A or Apple B if they have same characteristics?
       Simple answer: any.
       But we must select one instance from equivalence class.
       Let's select `f.c(a, 0)` for class 'val=0' and `f.c(a, 1)` for class 'val=1'.

  i.e. in state #1 we have 2 actions: {f.c(a, 0), f.c(a, 1)}

  >>> for i in bools():
  ...   print(f'{i+1}) assume a == {i}'); f.c(a, i).pp
  1) assume a == 0
  .?
  ??
  2) assume a == 1
  o?
  ??

  Let's store this two states in `g`
  >>> gs = [f.c(a, val).setname(f'a == {val}') for val in bools()]; gs
  [Field(2, 2, name='a == 0'), Field(2, 2, name='a == 1')]

  In each state {f.c(a, 0), f.c(a, 1)} we have 4 actions:
    forall val in [0, 1] :: {assume(c == val), assume(d == val)}

  > step2 = [(first_action, second_action) for first_action in [(a, 0), (a, 1)] for second_action in [(c, 0), (c, 1), (d, 0), (d, 1)]]

  >>> step2 = [('c', c, 0), ('c', c, 1), ('d', d, 0), ('d', d, 1)]
  >>> states2 = []
  >>> for g in gs:
  ...   for v_name, _var, value in step2:
  ...     states2.append(g.cuteq(_var, value).setname(f'{g.name}, {v_name} == {value}'))
  >>> len(states2)
  8

  >>> statuses = [(st.name, st.relax().get_model_status(as_str=True)) for st in states2]
  >>> pplist(statuses)
  ('a == 0, c == 0', 'all_true')
  ('a == 0, c == 1', 'all_false')
  ('a == 0, d == 0', 'all_true')
  ('a == 0, d == 1', 'all_false')
  ('a == 1, c == 0', 'all_false')
  ('a == 1, c == 1', 'unknown')
  ('a == 1, d == 0', 'all_false')
  ('a == 1, d == 1', 'unknown')

  >>> states2_may_be_sat = [st for i, st in enumerate(states2) if statuses[i][1] != 'all_false']
  >>> states2_relaxed = []
  >>> for i, st in enumerate(states2_may_be_sat):
  ...   st_relaxed = st.relax()
  ...   states2_relaxed.append(st_relaxed)
  ...   print(f'{i+1}) ' + st.name + ' ==> ' + st_relaxed.get_model_status(as_str=True)); st_relaxed.pp
  1) a == 0, c == 0 ==> all_true
  ..
  ..
  valid
  2) a == 0, d == 0 ==> all_true
  ..
  ..
  valid
  3) a == 1, c == 1 ==> unknown
  o?
  o?
  4) a == 1, d == 1 ==> unknown
  o?
  ?o

  >>> states2_relaxed[0] == states2_relaxed[1]
  True

  >>> A = [0,1,2,3]
  >>> A = [A[0]] + A[2:]; A
  [0, 2, 3]
  
  Delete duplicates:
  >>> states2_relaxed = [states2_relaxed[0]] + states2_relaxed[2:]
  >>> states2_relaxed[1].cuteq(b, 0).relax().pp
  o.
  oo
  unsat from (0, 1) == 0 and sum(0, 1) == 3

  >>> states2_relaxed[1].cuteq(b, 1).relax().pp
  oo
  oo
  valid

  >>> states2_relaxed[2].cuteq(b, 0).relax().pp
  o.
  oo
  unsat from (0, 1) == 0 and sum(0, 1) == 3

  >>> states2_relaxed[2].cuteq(b, 1).relax().pp
  oo
  oo
  valid

  >>> solutions = [states2_relaxed[0], states2_relaxed[2].cuteq(b, 1).relax()]
  >>> pplist(solutions)
  ..
  ..
  valid
  oo
  oo
  valid
  """

def example_enumerate_solutions_3by3():
  """
  Task: enumerate all possible 3x3 fields with minimal proof length (`minimal proof length` hard to formalize)
    It may be possible to replace `minimal proof length` with `minimum number of branches|cases|models`
  >>> f = Field(3, 3, 2); f.pp
  ???
  ???
  ???

    j=  0  1  2
  i=0: x0 x1 x2
  i=1: x3 x4 x5
  i=2: x6 x7 x8
  >>> x0, x1, x2, x3, x4, x5, x6, x7, x8 = f.unknowns
  >>> x5 # only case that we must check to cut other symmetrical wrong states, it beautiful... but how this thing can be formalized?
  (1, 2)

  Note: Also we can use x7, x1, x3, ... and we can't use x0, x4, x8 
  
  If we use cuteq:
    We can begin from 3 places:
    * corner
    * border
    * center

  If we use some other cutting action:
    We can begin from all possible sets of thing above:
      * {corner, border, center}
      * ...
      + something really interesting and complicated
  
  i.e.
  >>> possible_actions = set({'center', 'border', 'corner'})

  Let's begin from center:
  >>> used_actions = set()
  >>> used_actions.add('center')

  Hint: use dir to view all methods of set object
  > dir(possible_actions)

    If possible_actions can be mutable:
      > possible_actions.remove('center')
      > sorted(possible_actions)
      ['border', 'corner']

    If possible_actions can't be mutable: (we selected this branch as main)
      Do nothing
  
  Our first assumption:
  >>> f.c(x4, 0).pp
  ???
  ?.?
  ???

  I'd make a prediction: 
    We have only 1 solution:
    .o.
    o.o
    .o.
    Info|statistics:
      Name of the pattern: cross
      number of cells: 4
      symmetries: central, diagonal
  >>> f.c(x4, 0).relax() == f.c(x4, 0)
  True

  Dammit! relax can't solve this! And that's what this method is all about, because backtracking can create long proofs.
  If we don't like long proofs ==> we don't like pure backtracking (without some reorderings of actions, etc...)

  From properies of relax we can conclude that we need at least 2 cuteq's to view `unsat from ...`
  Something like:
    f.c(x4, 0).c(_, _)    100% f.check() != False from relax properties
    f.c(x4, 0).c(_, _).c(_, _) might be unsat

    TODO: implement proof|pattern above like this:
    > f.get_general_facts_from_current_state()
    fact1: f.c(x4, 0).c(_, _) ==> False

    > f.get_experience() # f.get_history()
    [Called method relax on state _, ...]

  Now we can update possible actions (because we in second state that != first state):
                                                                      ^^ be careful with equivalences...
    * A1: assumption in corner
    * A2: assumption in border

    + action level-2:
      thinking about what if A1 == A2? (make assumption Eq(A1, A2))
      Hmm... They really equivalent? (main branch: assume Yes)
      Proof of A1 == A2:
        Qed by commutativity of sum_around(center_cell)

    Formal recap of actions:
    Group1:
    * A1: assumption in corner
    * A2: assumption in border

    Group2:
    * try to prove Eq(A1, A2)
    * ... (can be complicated, be careful)

  >>> second_state_actions = \
      {
  ...   'A.corner': 'assumption in corner',
  ...   'A.border': 'assumption in border',
  ...   'B.prove': 'try to prove Eq(A1, A2)',
  ...   'B.use': 'use already proved Eq(A1, A2)'
  ... }

  >>> states = [possible_actions, second_state_actions]

  > glue_to_one_type(possible_actions, second_state_actions)

  Let's select 'minimally boring' action:
    selected: {B.prove, B.use}
      select simplest:
        selected: {B.use}

  We selected
  > second_state_actions['B.use']
  'use already proved Eq(A1, A2)'

  How to use fact about two object A and B? Spoiler =====>                                                                                    Glue it to one equivalence class!
  
  Before:
  > second_state_actions.keys()
  ['A.corner, 'A.border', 'B.prove', 'B.use']

  > glue_in({'A.corner, 'A.border'}, second_state_actions)

  After:
  > second_state_actions.keys()
  ['A', 'B.prove', 'B.use']
  """

CELLSTR = '.o?E' # ' вЂў?', ? = set({0, 1}), E = Empty set
CHAR_SET = CELLSTR[:3]
assert CHAR_SET == '.o?'
CELL_ZERO, CELL_ONE, CELL_UNKNOWN = 0, 1, 2
MODEL_STATUS_FALSE, MODEL_STATUS_TRUE, MODEL_STATUS_UNKNOWN = 0, 1, 2

def pplist(xs: list, numbers=False):
  """
  >>> xs = ['???', 'AAA', 'A\\nB\\nC']
  >>> pplist(xs)
  ???
  AAA
  A
  B
  C

  >>> pplist(xs, numbers=True)
  1)
  ???
  2)
  AAA
  3)
  A
  B
  C
  """
  no_elements_printed = True
  for i, x in enumerate(xs):
    no_elements_printed = False
    if numbers:
      print(f'{i+1})')
      print(x)
    else:
      print(x)
  if no_elements_printed:
    print('Empty')


def count(iterable):
  counter = 0
  for _ in iterable:
    counter += 1
  return counter


def cell_str(cell):
  assert cell in range(len(CELLSTR))
  return CELLSTR[cell]


def still_life_rule_simple(central_cell: "0|1", sum_around: int) -> bool:
  assert False, "This code just for demonstrating"
  assert central_cell in range(2)
  if sum_around == 2:
    return True
  if sum_around == 3:
    # But if central_cell in range(3) then
    #   we replace this line with `central_cell != 0`
    return central_cell == 1
  return central_cell == 0


def still_life_rule(central_cell: "0|1|2", sum_around_low: int, sum_around_high: int) -> int:
  """Rule that can work with 3-state cells: {0, 1, ?}
  TODO: fix unknown (now unknown really do nothing)
  """
  assert sum_around_low <= sum_around_high
  assert central_cell in range(3)
  false, true, unknown = range(3)
  # false means local distinct solutions/models = 0
  # true means local distinct solutions/models = 1
  # unknown means local distinct solutions/models > 1
  # local means `in set of cells in square 3x3`

  if sum_around_high == 2:
    # assert sum_around_low <= 2
    # assert sum_around == 2
    return true
  if sum_around_low == 3:
    if sum_around_high == 3:
      if central_cell == false:
        return false
      if central_cell == true:
        return true
      if central_cell == unknown:
        return unknown # {false, true} = unknown
    elif sum_around_high >= 4:
      return unknown

    # if central_cell == unknown:
    #   # In that case we can assume that central_cell == false
    #   # and realize contradiction
    #   # we have only 1 local solution
    #   return true

  if sum_around_low >= 4 or sum_around_high <= 1:
    if central_cell == false:
      return true
    if central_cell == true:
      return false
    if central_cell == unknown:
      # return {true, false}
      return unknown
  
  if sum_around_low <= 2 and sum_around_high >= 3:
    # set (lo, hi) includes (2, 3) ==>
    #   if (2, 3) -> return true then (lo, hi) -> return true
    # (2, 3) ==> sum == 2 or 3 ==> 
    # return true or {false: false, true: true, unknown: true}
    # return {false: false or true, true: true or true, unknown: true or true}
    # return {false: true, true: true, unknown: true}
    return true

  # TODO: How to check that we write in code all possible cases?
  # we can use z3-solver: see `check_still_life_rule.py`
  # 
  # This is how all cases checked by z3
  # goal = Or(
  #   shigh == 2,
  #   slow == 3,
  #   slow >= 4,
  #   shigh <= 1,
  #   And(slow <= 2, shigh >= 3)
  # )
  assert False, "still_life_rule: impossible case"


class Field:
  """2d Matrix with 3-state booleans:
  0 = false
  1 = true
  2 = unknown
  """
  def __init__(self, width, height, value=2, outer=0, rule=None, check_field=None, name=''):
    self.name = name
    self.data = []
    for _ in range(height):
      row = [value for _ in range(width)]
      self.data.append(row)
    self.rule = still_life_rule if rule is None else rule
    # What if you want to use custom check functions?
    if check_field is None:
      self.check_field = lambda field: True
    else:
      self.check_field = check_field
    self.outer_value = outer
    self.row_index_temp = 0

  def copy(self):
    """Create copy of instance. TODO: how to auto-check that arguments of copy is (almost) the same as arguments of __init__?
    Question: check_field=self.check_field give strange behaviour, how to safely copy lambda function from one field to another?
    Answer: use brackets for lambdas: (lambda field: True) if ... else ...
    """
    copied = Field(self.w, self.h, outer=self.outer_value, rule=self.rule, check_field=self.check_field, name=self.name)
    for i, j, cell in self.cells():
      copied[i, j] = cell
    return copied

  def cut(self, hyp):
    """cut <=> add_hypothesis <=> add_assumption <=> ...
    """
    ...
    
  def c(self, index2d, cell_value):
    return self.cuteq(index2d, cell_value)

  def cuteq(self, index2d, cell_value):
    """ `f.cuteq(index2d, value)` Returns cutted copy of the field

    f.cuteq(pos, cell_value) <=> f.cut(f[pos] == cell_value)

    Create instance and fill it with values:
      >>> f = Field(4, 3, 2)
      >>> f.setrow([0, 1, 1, 0])
      >>> f.setrow([1, 2, 2, 2])
      >>> f.setrow([0, 1, 1, 0])

    Define aliases:
      >>> x, y, z = f.unknowns

      Legend: '.' ~ F, 'o' ~ T, `?` ~ U

      >>> F, T, U = 0, 1, 2

    Let's do several assumptions:
    >>> f.cuteq(x, T).pp
    .oo.
    oo??
    .oo.
    unsat from (0, 0) == 0 and sum(0, 0) == 3

    `unsatisfiable` means "no solution exist for given set of constraints"

    `from (0, 0) == 0 and sum(0, 0) == 3` is just a first contraint that fails.
      Where:
      * notation `(0, 0) == 0` used for `f[0, 0] == 0` because in general one can just
        select another name of variable `f` for example `my_field`.

      * `sum(0, 0)` is a sum around center cell `(0, 0)`, center cell excluded.

    Or use short version of cuteq: `field.c(index2d, cell_value)`
    >>> f.c(y, T).pp
    .oo.
    o?o?
    .oo.
    unsat from (1, 2) == 1 and 4 <= sum(1, 2) <= 6

    Two experiments above resulting with this hypothesis:
    And(x != T, y != T) i.e. And(x == F, y == F)

    TODO: implement Field.get_context() # or .get_experience(), get_current_model()
    >>> f.get_experience() # doctest: +SKIP
    And(x == F, y == F)

    or
    hx: x == F
    hy: y == F

    or we can 'squash' this two possible outputs to one equivalence class
    
    Apply all experience:
    >>> f = f.cuteq(x, F).cuteq(y, F); f.pp
    .oo.
    o..?
    .oo.
    
    Possible variants of feature:
      > f = f.apply_experience() # <=> f = f.cuteq(x, F).cuteq(y, F)
      .oo.
      o..?
      .oo.
      
      Where goal object in Field?
      Answer:
        Bits inside Field equivalent to set of special type hypothesis.
        Nothing related to goal.

      > f.assume(f.get_experience())
      .oo.
      o..?
      .oo.
    
    >>> f.get_model_status(as_str=True) # doctest: -SKIP
    'unknown'

    >>> pplist([f'z == {i} ==> ' + f.cuteq(z, i).get_model_status(as_str=True) for i in [F, T]])
    z == 0 ==> all_false
    z == 1 ==> all_true

    Lemma group A {
      sat     <=> Not(all_false)
      unsat   <=> all_false
      unknown <=> set({sat, unsat}) <=> {Not(all_false), all_false} <=> True <=> No useful info
        <=> H: true <=> hypothesis context stay unchanged
    }
    TODO: Can we formalize (maybe using Coq) all steps in `Lemma group A`?

    May be that sat not equivalent to "all combinations is true solution":
      If that is true:
      We must change get_model_status()
        * Changed output for get_model_status() 
          from 'sat'|'unsat'|'unknown' to 'all_true'|'all_false'|'unknown'
      
    """
    # H: f[i, j] == cell_value
    # hyp = i, j, cell_value
    assert cell_value in range(3)
    assert self[index2d] == 2, f'Value of variable|hole in position {index2d} is already known.'
    copied = self.copy()
    copied[index2d] = cell_value
    return copied

  def check(self):
    # first check outer cells
    for i, j in self.outer_cells():
      if not self.is_valid_cell(i, j):
        return False
    for i, j, _ in self.cells():
      if not self.is_valid_cell(i, j):
        return False
    return self.check_field(self)

  def outer_cells(self):
    """
    hhhhh
    v???v w=3, h=2
    v???v
    hhhhh
    """
    # horizontal lines
    for j in range(-1, self.w + 1):
      yield (-1, j)
      yield (self.h, j)
    
    # vertical lines
    for i in range(self.h):
      yield (i, -1)
      yield (i, self.w)
    
  def show_info_about_cell(self, i, j):
    """
    Instance from Equivalence class 1. (0, 0) == 0 and sum(0, 0) == 3
    Instance from Equivalemce class 2. (1, 1) == 1 and 5 <= sum(1, 1) <= 6
    """
    value, sum_lo, sum_hi = self.cell_sum_tuple(i, j)
    sum_str = ''
    if sum_lo == sum_hi:
      sum_str = f'sum({i}, {j}) == {sum_hi}'
      if sum_lo == 2:
        # we don't need to show cell value
        return sum_str
    else:
      sum_str = f'{sum_lo} <= sum({i}, {j}) <= {sum_hi}'
    return f'({i}, {j}) == {value} and {sum_str}'

  def cell_sum_tuple(self, i, j):
    return self[i, j], self.get_sum_around_low(i, j), self.get_sum_around_high(i, j)

  def not_valid_cells(self) -> list:
    """Return list of cells, and each cell contradicts with `self.rule`
    """
    inner = [(i, j) for i, j, _ in self.cells() if not self.is_valid_cell(i, j)]
    outer = [(i, j) for i, j in self.outer_cells() if not self.is_valid_cell(i, j)]
    return outer + inner # first check outer cells (like in self.check())
    
  def is_valid_cell(self, ci, cj) -> bool:
    s_low = self.get_sum_around_low(ci, cj)
    s_high = self.get_sum_around_high(ci, cj)
    return 0 != self.rule(central_cell=self[ci, cj], sum_around_low=s_low, sum_around_high=s_high)

  def relax(self, max_iters=100):
    """Only tries simple one assumption with depth=1 but for all unknowns '?'
    Returns a copy of the instance ==> you can make chains:
    > f.relax(max_iters=1).relax(max_iters=1)
    """
    field_tmp = self.copy()
    field_tmp._mut_relax(max_iters)
    return field_tmp

  def check_near(i, j, distance=1):
    """
    Check near cell (i, j)
    if distance=1 checks
    o??
    .X?
    o??

    if distance=2 checks
    ?????
    oo???
    ..X??
    oo???
    ?????

    if distance=0 checks what?
    """
    pass

  def _mut_relax(self, max_iters=100):
    # trivial case: we don't need to make assumptions
    # if main branch already fails
    if not self.check():
      return
    something_changed = True
    iters = 0
    while something_changed and iters < max_iters:
      unknowns = self.unknowns
      something_changed = False
      for i, j in unknowns:
        assert self[i, j] == CELL_UNKNOWN
        # old_value just equal to 2 ==> we don't need to store it
        new_value = self[i, j]
        self[i, j] = CELL_ZERO
        if not self.check(): # TODO: replace with check_near(i, j)
          new_value = CELL_ONE
          # We can use code commented below if we want to derive unsat from `empty set`,
          # but in that case we need some extra info attached to empty_set (counterexample, proof).
          # TODO: think about empty set + extra info
          # 
          # Solution 1:
          # We can just give proof for
          #   case 1: cell(x, y) == Empty_set
          #     or two proofs for
          #   case 2: cell(x, y) == 0 -> false, cell(x, y) == 1 -> false
          #
          # self[i, j] = 1
          # if not self.check():
          #   new_value = 3
          self[i, j] = CELL_ONE
          if not self.check():
            # We have empty set and just return works?
            return
        else: # value 0 checks
          self[i, j] = CELL_ONE
          if not self.check(): # 1 not checks
            new_value = CELL_ZERO
        self[i, j] = new_value
        if new_value != CELL_UNKNOWN:
          something_changed = True
      iters += 1
    
  def setrow(self, cells, row_index=None):
    if row_index is not None:
      assert row_index in range(self.h)
      self.row_index_temp = row_index
    assert len(cells) == self.w
    self.data[self.row_index_temp] = cells
    self.row_index_temp = (self.row_index_temp + 1) % self.h

  def inside_bounds(self, i, j) -> bool:
    return i >= 0 and i < self.h and j >= 0 and j < self.w

  def get_cells_around(self, ci, cj, inside=False):
    """Returns generator that contains values :: 0|1|2 of cells around position (i, j)
    """
    if inside:
      for i in range(max(0, ci-1), min(ci+2, self.h)):
        for j in range(max(0, cj-1), min(cj+2, self.w)):
          if i == ci and j == cj:
            continue
          yield self[i, j]
    else:
      for i in range(ci-1, ci+2):
        for j in range(cj-1, cj+2):
          if i == ci and j == cj:
            continue
          yield self[i, j]

  def get_sum_around_low(self, ci, cj):
    # values is a generator object
    values = self.get_cells_around(ci, cj)
    return sum(filter(lambda value: value < 2, values))

  def get_sum_around_high(self, ci, cj):
    # values is a generator object
    values = self.get_cells_around(ci, cj)
    return sum(map(lambda value: 1 if value == 2 else value, values))

  def cells(self):
    """Returns generator that contains all cells of the field
    """
    for i, row in enumerate(self.data):
      for j, c in enumerate(row):
        yield (i, j, c) # I think `yield i, j, c` less obvious when user looks in documentation

  @property
  def unknowns(self):
    """Returns generator object that gives cells denoted by '?'. TODO: cache unknowns"""
    return ((i, j) for i, j, cell in self.cells() if cell == 2)

  @property
  def unknowns_list(self):
    return list(self.unknowns)

  @property
  def w(self):
    """Width of the field
    """
    return 0 if self.h == 0 else len(self.data[0])
  
  @property
  def h(self):
    """Height of the field
    """
    return len(self.data)
    
  @property
  def size(self):
    return self.w, self.h

  def get_model_status(self, as_str=False) -> "0|1|2":
    """Return 3-state boolean instead of 2-state like returns self.check() -> bool

    0 ~ F ~ all_false
    1 ~ T ~ all_true
    2 ~ U ~ unknown

    Possible feature:
      Legend: 
         ==> # implies
        <==> # if and only if

                        unsat|sat|unknown
      0 ~ F ~ all_false   (<==> unsat)   ~  ~   ~ solutions
      [False, False, False]
      1 ~ T ~ all_true    ( ==> sat)     ~ 
      [True, True, True]
      2 ~ U ~ unknown     (<==> unknown) ~ unknown #of solutions
      full_set
      3 ~ E ~ Empty_set   (???)          ~ might be internal solver error?
      {}
      4 ~ ? ~ exist_true  (<==> sat)   ~ might be several solutions!
      set(unknown) - all_false
      5 ~ ? ~ exist_false (<== unsat)
      6 ~ ? ~ 

      This going to complicated... Let's renew this :
      Remember: Our target is implemnt maximally symmetric|beautiful|simple to everyday usability thing

      Model states (elements):
      1. all_false [False, False, False, ...]
      2. middle    [True, False, True, ...]
      3. all_true  [True, True, True, ...]
      
      Model set-states (set of elements):
      unknown = {all_false, middle, all_true} # in sorted order
      sat     = {middle, all_true}
      unsat   = {all_false}
      + 5 other set-states

      Or for simplisity all 8 set-states (in gray code order):
      ???         = 000 = {}                    Note: Internal error?
      unsat       = 001 = {all_false}
      exist_false = 011 = {all_false, middle}
      ???         = 010 = {middle}              Note: don't know
      sat         = 110 = {middle, all_true}
      unknown     = 111 = {all_false, middle, all_true}
      ???         = 101 = {all_false, all_true} Note: might be "all_equivalent"
      all_true    = 100 = {all_true}

      Or for simplisity all 8 set-states (in bin code order):
      ???         = 000 = {}                    Note: Internal error?
      unsat       = 001 = {all_false}
      ???         = 010 = {middle}              Note: don't know
      exist_false = 011 = {all_false, middle}
      all_true    = 100 = {all_true}
      ???         = 101 = {all_false, all_true} Note: might be "all_equivalent"
      sat         = 110 = {middle, all_true}
      unknown     = 111 = {all_false, middle, all_true}

    Bad example for `might be several solutions!`:
    > f
    ??
    ??

    > f.print_solutions()
    solutions:
    0)
    ..
    ..

    1)
    oo
    oo

    But possible:
    o.
    oo
    ==> model_status == sat

    Good example for `might be several solutions!`:
    H_assumption: f[0, 0] == f[0, 1] == f[1, 0] == f[1, 1] 
      (or forall pos_i, pos_j : index2d :: f[pos_i] == [pos_j])
    > f
    ??
    ??

    > f.print_solutions()
    solutions:
    0)
    ..
    ..

    1)
    oo
    oo
    Other combinations not possible because H_assumption placed in context.
    ==> model_status == all_true
    """
    checked = self.check()
    if not checked:
      # unsat <=> all_false
      return 'all_false' if as_str else 0

    if count(self.unknowns) == 0:
      # We can return 'sat' even if count(self.unknowns) > 1
      # 'sat' means exists > 0 solutions
      # 'all_true' means all combinations|intantiations of variables in model are true solutions
      return 'all_true' if as_str else 1
    else:
      return 'unknown' if as_str else 2

  def setname(self, name: str):
    self.name = name
    return self

  def __repr__(self):
    if len(self.name) > 0:
      return f"Field({self.w}, {self.h}, name='{self.name}')"
    else:
      return f'Field({self.w}, {self.h})'

  def __str__(self, show_status=True, delimiter=None):
    """
    If get_model_status() == unknown:
    .oo.
    o..?
    .oo.
      
      TODO: Posssible feature:
      But if use backtracking to get ONLY one solution example
      > f.mutable_find_one_counterexample()
      We can see that model is satisfiable:
      > f.get_model_status() == sat
      True

    If get_model_status() == all_true:
    .oo.
    o..o
    .oo.
    all_true

    If get_model_status() == all_false: 
    .oo.
    o.oo
    .oo.
    unsat from ... (simplest [contradiction | proof of unsatisfability])

    Note:
      unsat <=> all_false (# of solutions = 0)
      sat   <=> any_true (exist at least one true solution i.e. # of solutions >= 1)

    >>> f = Field(4, 3, 2)
    >>> f.setrow([0, 1, 1, 0])
    >>> f.setrow([1, 0, 0, 1])
    >>> f.setrow([0, 1, 1, 0])
    >>> f.pp2
    . o o .
    o . . o
    . o o .
    valid

    TODO: implement .set_naming_func(lambda_func)
    > vn((0, 0))
    'x'
    > vn((0, 1))
    False
    > vn((0, 2))
    'y'

    > f.set_naming_func(vn)
    > f.setrow([2, 2, 1, 2], row_index=0)
    > f.pp2

    x ? o ?  <--- we can't see 'y', but we know that y == 1
    o . . o
    . o o .

    """
    s = ''
    if delimiter is None:
      for i, j, c in self.cells():
        if i > 0 and j == 0:
          s += '\n'
        s += cell_str(c)
    else:
      for i, j, c in self.cells():
        if j == 0:
          if i > 0:
            s += '\n'
          s += cell_str(c)
        else:
          s += delimiter + cell_str(c)

    if not show_status:
      return s
    
    status = self.get_model_status(as_str=True)
    if status != 'unknown':
      s += '\n'

    if status == 'unknown':
      # if self.solutions > 0 then sat
      # but if self.check() == True 
      # we know only that we don't have proof of `unsat`
      s += '' # unknown, undetermined
    elif status == 'all_true':
      s += 'valid'
    else: # status == 'all_false' == unsat
      # Show first counterexample
      nvc = self.not_valid_cells()
      if len(nvc) == 0:
        return s + 'unsat by check_field'
      xi, xj = nvc[0]
      s += f'unsat from {self.show_info_about_cell(xi, xj)}'
    return s

  @property
  def pp2(self):
    return print(self.__str__(delimiter=' '))

  @property
  def pp(self):
    return print(self)

  def _repr_with_bounds(self):
    """
    4x3
    .oo.|
    o..o|
    .oo.|
    -----
    """
    s = f'{self.w}x{self.h}\n'
    for i, j, c in self.cells():
      s += cell_str(c)
      if j == self.w - 1:
        s += '|\n'
    s += '-' * (self.w + 1)
    return s

  def from_str(self, s: str, delim='|'):
    """ Read field from string
    History: Code moved + fixed from antivirus Node.from_str
    """
    assert len(delim) == 1, f"Delimeter is just single char, but it have length={len(delim)}"
    delimeter_set = delim + '\r\t\n'
    assert delim not in CHAR_SET, f"Delimeter intersects with CHAR_SET: {CHAR_SET}"
    i, j = 0, 0
    for ch in s:
      assert i in range(self.h)
      assert j in range(self.w)
      if ch in delimeter_set:
        continue
      ch_index = CHAR_SET.index(ch)
      assert ch_index >= 0, f"char: {ch} not in CHAR_SET: {CHAR_SET}"
      self[i, j] = ch_index
      j += 1
      if j == self.w:
        i, j = i+1, 0
    return self

  def __getitem__(self, t):
    i, j = t
    if not self.inside_bounds(i, j):
      return self.outer_value
    return self.data[i][j]
    
  def __setitem__(self, t, cell: "0|1|2"):
    if type(cell) == str:
      assert cell in CELLSTR
      self.data[t[0]][t[1]] = CELLSTR.index(cell)
    else:
      assert cell in range(3)
      self.data[t[0]][t[1]] = cell

  def __eq__(self, other):
    assert isinstance(other, Field), f'Comparing `==` with wrong type {type(other)}.'
    if not(self.w == other.w and self.h == other.h):
      return False
    for i, j, cell in self.cells():
      if cell != other[i, j]:
        return False
    return True

  def __contains__(self, other):
    """ syntax: other in self """
    raise Exception("Not implemented")

  def __le__(self, other):
    assert self.w == other.w, f"Not comparable by width: left.w={self.w}, right.w={other.w}"
    assert self.h == other.h, f"Not comparable by height: left.h={self.h}, right.h={other.h}"
    for i, j, cell in self.cells():
      other_cell = other[i, j]
      if not (other_cell == 2 or cell == other_cell):
        return False
    return True

  def __lt__(self, other):
    return self != other and self <= other

  def ternary_hash(self):
    """
    >>> assert Field(0, 0).ternary_hash() == Field(0, 1).ternary_hash() == Field(1, 0).ternary_hash()
    >>> g = Field(3, 1)
    >>> g.setrow([0, 0, 0]); g.ternary_hash()
    0
    >>> g.setrow([1, 1, 1])
    >>> assert 9*1 + 3*1 + 1 == g.ternary_hash()
    >>> g.setrow([2, 2, 2])
    >>> assert 9*2 + 3*2 + 2 == g.ternary_hash()
    """
    field_hash = 0
    for i, j, value in self.cells():
      field_hash = 3 * field_hash + value
    return field_hash

  def __hash__(self):
    return hash((self.h, self.ternary_hash()))

  @property
  def no(self):
    """ Used to change return value to `None`
    > update_by_branch(gs, 2, x22)
    ...

    > update_by_branch(gs, 2, x22).no
    [Empty line]
    """
    return None


class Rule:
  """
  TODO:
  To create game of life rule use:
  > rule_expr = If(sum_around(2), ...)
  > still_life_rule = Rule(rule_expr)

  To create field with that rule pass Rule as argument to Field constructor
  and it use .is_valid_cell() predicate that defined in Rule:

  > f = Field(width=2, height=2, value=1, rule=still_life_rule); f
  oo
  oo

  > still_life_rule.is_valid_cell(0, 0)
  True
  """
  def __init__(self, rule_expr):
    ...

  def still_from_golly_rule(rule_str: str):
    """Returns still life rule from any dynamic cellular rule
    """
    ...

  def is_valid_cell() -> bool:
    ...


if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
