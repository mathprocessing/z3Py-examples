"""
Esquisse of link.
May be helpful in future, but I don't know how to use it.
"""

def destruct_old(container, ind):
  """
  destruct splits field 'f' to 'f_case_varname_False' and 'f_case_varname_True'
  and also populates set of branches `container` by this new.

  let '->' = `destruct` in
    'f' -> ['fx0', 'fx1']
    'fx0' -> ['fx0y0', 'fx0y1']
  where 'fx0y1' == fx0y1.name
  """
  pass

class Link:
  def __init__(self, container: "list|set<field>", ind: int):
    self.container = container
    self.target = container[ind]

  def __repr__(self):
    return f'Link(container={self.container}, target={self.target})'

  def by(hyp):
    pass