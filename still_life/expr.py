def is_expr(o):
  return isinstance(o, Expr)

def is_const(o):
  return isinstance(o, Const)

def is_var(o):
  return isinstance(o, Var)

def bool_vars(s: str):
  assert type(s) == str
  for ch in s:
    yield Var(ch)

class Expr:
  def check(self, field) -> bool:
    """ Possible: check(var_index), eval(env), ... """
    return True

  def copy(self):
    assert False, "expr.copy() not implemented"
    return self

  def get_hash(self) -> int:
    return 0


class Const(Expr):
  def __init__(self, val):
    self.val = val

  def copy(self):
    return self

  def __repr__(self):
    return f'Const({self.val})'

  def __str__(self):
    return str(self.val)


class Var(Expr):
  """
  >>> hole = Var(); hole
  Var(_)
  >>> x, y = bool_vars('xy'); (x, y)
  (Var(x), Var(y))
  """
  def __init__(self, name=None):
    if name is not None:
      assert type(name) == str
      self.name = name
    else:
      self.name = '_'

  def copy(self):
    return Var(name=self.name)

  def __repr__(self):
    return f'Var({self.name})'

  def __str__(self):
    return str(self.name)

  def __eq__(self, other):
    if type(other) == 'int':
      other = Const(other)
    assert is_expr(other)
    return Eq(self, other)


class Eq(Expr):
  def __init__(self, left, right):
    assert is_expr(left) and is_expr(right)
    self.args = [left, right]

  def copy(self):
    return Eq(self.args[0], self.args[1])

  def __repr__(self):
    return f'Eq({self.args[0]}, {self.args[1]})'


if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
