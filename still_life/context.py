"""
>>> f = Field(3, 3, 2)
>>> x0, x1, x2, x3, x4, x5, x6, x7, x8 = f.unknowns
>>> f.setrow([0, 1, 0]); f.pp
.o.
???
???

>>> f.c(x3, 0).relax().pp
.o.
.oo
???
unsat from (0, 2) == 0 and sum(0, 2) == 3

>>> f.c(x3, 1).relax().pp
.o.
o.o
???

>>> pplist(branch_on(f, x3), numbers=True)
1)
.o.
o.o
???

>>> list(branch_on(f, x3)) == list(branch_on(f, x4)) == list(branch_on(f, x5))
True

>>> list(delete_duplicates((i % 3 for i in range(5))))
[0, 1, 2]

>>> get_checkable(f, [x3, x4, x5])
[Field(3, 3)]

>>> pplist(get_checkable(f, [x3, x4, x5]), numbers=True)
1)
.o.
o.o
???

>>> gs = get_checkable(f, [x3, x6]); ppfields(gs, numbers=True)
.o. .o. .o.
o.o ??? ???
??? .?? o??

Before update:
>>> gs[0].pp
.o.
o.o
???

After update:
>>> update_by_branch(gs, 0, x7).pp
.o.
o.o
?o?

Before update:
>>> gs[1].pp
.o.
???
.??

>>> str(get_checkable(gs[1], gs[1].unknowns)[0])
'.o.\\no.o\\n.o?'

>>> ppfields(get_checkable(gs[1], gs[1].unknowns, relaxed=False))
.o. .o. .o. .o. .o. .o. .o. .o. .o. .o.
.?? o?? ?.? ?o? ??. ??o ??? ??? ??? ???
.?? .?? .?? .?? .?? .?? ..? .o? .?. .?o

Too long list, how to make it shorter?
Let's see in deep than after this return back!

To implement this I created option undo_relax:
>>> ppfields(get_checkable(gs[1], gs[1].unknowns, relaxed=True, undo_relax=True))
.o. .o. .o. .o. .o. .o. .o.
o?? ?.? ??o ??? ??? ??? ???
.?? .?? .?? ..? .o? .?. .?o

>>> ppfields(get_checkable(gs[1], gs[1].unknowns, relaxed=True, undo_relax=False))
.o. .o. .o. .o. .o.
o.o ??? ??? ??? ???
.o? ..? .o? .?. .?o

>>> hs = get_checkable(gs[1], gs[1].unknowns, relaxed=True, undo_relax=False)
>>> len(hs)
5

>>> hs = get_checkable(gs[1], gs[1].unknowns, relaxed=True, undo_relax=False)

> Same list but with comments
1) this branch have assumption `x4 == 1` but `x4 == 0` not present in that list because x4 == 0 ==> unsat
.o.
o??
.??
2) this branch have assumption `x5 == 1` but `x4 == 0` not present in that list because x4 == 0 ==> unsat
.o.
?.?
.??

...

Before update:
>>> gs[2].pp
.o.
???
o??

After update:
>>> gs2_old = gs[2].copy()
>>> update_by_branch(gs, 2, x8).no
>>> assert gs2_old != gs[2]
>>> gs[2].pp
.o.
???
o?.

>>> unsat_by_branch(gs[2], x4)
False

>>> f2 = gs[0].copy()

Three kinds of model status:

  UNSAT (<==> ALL_FALSE)
  >>> f2.c(x6, 1).c(x8, 1).pp2
  . o .
  o . o
  o o o
  unsat from (3, 1) == 0 and sum(3, 1) == 3

  ALL_TRUE (ALL_TRUE implies SAT but not vise versa)
  >>> f2.c(x6, 1).c(x8, 0).pp2
  . o .
  o . o
  o o .
  valid

  UNKNOWN
  >>> f2.c(x6, 1).pp2
  . o .
  o . o
  o o ?

  >>> fs = [f2.c(x6, 1).c(x8, 1), f2.c(x6, 1).c(x8, 0), f2.c(x6, 1)]
  >>> ppfields(fs)
  .o. .o. .o.
  o.o o.o o.o
  ooo oo. oo?
  F   T   U

  >>> fs[2]._mut_relax()
  >>> ppfields(fs)
  .o. .o. .o.
  o.o o.o o.o
  ooo oo. oo.
  F   T   T

  >>> fs = delete_duplicates(fs)
  >>> ppfields(fs)
  .o. .o.
  o.o o.o
  ooo oo.
  F   T

  If `not f.check()` <==> to `unsat` may be we can replace `.check()` with `.is_unsat()`?
    Answer: No, because `not unsat` equivalent to `sat`, but if check() returns True
    then we have set-state: `{sat, unsat}` instead of `{sat}`.

  >>> fs_unsat = list(filter(lambda fi: not fi.check(), fs))
  >>> fs_unsat[0].pp2
  . o .
  o . o
  o o o
  unsat from (3, 1) == 0 and sum(3, 1) == 3

>>> ppfields(gs)
.o. .o. .o.
o.o ??? ???
?o? .?? o?.

If we think about list above as set of models (not distinct)
we can do some things (create method) for working only with sets.

Lemma: Any method that work for sets work for elements.

Let's create set of (distinct) models from single element by using cuteq's.
>>> f = Field(4, 4, 2)

# Fish-hook (eater1)
# see: https://en.wikipedia.org/wiki/Still_life_(cellular_automaton)
>>> f.setrow([1, 1, 0, 0])
>>> f.setrow([1, 0, 1, 0])
>>> f.setrow([0, 0, 1, 0])
>>> f.setrow([0, 0, 1, 1])
>>> f.pp2
o o . .
o . o .
. . o .
. . o o
valid

Let's forget information about values of several cells:
>>> for i, j, _ in f.cells():
...   if (i + j) % 2 == 0:
...     f[i, j] = CELL_UNKNOWN

>>> f.pp2
? o ? .
o ? o ?
? . ? .
. ? o ?

--Define variables(shortcuts)----------------------------
>>> x00, x02, x11, x13, x20, x22, x31, x33 = f.unknowns
>>> F, T, U = range(3)

---------------------------------------------------------
>>> def pprelax(fi): fi.pp2; print('relaxed'); fi.relax().pp2
>>> pprelax(f.c(x00, F))
. o ? .
o ? o ?
? . ? .
. ? o ?
relaxed
. o ? .
o . o o
o . o .
. o o .
unsat from (2, 2) == 1 and sum(2, 2) == 4

>>> pprelax(f.c(x00, T))
o o ? .
o ? o ?
? . ? .
. ? o ?
relaxed
o o . .
o . o .
. . o .
. . o o
valid

So easy task: solved with two assumptions (+relax).
Let's make it harder.
>>> for i, j, _ in f.cells():
...   if (i + j) % 3 == 2: # randomly changed :)
...     f[i, j] = CELL_UNKNOWN

>>> f.pp2
? o ? .
o ? o ?
? . ? ?
. ? ? ?

Let's try last case (when we see "valid")
>>> pprelax(f.c(x00, T))
o o ? .
o ? o ?
? . ? ?
. ? ? ?
relaxed
o o . .
o . o .
. . ? ?
. ? ? ?

Ok, not too easy.
To ensure that we don't have duplicates we can store all models in set.
Or we can incapsulate list inside a class as private object.
And allow only such set of operations that will not create duplicates.

>>> fs=[f.c(x00, T).relax()]
>>> count(fs[0].unknowns)
5

if by=None `destruct` selected first unknown var
>>> list(fs[0].unknowns)[0]
(2, 2)

>>> destruct(fs, 0, by=(2, 3))
>>> len(fs)
2
>>> assert fs[0][2, 3] == F
>>> assert fs[1][2, 3] == T
>>> fs[1].pp2
o o . .
o . o .
. . ? o
. ? ? ?

>>> def fill(i, j, n, relaxed=False):
...   update_by_branch(fs, n, (i, j), relaxed=relaxed)
>>> fill(2, 2, n=1); fs[1].pp2
o o . .
o . o .
. . . o
. ? ? ?

>>> destruct(fs, 1, by=(3, 2))
>>> assert len(fs) == 3
>>> assert fs[2][3, 2] == 1
>>> fs[1].pp2
o o . .
o . o .
. . . o
. ? . ?

>>> fill(3, 1, n=1); fs[1].pp2
o o . .
o . o .
. . . o
. . . ?

>>> fs[1]._mut_relax()
>>> assert len(fs) == 3
>>> clear_unsat(fs)
cleared: [1]

>>> assert len(fs) == 2

Show me rest cases
>>> ppfields(fs)
oo.. oo..
o.o. o.o.
..?. ...o
.??? .?o?

>>> fill(3, 3, n=1); fs[1].pp2
Traceback (most recent call last):
...
AssertionError: # of branches is 2 != 1

>>> fill(3, 1, n=1); fs[1].pp2
Traceback (most recent call last):
...
AssertionError: # of branches is 0 != 1

Now we can't use `clear_unsat`, but can use
`destruct`. May be better if after some
degging to depth f automatically marks as unsat
(It's equivalent to f.apply_experince()?)
>>> clear_unsat(fs)
cleared: []
>>> destruct(fs, 1)
>>> ppfields(fs)
oo.. oo.. oo..
o.o. o.o. o.o.
..?. ...o ...o
.??? ..o? .oo?
U    F    F

>>> clear_unsat(fs)
cleared: [1, 2]
>>> assert len(fs) == 1
>>> fill(2, 2, n=0); fs[0].pp2
o o . .
o . o .
. . o .
. ? ? ?

f[3,1] + f[3,2] + f[3,3] <= 2
if f[3,1] == 1 then f[3,2] == 1
i.e.
f[3,3] == 0
How to check that actions above is locally optimal?

Let's check that from f[3,1]==1 we arrive to absurd state
>>> fs[0].c((3,1), T).relax().pp2
o o . .
o . o .
. . o .
. o o o
unsat from (4, 2) == 0 and sum(4, 2) == 3

Then we manually conclude that f[3,1] == F.
How to not manually? Use destruct?
Or may exists another view of that problem?

Another way to do same thing:
> TODO: find new way without destruct.
* destruct
* f.apply_experience()
* ...

Using destruct:
>>> destruct(fs, 0)
>>> destruct(fs, 1)
>>> ppfields(fs)
oo.. oo.. oo..
o.o. o.o. o.o.
..o. ..o. ..o.
..?? .o.? .oo?
U    F    U

.copy() copies only list and links to elements
May be better is use .deepcopy()
>>> fs2 = deepcopy(fs)
>>> fs[2]._mut_relax()

>>> clear_unsat(fs)
cleared: [1, 2]

>>> ppfields(fs2)
oo.. oo.. oo..
o.o. o.o. o.o.
..o. ..o. ..o.
..?? .o.? .oo?
U    F    U

>>> for f in fs2:
...   f._mut_relax()

>>> ppfields(fs2)
oo.. oo.. oo..
o.o. o.o. o.o.
..o. ..o. ..o.
..oo .o.? .ooo
T    F    F

Test set of fields:
  >>> fs = set()
  >>> fs.add(f)

  Check elimination:
  >>> fs.add(f.copy())
  >>> len(fs)
  1

  Problem: We can't call fs[0], because it's a set.
  Possible ways to find solution: (Lemma: ways below must be unique, 
    i.e. have maximum "distance" between each other and be "optimal" instance for some parent equivalence class)
  A) change set to iset (list + deleting duplicates)
  B) use id's like `fs.get_by_name("f_start")` to call .by on particular states
  C) ...

  Actual ways : {{}, {A}, {B}, {C}, {A, B}, {A, C}, {B, C}, {A, B, C}}

  >>> from itertools import combinations
  >>> import itertools
  >>> A, B, C = (Way(ch) for ch in 'ABC')
  >>> actual_ways = [{}, {A}, {B}]
  >>> all_subsets = list(map(lambda t: sorted(t), concat([list(combinations([A,B,C], i)) for i in range(4)]))); all_subsets
  [[], [A], [B], [C], [A, B], [A, C], [B, C], [A, B, C]]

  >>> assert len(all_subsets) == 8

  SELECTED: {A, B}
  >>> as_element(list(filter(lambda xs: C not in xs and len(xs) == 2, all_subsets)))
  [A, B]

  # >>> destruct(fs, 0)
  # Link
"""
from functools import reduce
from still_life_patterns import *
from expr import Eq, Const, Var, Expr

STATUS_ALL_FALSE, STATUS_ALL_TRUE, STATUS_UNKNOWN = range(3)

def rem_elem():
  """
  >>> a = ["H", "e", "l"]
  >>> a.insert(1, 42)
  >>> a
  ['H', 42, 'e', 'l']
  >>> del a[1]
  >>> a
  ['H', 'e', 'l']
  """

def deepcopy(xs):
  return [x.copy() for x in xs]

def clear_unsat(fields, print_info=True):
  i = len(fields) - 1
  if print_info:
    cleared = set()
  while i >= 0:
    if not fields[i].check():
      if print_info:
        cleared.add(i)
      del fields[i]
    i -= 1
  if print_info:
    print(f'cleared: {sorted(cleared)}')

def destruct(fields, ind=0, by=None):
  assert len(fields) > 0
  field = fields[ind]
  var_index = by
  if by is None:
    var_index = list(field.unknowns)[0]
  fields[ind] = field.cuteq(var_index, 0)
  alt = field.cuteq(var_index, 1)
  fields.insert(ind+1, alt)


def as_element(xs):
  """Convert list [x] to single element x"""
  assert len(xs) == 1, "list is empty or contains several elements"
  return xs[0]

class Way:
  def __init__(self, name):
    self.name = name

  def __lt__(self, other):
    return self.name < other.name

  def copy(self):
    return Way(self.name)

  def __repr__(self):
    return f'{self.name}'

  def __eq__(self, other):
    assert isinstance(other, Way)
    return self.name == other.name

  def __hash__(self):
    return hash(self.name)

class Context:
  def __init__(self, field, hs=None):
    self.field = field
    self.hs = [] if hs is None else hs

  def copy(self):
    chs = [h.copy() for h in self.hs]
    return Context(self.field, hs=chs)

  def add(self, h) -> bool:
    if h not in self:
      self.hs.append(h)

  def check(self):
    return all(h.check(self.field) for h in self.hs)

  def __contains__(self, h):
    """ TODO: implement contains """
    return False
  
  def __repr__(self):
    return f'Context(hs={repr(self.hs)})'

def test_context():
  """
  >>> f = Field(3, 3)
  >>> ctx = Context(f)
  >>> x = (0, 0)
  >>> ctx.add(Eq(Const(x), Const(0)))
  >>> ctx.check()
  True
  >>> ctx.hs
  [Eq((0, 0), 0)]

    Possible better representation:
    > ctx.hs
    [Eq(x, 0)]
    OR
    [x == 0]

  Runs .relax() method o the field ctx.field
  > ctx.relax()

  Field is a just part of `context` (set of hypotheses)
    Note: hyps can be calculated, i.e. set can be lazy computable, computable by model, constraint programming on hyps also possible... but hard

  I think might be better if context.hyp_set() just links to field as `set`.
  i.e. field from that perspective can viewed only as `hyp_set`
    > ctx.get_full_set_of_hyps() == (ctx.field.to_hyp_set() + ctx.to_hyp_set())

  >>> ctx.field.pp2
  ? ? ?
  ? ? ?
  ? ? ?

  We can prove that specific order of using hypotheses is less optimal than other
  > ctx.define_order_of_hyps_by_proof()
  """


def test_change_by_looking():
  """
  -- Define variables (links to field substates) and lamdas --
  Note: state = cartesian_product(substates) + something_useful

  >>> def ch(f): return 7 in sum_cells(f.cell_sum_tuple(1, 1))

  TODO: replace this behaviour using Sum((1, 1), 7)
  > center = (1, 1)
  > ctx.add(Eq(Sum(center), 7))
    OR
    > ctx.add(Eq(Sum(center), Const(7)))

  >>> f = Field(3, 3, CELL_UNKNOWN, check_field=ch)
  >>> x, y, z = ((0, j) for j in range(3))

  -- Change main state by looking --

  > f.pp2
  start state

  > f.change_state(direction)
      .prove_by_looking(sequence of substates|policy_function)
  next state
    OR
    > f.add_hyp(H).from_looking_on(substates=[x1, x2, x3])
  """


def irange(a, b):
  return range(a, b+1)

def sum_cells(tup):
  val, lo, hi = tup
  if val == 2:
    return irange(lo, hi+1)
  return irange(lo+val, hi+val)


def ex_proof_by_looking():
  """
  >>> def ch(f): return 7 in sum_cells(f.cell_sum_tuple(1, 1))
  >>> f = Field(3, 3, 2, check_field=ch)
  >>> x, y, z = ((0, j) for j in range(3))
  
  >>> f.c(x, 0).c(y, 0).pp2
  . . ?
  ? ? ?
  ? ? ?
  >>> f.check()
  True
  >>> f1 = f.c(x, 0).c(y, 0)
  >>> f1.check_field(f1)
  True
  
  >>> f2 = f.c(x, 0).c(y, 0).c(z, 0)
  >>> f2.check_field(f2)
  False
  
  >>> f2.cell_sum_tuple(1, 1)
  (2, 0, 5)
  
  >>> f2.pp2
  . . .
  ? ? ?
  ? ? ?
  unsat by check_field
  
  >>> f1.relax().pp2
  . . o
  o o o
  o ? ?
  unsat from (1, 1) == 1 and 4 <= sum(1, 1) <= 6
  
  >>> r = lambda g: g.relax()
  >>> ppfields(map(r, gen_branch_on(f.c((1,1), 0), x)))
  Empty
  >>> f[1,1] = 1
  >>> ppfields(map(r, gen_branch_on(f.c((1,0), 0), x)))
  Empty
  
  Let's check proof
  >>> f.c((1,0), 0).c(x,0).relax().pp2
  . o o
  . o o
  o ? ?
  unsat from (1, 1) == 1 and 4 <= sum(1, 1) <= 6
  
  >>> f.c((1,0), 0).c(x,1).relax().pp2
  o o .
  . o o
  ? ? ?
  unsat from (0, 2) == 0 and sum(0, 2) == 3
  
  >>> f[1, 0] = 1
  
  Print main state
  >>> f.pp2
  ? ? ?
  o o ?
  ? ? ?
  
  Qed by branching on x:
  >>> ppfields(get_checkable(f, [x]))
  Empty
  
  Details:
  >>> ppfields([f.c(x,i).relax() for i in bools()])
  ..o oo?
  ooo oo?
  o?? ..o
  F   F
  
  Proofs:
  >>> pplist([f.c(x,i).relax() for i in bools()], numbers=True)
  1)
  ..o
  ooo
  o??
  unsat from (1, 1) == 1 and 4 <= sum(1, 1) <= 6
  2)
  oo?
  oo?
  ..o
  unsat from (1, 1) == 1 and 4 <= sum(1, 1) <= 6
  """

def ppfields(fields, numbers=False, delimiter=' '):
  """
  TODO: How to replace {:<3} with {:<width} ?
  >>> ps = '{:<3} {:<3} {:<3}'
  >>> indexes = ['0)', '1)', '2)']
  >>> print(ps.format(*indexes)+'|<- space here')
  0)  1)  2) |<- space here

  > ppfields(fields)
  1)  2)  3)  4)  5)
  .o. .o. .o. .o. .o.  | . o o . <-- idea of .pp2
  o.o ??? ??? ??? ???  | o . . o
  .o? ..? .o? .?. .?o  | . o o .
  T   U   F

  T ~ all_true
  F ~ unsat
  U ~ unknown
  """
  fields = list(fields)
  if len(fields) == 0:
    print("Empty")
    return
  # print top line numbers: 1) 2) 3) ...
  # TODO: add code here
  # print fields
  lines = fields[0].__str__(show_status=False).split()
  w, h = fields[0].w, fields[0].h
  assert len(lines[0]) == fields[0].w
  assert len(lines) == fields[0].h
  for field in fields[1:]:
    lines_tmp = field.__str__(show_status=False).split()
    assert len(lines_tmp) == len(lines)
    for i, s in enumerate(lines_tmp):
      lines[i] += delimiter + s
  print('\n'.join(lines))
  # print bottom line: F  T  U ...
  only_unknowns = True
  s = ''
  for field in fields:
    status = field.get_model_status()
    if status == STATUS_ALL_TRUE:
      only_unknowns = False
      s += 'T'
    elif status == STATUS_ALL_FALSE:
      only_unknowns = False
      s += 'F'
    elif status == STATUS_UNKNOWN:
      s += 'U'
    else:
      assert False

  spaces = ' '*(w)
  s = spaces.join(s)
  if not only_unknowns:
    print(s)


def update_by_branch(fields, ind, var_index, relaxed=True):
  field = fields[ind]
  branches = branch_on(field, var_index, relaxed=relaxed)
  assert len(branches) == 1, f'# of branches is {len(branches)} != 1'
  fields
  fields[ind] = branches[0]
  return branches[0] # need to use `.pp`, but also drawback: we must define `.no` to emulate `return None`


def unsat_by_branch(field, var_index):
  branches = branch_on(field, var_index)
  return len(branches) == 0


def get_checkable(field, var_list, relaxed=True, undo_relax=False):
  return delete_duplicates(concat(list(gen_branch_on(field, x_var, checkable=True, relaxed=relaxed, undo_relax=undo_relax)) for x_var in var_list))


def concat(xss):
  """
  >>> concat([])
  []
  >>> concat([[]])
  []
  >>> concat([[1, 2], [3], [4, 5], []])
  [1, 2, 3, 4, 5]
  """
  return reduce(lambda xs, ys: xs + ys, xss, [])


def delete_duplicates(xs, equal_func=None) -> list:
  """
  >>> [list(delete_duplicates( (i*i + k) % 3 for i in range(10) )) for k in range(3)]
  [[0, 1], [1, 2], [2, 0]]

  >>> [sorted(set( (i*i + k) % 3 for i in range(10) )) for k in range(3)]
  [[0, 1], [1, 2], [0, 2]]
  """
  if equal_func is None:
    return list(gen_delete_duplicates(xs))
  else:
    result = []
    for i, x in enumerate(xs):
      unique = True
      for j in range(i+1, len(xs)):
        y = xs[j]
        if equal_func(x, y):
          unique = False
          break
      if unique:
        result.append(x)
    return result
    #     if y not is None:
    #       if equal_func(x, y):
    #         xs[j] = None


def gen_delete_duplicates(xs):
  """ Delete duplicated from iterable """
  hashes = []
  for x in xs:
    hash_value = hash(x)
    if hash_value not in hashes:
      hashes.append(hash_value)
      yield x


def gen_branch_on(field, var_index, checkable=False, relaxed=True, undo_relax=False):
  assert not (relaxed == False and undo_relax == True), "`undo_relax` can be used when `relaxed=True`"
  for i in bools():
    child = field.cuteq(var_index, i)
    # child._mut_undo_relax() can be faster if we need backtracking
    if relaxed:
      if undo_relax:
        new_child = child.relax()
        if new_child.check(): # forward check after relax
          yield child
      else:
        child._mut_relax()
        if child.check(): # check after relax
          yield child
    else: # not relaxed
      # simple check without relax
      if child.check():
        yield child


def branch_on(field, var_index, checkable=False, relaxed=True):
  return list(gen_branch_on(field, var_index, checkable, relaxed))

def example_work_with_symmetries():
  """
  To work with symmetries {rotate, mirror} we can use assumptions.
  For example implication: 
  [a, b, c]
  assume: a <= c ~ Implies(a, c) ~ a ==> c, where `<=` less or equal

  Note: This idea can be generalized|expressed as `wlog` (without loss of generality) tactic (like in Lean and Coq)
  >>> lst = \
      [
  ...   [0, 0, 0],
  ...   [0, 0, 1],
  ...   [0, 1, 0],
  ...   [0, 1, 1],
  ...   [0, 0, 1],
  ...   [1, 0, 1],
  ...   [0, 1, 1],
  ...   [1, 1, 1],
  ... ]
  >>> lst_sym = delete_duplicates(lst, equal_func=lambda x,y: x == y)
  >>> pplist(lst_sym)
  [0, 0, 0]
  [0, 1, 0]
  [0, 0, 1]
  [1, 0, 1]
  [0, 1, 1]
  [1, 1, 1]

  >>> lst = \
      [
  ...   [0, 0, 0],
  ...   [0, 0, 1],
  ...   [0, 1, 0],
  ...   [0, 1, 1],
  ...   [1, 0, 0],
  ...   [1, 0, 1],
  ...   [1, 1, 0],
  ...   [1, 1, 1],
  ... ]

  >>> a = [1,2,3]
  >>> a[::-1]
  [3, 2, 1]

  >>> def mirror(xs): return xs[::-1]
  >>> lst2 = delete_duplicates(lst, equal_func=lambda x,y: x == y or mirror(x) == y)
  >>> lst_sym2 = [(e if e[0] <= e[2] else mirror(e)) for e in lst2]
  >>> pplist(lst_sym2)
  [0, 0, 0]
  [0, 1, 0]
  [0, 0, 1]
  [1, 0, 1]
  [0, 1, 1]
  [1, 1, 1]

  >>> lst_sym == lst_sym2
  True
  """

def infinite_plane():
  """
  >>> f = Field(10,7,2)
  >>> f.setrow([int(i != 5) for i in range(10)], row_index=2)

  >>> r = [(1, j) for j in range(10)]
  >>> f.outer_value = 2

  > u = 4
  > f[r[u]] = 1
  > f[r[u+2]] = 1

  > f[r[8]] = 0
  > f._mut_relax()

  > f[r[0]] = 1
  > f.pp2
  ? ? ? ? ? ? ? ? ? ?
  ? ? ? ? ? ? ? ? ? ?
  o o o o o . . . . .
  ? ? ? ? ? ? ? ? ? ?
  ? ? ? ? ? ? ? ? ? ?
  """

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
