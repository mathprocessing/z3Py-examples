def inc(x):
  return x + 1

def la_test(func, n):
  """
  >>> la_test(inc, 42)
  43
  >>> La(inc).run(42)
  43
  """
  return func(n)

class La:
  def __init__(self, func):
    self.func = func
    
  def run(self, n):
    return self.func(n)

if __name__ == '__main__':
  import doctest
  flags = {
    'optionflags': 
      doctest.FAIL_FAST
  }
  doctest.testmod(**flags)
