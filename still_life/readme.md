# Test
``` bash
pytest --doctest-modules
pytest --doctest-modules --doctest-report only_first_failure
pytest --doctest-modules --doctest-report only_first_failure -W ignore::DeprecationWarning
```
See: 
* [pytest + doctest](https://docs.pytest.org/en/7.1.x/how-to/doctest.html)
* [pytest: How to disable deprecation warnings](https://stackoverflow.com/questions/40710094/how-to-suppress-py-test-internal-deprecation-warnings)

# Run single file
```bash
$ python3 -u -Xutf8 still_life_patterns.py
```

## Interactive mode
```bash
$ python3 -i -Xutf8 still_life_patterns.py
```

# Ideas
```
TODO: Make extension of vscode to view how solver solve cli represented still life puzzles.
.oo.
o???
.???
ENABLE SOLVER: 1   # solver enabled, and now we can see all tries in field repr above (not only in terminal)
ENABLE SOLVER:     # Just delete `1` and solver stop actions

User can edit field in real-time at the same time that solver changes hypothesis context.
This demonstrates human-machine real-time collaboration.
Note: Why we want extension of vscode? Because we need "real-time" collaboration.
      If exists other solution for real-rime problem (may be some multitreading framework) also possible to use it.

Example task "find shorter proof for something".
This task very hard (NP-hard) for machines but if task almost solved then solver can "close goal" by some tactic.
Also possible to define constraint on subgoals (how to divide task on easy solvable parts).

TODO: Maximally Abstract Chess(Checkers, C...) Engine = MACEN
```

# Books and articles  
* Constraint Programming and Hybrid Formulations for Three Life Designs
  Robert Bosch & Michael Trick 

* The still-Life density problem and its generalizations https://arxiv.org/pdf/math/9905194.pdf
  Noam D. Elkies

