"""
>>> f = Field(3, 3, 2); f.pp
???
???
???

Let's find all non-symmetric solutions.
if we use horizontal axis symmetry we can
glue state
>>> f.c((0,0),0).c((f.h-1,0),1).pp
.??
???
o??

with state
>>> f.c((0,0),1).c((f.h-1,0),0).pp
o??
???
.??

Or use delete_duplicates with some additional
equivalence (rotate + translation + mirror)
>>> fs = [f]

>>> lu, ru = (0, 0), (0, f.w - 1)

Set by `H_vertical_axis_symmetry: f[lu] <= f[ru]`
>>> fs[0].check_field = lambda field: le(field[lu], field[ru])

>>> for _ in range(2): destruct(fs, 0)
>>> ppfields(fs)
..? .o? o??
??? ??? ???
??? ??? ???

>>> old_value = fs[2][ru]

>>> relax_all(fs)
>>> ppfields(fs)
..? .o? o.o
??? ??? ooo
??? ??? ???
U   U   F

You can see that fs[2][ru] changes, because an additional constraint has been created (check_field)
>>> assert fs[2][ru] != old_value

Interesting thing is that we can still create new symmetries by assumptions!
Let's create a hypothesis context with diagonal symmetry:
>>> destruct(fs, 0, by=(1, 0)) # So simple, isn't it?
>>> ppfields(fs)
..? ..? .o? o.o
.?? o?? ??? ooo
??? ??? ??? ???
U   U   U   F

Let's focus only on first field `fs[0]`
> have_diagonal_symmetry(fs[0])
True
> get_symmetries_set(fs[0])
{diag_sym}

>>> ld, rd = (f.h - 1, 0), (f.h - 1, f.w - 1)

We can add hypothesis H_diag: `fs[0][ld] <= fs[0][ru]`
>>> fs[0].check_field = lambda field: le(field[ld], field[ru])

And branch on `ld` to check that `H_diag` works:
>>> destruct(fs, 0, by=ld)
>>> ppfields(fs)
..? ..? ..? .o? o.o
.?? .?? o?? ??? ooo
.?? o?? ??? ??? ???
U   U   U   U   F

>>> relax_all(fs)
>>> ppfields(fs)
..? ..o ..? .o? o.o
.?? .oo o?? ??? ooo
.?? o?? ??? ??? ???
U   F   U   U   F

>>> assert fs[1][ru] == 1, "MethodGroup{check_field + relax} give unxprected behaviour"

Note: We can't use `clear_unsat` because possible that we loose some useful symmetries|equivalences,
      but if we prove that we can't loose, after that, of course, we can use it.

Our next target may be "hunt" on other symmetries:
* Try to move current state to 
  "`exists i, j:index | i <> j :: mirror(fs[i]) = fs[j]` can be derived from context"
  Note: Instead `mirror` one can substitute any symmetry bijection.

* TODO: ...

This two states can be glued together by using diagonal symmetry:
>>> ppfields(fs[2:4])
..? .o?
o?? ???
??? ???

But before gluing we need to destruct fs[3] on two subcases:
>>> destruct(fs, 3, by=(1, 0))
>>> ppfields(fs[2:5])
..? .o? .o?
o?? .?? o??
??? ??? ???

Now we can glue fs[2] with fs[3]
>>> assert 2 != 3, "we can't glue f[i] with itself"
>>> glue(fs, 2, 3, by=diagonally_symmetrical)
>>> ppfields(fs[2:5])
..? .o? o.o
o?? o?? ooo
??? ??? ???
U   U   F

Now our interface is: {
  distruct by variable (substate),
  glue by equivalence relation,
  ...
  new may be:
  glue using induction on ...,

  I think is really hard to extend this interface, what if this can be automated?
  If we like to think recursively:
    For example we can use `destruct and glue` to find `I - (destruct + glue)`
  if not:
    ...
}

TODO: add more abstract operators like two above

Remember: Our goal is to change all unknown (`U`) statuses in fs to `F` or `T`.

Let's view all subcases:
>>> ppfields(fs)
..? ..o ..? .o? o.o
.?? .oo o?? o?? ooo
.?? o?? ??? ??? ???
U   F   U   U   F

We can sort them by model_status and move F's to right:
>>> fs = sorted_by_status(fs)
>>> ppfields(fs)
..? ..? .o? ..o o.o
.?? o?? o?? .oo ooo
.?? ??? ??? o?? ???
U   U   U   F   F

For user is a really great feature: print all possible actions (including using symmetries) in current state `S`

>>> destruct(fs, by=(0, 2))
>>> print(); ppfields(fs)
<BLANKLINE>
... ..o ..? .o? ..o o.o
.?? .?? o?? o?? .oo ooo
.?? .?? ??? ??? o?? ???
U   U   U   U   F   F

>>> relax_all(fs)
>>> print(); ppfields(fs)
<BLANKLINE>
... ..o ..? .o? ..o o.o
.?? .oo o?? o.? .oo ooo
.?? .?? ??? ??? o?? ???
U   F   U   U   F   F

In fs[0] we can use diag + translation symmetry
>>> destruct(fs, 0, by=(1, 1))
>>> destruct(fs, 0, by=(2, 2))
>>> print(); ppfields(fs)
<BLANKLINE>
... ... ... ..o ..? .o? ..o o.o
..? ..? .o? .oo o?? o.? .oo ooo
.?. .?o .?? .?? ??? ??? o?? ???
U   U   U   F   U   U   F   F

fs[1] inside fs[2] ==> fs[1] already contains in fs[2]
fs[1] can be deleted
> glue_with(fs, 1, 2)
>>> del fs[1]
>>> destruct(fs, 1, by=(2, 1))
>>> print(); ppfields(fs)
<BLANKLINE>
... ... ... ..o ..? .o? ..o o.o
..? .o? .o? .oo o?? o.? .oo ooo
.?. ..? .o? .?? ??? ??? o?? ???
U   U   U   F   U   U   F   F

>>> destruct(fs, 4, by=(2, 0))
>>> relax_all(fs)
>>> fs = sorted_by_status(fs)
>>> print(); ppfields(fs)
<BLANKLINE>
... ..? .o? ... ... ..o ..? ..o o.o
.o? o?? o.? ... .oo .oo oo? .oo ooo
.o? o?? ??? ... ..o .?? .o? o?? ???
U   U   U   T   F   F   F   F   F

fs[0] inside fs[1]
>>> del fs[0]
>>> destruct(fs, 0, by=(1, 2))
>>> ppfields(fs)
..? ..? .o? ... ... ..o ..? ..o o.o
o?. o?o o.? ... .oo .oo oo? .oo ooo
o?? o?? ??? ... ..o .?? .o? o?? ???
U   U   U   T   F   F   F   F   F

>>> fs[0]._mut_relax()
>>> fs[0].pp2
. . .
o ? .
o ? ?

fs[2] have diagonal symmetry
>>> destruct(fs, 2, by=(0, 2))
>>> destruct(fs, 2, by=(2, 0))
>>> ppfields(fs[3:5])
.o. .oo
o.? o.?
o?? ???

fs[3] in fs[4] (we use duag_symm by two applying distructs + glue)
>>> del fs[3]
>>> destruct(fs, 1, by=(1, 1))
>>> destruct(fs, 0, by=(1, 1))
>>> relax_all(fs)
>>> fs = sorted_by_status(fs)
>>> print(); ppfields(fs)
<BLANKLINE>
... .o. .oo ... ... ... ..o ... ..o ..? ..o o.o
oo. o.o o.o ... o.. o.o ooo .oo .oo oo? .oo ooo
o?? .o? ??. ... ooo ooo o?? ..o .?? .o? o?? ???
U   U   U   T   F   F   F   F   F   F   F   F

>>> destruct(fs, 2, by=(2, 1))
>>> destruct(fs, 1)
>>> destruct(fs, 0, by=(2, 1))
>>> print(); ppfields(fs[:6])
<BLANKLINE>
... ... .o. .o. .oo .oo
oo. oo. o.o o.o o.o o.o
o.? oo? .o. .oo ?.. ?o.
U   U   T   T   U   U

fs[3] in fs[5]
>>> del fs[3]
>>> destruct(fs, 4)
>>> relax_all(fs)
>>> fs = sorted_by_status(fs)
>>> print(); ppfields(fs)
<BLANKLINE>
... .o. .oo .oo ... ... .oo ... ... ..o ... ..o ..? ..o o.o
oo. o.o o.o o.o ... oo. o.o o.. o.o ooo .oo .oo oo? .oo ooo
oo. .o. .o. oo. ... o.o o.. ooo ooo o?? ..o .?? .o? o?? ???
T   T   T   T   T   F   F   F   F   F   F   F   F   F   F

>>> len(fs)
15

You can see 5 solutions.
>>> clear_unsat(fs, False)
>>> assert len(fs) == 5
>>> print(); ppfields(fs)
<BLANKLINE>
... .o. .oo .oo ...
oo. o.o o.o o.o ...
oo. .o. .o. oo. ...
T   T   T   T   T

But we can find only solutions with fixed size
to reduce translation symmetries.
i.e. we assume that always f[0, 0] = 1 and
no empty row|column in border.

For example this solutions have size 3x3:
>>> ppfields(fs[1:-1])
.o. .oo .oo
o.o o.o o.o
.o. .o. oo.
T   T   T

Let's find 4x3 fixed size solutions:
>>> f = Field(4, 3)
>>> f[0, 0] = 1
>>> fs = [f]
>>> destruct(fs, by=(2, 0))
>>> ppfields(fs)
o??? o???
???? ????
.??? o???

> relax_all(fs) # don't call relax because we want to glue using vmirror
>>> destruct(fs, by=(2, 3))
>>> destruct(fs, 1, by=(0, 3))
>>> ppfields(fs)
o??? o??. o??o o???
???? ???? ???? ????
.??. .??o .??o o???

fs[2] in fs[3] (also fs[3] easily provable)
>>> del fs[2]

Now we can call relax.
But stop... we depend on relax, this a bad thing.
This implies that we must save states from the past
in history.
>>> history = [fs[2].copy()]
>>> relax_all(fs)
>>> ppfields(fs)
o??? o??. oo??
???? ???? .o??
.??. .??o oo.?
U    U    F

Apply same trick to column[3] in fs[0]
>>> destruct(fs, by=(1, 3))
>>> destruct(fs, by=(2, 2))
>>> destruct(fs, 2, by=(2, 2))
>>> ppfields(fs)
o??? o??? o??? o??? o??. oo??
???. ???. ???o ???o ???? .o??
.?.. .?o. .?.. .?o. .??o oo.?
U    U    U    U    U    F

From non-zero rows, columns:
>>> fs[0][0, 3] = 1
>>> fs[0][2, 1] = 1
>>> fs[1][0, 3] = 1
>>> fs[2][2, 1] = 1
>>> ppfields(fs)
o??o o??o o??? o??? o??. oo??
???. ???. ???o ???o ???? .o??
.o.. .?o. .o.. .?o. .??o oo.?
U    U    U    U    U    F

vmirror: (fs[0] + assumption) in fs[1]
>>> destruct(fs, by=(1, 0))
>>> del fs[0]
>>> ppfields(fs)
o??o o??o o??? o??? o??. oo??
o??. ???. ???o ???o ???? .o??
.o.. .?o. .o.. .?o. .??o oo.?
U    U    U    U    U    F

>>> relax_all(fs)
>>> fs = sorted_by_status(fs)
>>> ppfields(fs)
o??? o??? o??. o?oo o.oo oo??
???o ??.o ???? o.o. ooo. .o??
.o.. .?o. .??o .o.. ..o. oo.?
U    U    U    F    F    F

Instead of symmetry: [diag, mirror, rotate, ...]
also possible to use graph isomorphisms.
It may help to reduce code.
One function to all symmetries.
Example: 
* Task A isomorphic to task B.
  Then we can delete any of two.
* Task A contains in task B.
  We can delete only A.
  
>>> destruct(fs, 0, by=(1, 1))
>>> destruct(fs, 2, by=(0, 1))
>>> destruct(fs, 4, by=(0, 2))
>>> destruct(fs, 4, by=(1, 3))
>>> relax_all(fs)
>>> fs = sorted_by_status(fs)
>>> ppfields(fs)
o?.. oo.o oo?? o.?? oo.o o?.. o.o. o?oo o.oo oo??
???o o.oo .ooo oo.o ??.o ??o. oo.o o.o. ooo. .o??
.??o .o.. .o.. .oo. .?o. .?oo ..oo .o.. ..o. oo.?
U    F    F    F    F    F    F    F    F    F

>>> destruct(fs, by=(1, 1))
>>> destruct(fs, 1, by=(1, 2))
>>> relax_all(fs)
>>> len(fs)
12

>>> clear_unsat(fs, False)
>>> ppfields(fs)
oo..
o..o
..oo
T

>>> solutions = [fs[0].copy()]

Last hyp:
  in corner exist T value.
Now we apply inverted hyp:
  forall corners value <> T.
+ Fixed size hyp:
  sum in border rows|columns > 0.

Check other cases:
>>> f = Field(4, 3)
>>> f.setrow([0,2,2,0])
>>> f.setrow([1,2,2,1])
>>> f.setrow([0,2,2,0])
>>> fs = [f]
>>> ppfields(fs)
.??.
o??o
.??.

Use vmirror.
>>> destruct(fs, by=(0, 1))
>>> destruct(fs, by=(0, 2))
>>> print(); ppfields(fs)
<BLANKLINE>
.... ..o. .o?.
o??o o??o o??o
.??. .??. .??.

From vmirror: fs[1] in fs[2]
>>> del fs[1]

From fixed size hyp:
>>> del fs[0]
>>> ppfields(fs)
.o?.
o??o
.??.
>>> relax_all(fs)
>>> ppfields(fs)
.o?.
o.?o
.o?.

Let's use check_field for hmirror:
>>> fs[0].check_field = lambda field: le(field[0, 2], field[2, 2])
>>> destruct(fs, by=(2, 2))
>>> relax_all(fs)
>>> ppfields(fs)
.oo. .oo.
o.?o o..o
.o.. .oo.
F    T

>>> clear_unsat(fs, False)
>>> solutions.append(fs[0].copy())
>>> ppfields(solutions)
oo.. .oo.
o..o o..o
..oo .oo.
T    T

-----------------------------------

For user is a really great feature: print all possible actions (including using symmetries) in current state `S`
> get_possible_actions()
[cuteq((0, 0)), cut(H), use(diag_symm), ..., squash_states(A, B), squash_by_induction(infinite_grid(type='2D')), ...]

squash_states - use some equivalences
squash_by_induction - use induction_step where possible in all combinations and after some equivalences (Now I don't know how exacly it will work)

> get_shortest_proof_of_unsat()
[action1, action2, ...]

Get the shortest proof that a given state has as many solutions as it has.
> get_shortest_proof(current_state, set_of_lemmas, score_function)
About `score_function :: action -> real`: Map each action to score value.

"""
from still_life_patterns import *
from expr import Eq, Const, Var, Expr

from context import destruct,\
  clear_unsat, deepcopy, ppfields


def solve(fields, order):
  """
  In each state we have policy function that
  indicates the best action (or better than some min_bound).

  We focus on two actions:
  1. Destruct by hyp
  2. Glue by equivalence

  >>> f = Field(4, 3)
  >>> f.setrow([2, 2, 2, 2])
  >>> f.setrow([2, 2, 2, 2])
  >>> f.setrow([0, 2, 2, 2])
  >>> order = f.unknowns
  >>> fs = Fields([f])
  >>> fs.destruct(0, by=(0, 0))
  >>> fs.destruct(0, by=(1, 0))
  >>> fs.pp
  .??? .??? o???
  .??? o??? ????
  .??? .??? .???
  

  """
  f = field

  def solve_aux(max_depth):
    pass


def relax_all(fields):
  # if check() already exists in relax, we don't need that check
  # And (obviously) it have more time to run (TODO: create benchmark)
  for f in fields: 
    f._mut_relax()

def sorted_by_status(fields, reverse=True):
  return sorted(fields, key=lambda field: field.get_model_status(), reverse=reverse)

def glue(fields: list, indA: int, indB: int, by):
  """ Glue two states together if they equivalent (up to symmetry, some other equivalences, ...) to each other
  Example: 
  After `glue(fs, 0, 1, by=diagonally_symmetrical)`
  if diagonally_symmetrical(fs[0], fs[1]):
    fs[0] do not changed
    fs[1] will be deleted
  """
  assert by(fields[indA], fields[indB]), f"Cannot glue because this constraint is false: {by.__name__}"
  del fields[indB]

def try_glue(fields: list, indA: int, indB: int, by) -> bool:
  """ This can be helpful for automation
  """
  if by(fields[indA], fields[indB]):
    del fields[indB]
    return True
  return False

def le(a: "0|1|2", b: "0|1|2") -> bool:
  if a == 1:
    return b != 0
  return True

def get_symmetries_set(field: Field) -> set:
  """ Return set of symmetries that map context to itself (~ identity)
  whole set: {h_sym, v_sym, ...}
  """

def diagonally_symmetrical(f1: Field, f2: Field) -> bool:
  field_context_symm = (diag_mirror(f1) == f2)
  # don't know how check check_field_symm
  # Let's reset it
  f1.check_field = lambda f: True
  f2.check_field = lambda f: True

  # Now we know that it's true
  check_field_symm = True

  # Or we can mirror check_field
  # f2.check_field = lembda f: f2.check_field(diag_mirror(f))
  # But this is slow for perfomance

  return field_context_symm and check_field_symm

def diag_mirror(field: Field) -> Field:
  """
  >>> f = Field(3, 3, 2)
  >>> f.setrow([0, 1, 2])
  >>> f.setrow([2, 2, 1])
  >>> f.setrow([0, 0, 0])
  >>> f.pp2
  . o ?
  ? ? o
  . . .
  >>> diag_mirror(f).pp2
  . ? .
  o ? .
  ? o .
  """
  assert field.w == field.h, "Width and height of field are not equal to each other."
  field_diag = field.copy()
  for i, j, _ in field_diag.cells():
    if i != j:
      field_diag[i, j] = field[j, i]
  return field_diag

class Fields(list):
  def copy(self):
    return Fields([field.copy() for field in self])

  @property
  def trues(self):
    return Fields(filter(lambda field: field.get_model_status() == MODEL_STATUS_TRUE, self))

  @property
  def falses(self):
    return Fields(filter(lambda field: field.get_model_status() == MODEL_STATUS_FALSE, self))

  @property
  def unknowns(self):
    return Fields(filter(lambda field: field.get_model_status() == MODEL_STATUS_UNKNOWN, self))

  def clear_unsat(self):
    clear_unsat(self, False)

  def destruct(self, n, by=None):
    destruct(self, n, by)

  def sort_by_status(self):
    self.sort(key=lambda field: 2 - field.get_model_status())

  def mut_relax(self):
    relax_all(self)

  @property
  def pp(self):
    ppfields(self)

  def vmirror(self, n):
    """ flip over vertical axis
    >>> (3 // 2, 4 // 2)
    (1, 2)
    """
    f = self[n]
    if f.w <= 1:
      return
    assert f.w >= 1
    for j in range(f.w // 2): # 3 -> 1 case, 4 -> 2 cases
      for i in range(f.h):
        a = i, j
        b = i, f.w - j - 1
        f[a], f[b] = f[b], f[a]

  def hmirror(self, n):
    """ flip over horizontal axis """
    f = self[n]
    if f.h <= 1:
      return
    assert f.h >= 1
    for j in range(f.w):
      for i in range(f.h // 2):
        a = i, j
        b = f.h - i - 1, j
        f[a], f[b] = f[b], f[a]

  def inside(self, n1, n2):
    return self[n1] <= self[n2]

  def inside_pairs(self):
    """
    > assert fs[1] <= fs[2]
    > fs.inside_pairs()
    > [(1, 2), ..]
    TODO: we can use transitivity to reduce number of steps
    """
    rng = range(len(self))
    return [(n1, n2) for n1 in rng for n2 in rng if n1 != n2 and self.inside(n1, n2)]

  def glue(self, n1, n2) -> bool:
    assert n1 >= 0 and n2 >= 0
    if n1 == n2:
      return # fields.glue(x, x) do nothing
    assert self.inside(n1, n2), f"Can't glue {n1} to {n2}"
    del self[n1]

  def __contains__(self, field):
    """
    >>> f = Field(3, 3)
    >>> f[0, 0] = 0
    >>> fs = Fields([f])
    >>> g = Field(3, 3)
    >>> g in fs
    False
    >>> g[0, 0] = 1
    >>> g in fs
    False
    >>> f in fs
    True
    >>> all((f.cuteq((0, 1), v) in fs) for v in bools())
    True
    """
    assert isinstance(field, Field)
    for f in self:
      if field <= f:
        # if exists any field then contains (less or equal)
        return True
    # TODO: we can glue(union) all fields to one `funion` and after call `field in funion`
    # Add can return Ternary {F, T, U}
    # I think we don't need this TODO because `self` is like set_union(f1, f2, ...)
    # and we know `exists i, g in f_i <==> g in set_union(f1, f2, ...)`
    return False

def mut_vmirror(f):
  """ flip over vertical axis
  >>> (3 // 2, 4 // 2)
  (1, 2)
  """
  if f.w <= 1:
    return
  assert f.w >= 1
  for j in range(f.w // 2): # 3 -> 1 case, 4 -> 2 cases
    for i in range(f.h):
      a = i, j
      b = i, f.w - j - 1
      f[a], f[b] = f[b], f[a]

def mut_hmirror(f):
  """ flip over horizontal axis """
  if f.h <= 1:
    return
  assert f.h >= 1
  for j in range(f.w):
    for i in range(f.h // 2):
      a = i, j
      b = f.h - i - 1, j
      f[a], f[b] = f[b], f[a]

def vmirror(f):
  fc = f.copy()
  mut_vmirror(fc)
  return fc

def hmirror(f):
  fc = f.copy()
  mut_hmirror(fc)
  return fc


if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
