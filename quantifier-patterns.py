from z3 import *
from z3.z3util import get_models

def prove_all(axioms, claims: list, desc=None, print_claim=True, delimeter=': '):
  for claim in claims:
    status = prove(axioms, claim, desc=None, print_claim=True, delimeter=': ')
    if not status:
      return status
  return True

SHOW_PROOF = False
PRT_PROOF_STATUS = True
def prove(axioms, claim, desc=None, print_claim=True, delimeter=': '):
  s = Solver()
  s.add(axioms)
  s.add(Not(claim))

  sclaim = str(claim) if print_claim else ''
  delim = delimeter if print_claim else ''

  message = lambda prefix: f"{prefix} {sclaim}" if desc is None else f"{prefix} {desc}{delim}{sclaim}"
  if s.check() == sat:
    print(message("counterexample for claim:"))
    print(s.model())
    return False
  elif s.check() == unknown:
    print(message("failed to prove"))
    return False
  else: # unsat
    if PRT_PROOF_STATUS:
      print(message("proved"))
    if SHOW_PROOF:
      print(f"proof: {s.proof()}")
    return True

# https://github.com/Z3Prover/z3/blob/master/examples/python/socrates.py
# free variables used in forall must be declared Const in python
# Object = DeclareSort('Object')
# x = Const('x', Object)

def quantifier_patterns():
  """
  >>> x, y, n, k = Consts("x y n k", IntSort())
  >>> P = Function('P', IntSort(), BoolSort())

  >>> axioms = [
  ...   P(0),
  ...   ForAll([k], Implies(P(k), P(k+1)))
  ... ]

  >>> axioms
  [P(0), ForAll(k, Implies(P(k), P(k + 1)))]

  prove_all(axioms, (P(i) for i in range(22))) 23 is too big

  >>> prove_all(axioms, (P(i) for i in range(3)))
  proved P(0)
  proved P(1)
  proved P(2)
  True

  >>> 
  goal = ForAll([k], P(k))
  """

def solve_example():
  """
  Create variables:
  >>> x, y = Ints('x y')

  syntax of solve: solve(constraint1, constraint2, ...)

  x + 2 * y == 7 ==> x = 7 - 2y ==> 7 - 2y > 2 ==>
  ==> y < 5/2
  let y = 2
    And(3 > 2, 2 < 10, x == 3)

  let y = 1
    And(5 > 2, 1 < 10, x == 5)

  let y = ...

  Model M:
    (ite (y <= 2) (x == 7 - 2 * y) False)

  Below we check that M is correct:
  solve(x > 2, y < 10, x + 2 * y == 7, y > 2)
                                       ^^^^^ added to prove ite condition `y <= 2`
  >>> solve(x > 2, y < 10, x + 2 * y == 7, y > 2)
  no solution
  
  Another way to do same thing:

  >>> prove([x > 2, y < 10, x + 2 * y == 7], y <= 2)
  proved y <= 2
  True

  What if premise `y <= 2` can be more strong?
  >>> prove([x > 2, y < 10, x + 2 * y == 7], y <= 1)
  counterexample for claim: y <= 1
  [y = 2, x = 3]
  False

  Let's actually prove correctness if model M:
    (ite (y <= 2) (x == 7 - 2 * y) False)

  To prove correctness we must prove it using if and only if,
  we can prove it in two stages because:
    forall P Q : Prop :: ((P -> Q) and (Q -> P)) -> (P <-> Q)
    And we can check it right now!

  where <-> notation for "if and only if" (in Z3Py you can use `==`)
         -> notation for implication      (in Z3Py you can use `Implies`)

  >>> p, q = Bools('p q')

  From left to right
  >>> prove([Implies(p, q), Implies(q, p)], p == q)
  proved p == q
  True

  From right to left
  >>> e1 = And(Implies(p, q), Implies(q, p))
  >>> prove([p == q], e1)
  proved And(Implies(p, q), Implies(q, p))
  True
 
  Just for better understanding:
  * We can print z3 expressions as SMT LIB expressions

  >>> [(p == q).sexpr(), 'equivalent to', e1.sexpr()]
  ['(= p q)', 'equivalent to', '(and (=> p q) (=> q p))']

  Sometimes in z3py we need BoolVal and IntVal as contructors for expressions
  >>> BoolVal(True), IntVal(42)
  (True, 42)

  But now we don't need it (I think we need in `If(y <= 2, x == 7 - 2 * y, BoolVal(False))` but after some experiment realize that we don't need it)
  
  >>> left = And(x > 2, y < 10, x + 2 * y == 7)
  >>> right = If(y <= 2, x == 7 - 2 * y, False)

  From left to right
  >>> prove([left], right)
  proved If(y <= 2, x == 7 - 2*y, False)
  True

  From right to left
  >>> prove([right], left)
  proved And(x > 2, y < 10, x + 2*y == 7)
  True

  Cool! We are proved that model
  >>> right.sexpr()
  '(ite (<= y 2) (= x (- 7 (* 2 y))) false)'

  is correct for system of inequalities:
  >>> left
  And(x > 2, y < 10, x + 2*y == 7)
  """  

def simplify_example():
  """
  >>> x, y = Ints('x y')
  >>> simplify(Implies(x == y, y == x))
  Or(Not(x == y), y == x)

  TODO: how to make this equal to `True`?

  >>> z = Int('z')
  >>> simplify(Distinct(x, y, z), blast_distinct=True)
  And(Not(x == y), Not(x == z), Not(y == z))

  >>> simplify(x - 1 + 2*x + y*(1 - x))
  -1 + 3*x + y*(1 + -1*x)
  
  Options don't change result:
  >>> simplify(x - 1 + 2*x + y*(1 - x), arith_ineq_lhs=True, arith_lhs=True)
  -1 + 3*x + ...

  Bug: sometimes simplify return `-1 + 3*x + (1 + -1*x)*y`
    (sometimes= change `>>> solve(x << 2 == 24, x < 100, x >= 0)` to
      `>>> solve(x << 2 == 24, x != 6, x < 100, x >= 0)`)
  But usually: `-1 + 3*x + y*(1 + -1*x)`

  Help: Try to call `help_simplify()` to see more info about options of simplify

  Question:
    How to find example E that satisfies that conditions:
  `not simplify(E).eq(simplify(E, some_options=True))`?

  `expr.eq()` method used for comparing Ast's (expression trees)
  >>> (x == 1).eq(x == 1), (1 == x).eq(x == 1)
  (True, True)
  
  And remember: `1 == 1` and `True` is not structurally equal expressions

  Because we have two distinct equivalences:
  1. `e1 == e2`
  2. `e1.eq(e2)`

  Now we need to use IntVal and BoolVal:
  >>> (IntVal(1) == 1).eq(BoolVal(True))
  False
   
  Some simple proof of `I need to remember this two crazy things IntVal and BoolVal`:

    because this two not works:
    >>> (1 == 1).eq(BoolVal(True))
    Traceback (most recent call last):
    ...
    AttributeError: 'bool' object has no attribute 'eq'

    * first not works because `1 == 1` evaluated to `True` and `True` is not Ast.

    >>> (True).eq(BoolVal(True))
    Traceback (most recent call last):
    ...
    AttributeError: 'bool' object has no attribute 'eq'
    
    * second not works because it's a just `True` and again it not Ast.
  """

def bitvectors_example():
  """
  >>> x, y = BitVecs('x y', 32)
  >>> solve(x >> 2 == 3)
  [x = 12]

  >>> solve(x << 2 == 3)
  no solution

  >>> solve(x << 2 == 24, x < 100, x >= 0)
  [x = 6]

  It better to use get_models because solve just prints value `[x = 6]`

  >>> ms = get_models(And(x << 2 == 24, x < 100, x >= 0), 2); ms
  [[x = 6]]

  >>> x_values = [m[x].as_long() for m in ms]; x_values
  [6]

  >>> to_bin = lambda i: bin(i)[2:]
  >>> to_bin(x_values[0]), to_bin(24)
  ('110', '11000')

  i.e. '11000' = shift_left('110' by 2)

  >>> solve(x << y == y >> x, x >= 0, x < 1)
  [y = 0, x = 0]

  >>> solve(x << y == y >> x, x > 0, y >= 0, x < 262144, y < 15)
  no solution

  >>> solve(x << y == y >> x, x > 0, y >= 0, x <= 262144, y < 15)
  [x = 262144, y = 14]

  TODO: How to check this manually?
  >>> to_bin(262144), to_bin(14)
  ('1000000000000000000', '1110')

  Number of zeros for x:
  >>> x_number_of_zeros = len(to_bin(262144)) - 1; x_number_of_zeros
  18
  
  And we have y value:
  >>> bit_vec_size = 32; bit_vec_size - x_number_of_zeros
  14

  >>> min_bound = 27
  >>> solve(x << y == y >> x, x > 0, y >= 0, x <= min_bound, y <= min_bound)
  no solution

  >>> min_bound = 28
  >>> solve(x << y == y >> x, x > 0, y >= 0, x <= min_bound, y <= min_bound)
  [x = 16, y = 28]

  >>> to_bin(16), to_bin(28)
  ('10000', '11100')

  `BitVec` have size of 32 bits
  ... do some manipulations ...
  16 << 28 == 28 >> 16
  '10000' << 28 == '11100' >> 16
  '(27_0)10000' << 28 == 0

  '(27_0)11100' >> 16 == 0 because 11100 >> 5 == 0, 11100 >> 4 == 1
                      i.e. because lemma: x < 2^5 -> x >> 5 == 0

  Lemma shift_right_is_zero:
  >>> _ = prove([x >= 0], Implies(x < 2**5, x >> 5 == 0))
  proved Implies(x < 32, x >> 5 == 0)

  Lemma shift_left_is_zero:
  >>> _ = prove([x >= 0], Implies(x % 2**5 == 0, x << (32 - 5) == 0))
  proved Implies(x%32 == 0, x << 27 == 0)

  >>> assert 16 % 2**2 == 0

  i.e. for instance `16 << 28 == 0`
  works lemma 
  Implies(x % 2**2 == 0, x << (32 - 4) == 0)
  ==>
  Implies(16 % 4 == 0, 16 << 28 == 0)
    
  TODO: prove lemma below
  > statement = ForAll([y], Implies(And(x < pow(2, y), y >= 0), x >> y == 0))
  TypeError: unsupported operand type(s) for ** or pow(): 'int' and 'BitVecRef'

  > _ = prove([x >= 0], statement)
  proved ...
  
  Wrong way of thinking because rshift and lshift just forgets values:
  16 << (28 % 32) ==> 16 << (-4) ==> 16 >> 4 ==> 16 / 2^4 ==> 1
  28 >> 16 ==> ...

  >>> _ = solve(x >> 1 == 1, x > 2), solve(x >> 1 == 1, x > 3)
  [x = 3]
  no solution

  """

def uninterpreted_functions():
  """
  >>> x, y = Ints('x y')
  >>> f = Function('f', IntSort(), IntSort())
  >>> solve(f(f(x)) == x, f(x) == y, x != y)
  [x = 0, y = 1, f = [1 -> 0, else -> 1]]

  >>> solve(f(f(x)) == x, f(x) == y, x != y, f(x) == 1 - x)
  [x = 1, y = 0, f = [0 -> 1, else -> 0]]

  >>> solve(f(f(x)) == x, f(x) == y, x != y, f(f(f(x))) == x+1 )
  [x = -1, y = 0, f = [0 -> -1, else -> 0]]

  > d = 2
  > hyps2 = [Implies(i >= 0, And(-d <= f(i) - i, f(i) - i <= d, f(i) >= -10)) for i in range(5)]

  history:
    Firstly I have
    > hyps2 = [f(i) == i+1 for i in range(5)]
    then I shrink that precondition to (and lemma holds during each shrink)
    shrink 1: hyps2 = [f(i) == i+1 for i in range(4)]
    shrink 2: hyps2 = [f(i) == i+1 for i in range(3)]
    ...
    for
    > hyps2 = [f(i) == i+1 for i in range(0)]
    lemma not holds
    and I'm stop on
    > hyps2 = [f(i) == i+1 for i in range(1)]
    i.e.
    > hyps2 = [f(0) == 1]

  How to prove that lemma for 5 <= N <= +inf
  >>> N = 10 # for N = 1500 lemma holds
  >>> hyps1 = [f(f(i)) == i+2 for i in range(N)]
  >>> hyps2 = [f(0) == 1]
  >>> goal = [Or(*[f(i) != i+1 for i in range(0, N+1)])]
  >>> hs = hyps1 + hyps2 + goal

  >>> solve(*hs)
  no solution
  """

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.ELLIPSIS | doctest.FAIL_FAST)

