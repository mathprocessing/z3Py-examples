ID_NUM = 0

def to_str(n, name):
  return '' if n is None else name + '=Node'

def none_to_unknown(n):
  if n is None:
    return Unknown()
  else:
    return n

class Node:
  def __init__(self, mother=None, father=None):
    global ID_NUM
    self._id = ID_NUM
    ID_NUM += 1
    self.mother = none_to_unknown(mother)
    self.father = none_to_unknown(father)

  def __repr__(self):
    if isinstance(self.mother, Unknown):
      if isinstance(self.father, Unknown):
        return f"Node(id={self._id})"
      else:
        return f"Node(id={self._id}, {to_str(self.father, 'father')})"
    else:
      if isinstance(self.father, Unknown):
        return f"Node(id={self._id}, {to_str(self.mother, 'mother')})"
      else:
        return f"Node(id={self._id}, {to_str(self.mother, 'mother')}, {to_str(self.father, 'father')})"
  
  def __eq__(self, other):
    if isinstance(self, Unknown) or isinstance(other, Unknown):
      return False
    return self._id == other._id

  def parents(self):
    if not isinstance(self.mother, Unknown):
      yield self.mother
    if not isinstance(self.father, Unknown):
      yield self.father

  def childs(self, mapfunc=lambda x: x):
    if not isinstance(self.mother, Unknown):
      yield self.mother
      for c in self.mother.childs():
        yield c
      # yield mapfunc(self.mother.childs(mapfunc))
    if not isinstance(self.father, Unknown):
      yield self.father
      for c in self.father.childs():
        yield c


class Unknown(Node):
  def __init__(self):
    pass

  def __repr__(self):
    return '?'

  @property
  def mother(self):
    return Unknown()
  
  @property
  def father(self):
    return Unknown()

def test_simple():
  """
  >>> n = Node(Node(), Node(Node(), Node()))
  >>> list(n.parents())
  [Node(id=0), Node(id=3, mother=Node, father=Node)]

  >>> list(n.mother.parents())
  []

  >>> list(n.father.parents())
  [Node(id=1), Node(id=2)]

  >>> assert list(n.father.mother.childs()) == []
  >>> n.father.mother.mother
  ?

  >>> list(n.childs())
  [Node(id=0), Node(id=3, mother=Node, father=Node), Node(id=1), Node(id=2)]

  Equivalence of unknowns:
  >>> assert n.father.mother == n.father.mother
  >>> n.father.mother.mother == n.father.mother.mother
  False

  Last also can be (if we change __eq__ model)
  > n.father.mother.mother == n.father.mother.mother
  ?

  """

def enumerate_all_models():
  """
  > all_models(depth=3)
  [Node, ...]

  Reading string:
  xy
  zt

  H - horizontal link
  V - vertical link
  is equivalent to reading
  And(conn(x, y, H), conn(z, t, H),
    conn(x, z, V), conn(y, t, V))

  +
  x -> y -> t  equivalent to  x -> z -> t
  """

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)