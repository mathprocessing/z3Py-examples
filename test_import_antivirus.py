"""
>>> from antivirus import Node, R
>>> n = Node(3, 5)
>>> n[0] = [-1, 0, -2]
>>> n[1] = [-2, 0, -2]
>>> n.mut_move(0, R)
>>> print(n)
#-0
--0
---
---
---
"""

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
