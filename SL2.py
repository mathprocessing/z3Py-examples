"""
check proof

abc
def
ghi

let a == b #g c 
"""

def doc(x):
  return x.__doc__

def ex1():
  """
  abc******
  $a = 0
  """


def lines(inp):
  return [s.strip() for s in inp.split('\n') if s.strip()]

def test():
  """
  >>> l = lines(doc(ex1))
  >>> get_state(l)
  
  """

if __name__ == "__main__":
  import doctest
  doctest.testmod()
