from z3 import *

def prove_all(axioms, claims: list, desc=None, print_claim=True, delimeter=': '):
  for claim in claims:
    status = prove(axioms, claim, desc=None, print_claim=True, delimeter=': ')
    if not status:
      return status
  return True

SHOW_PROOF = False
def prove(axioms, claim, desc=None, print_claim=True, delimeter=': '):
  s = Solver()
  s.add(axioms)
  s.add(Not(claim))

  sclaim = str(claim) if print_claim else ''
  delim = delimeter if print_claim else ''

  message = lambda prefix: f"{prefix} {sclaim}" if desc is None else f"{prefix} {desc}{delim}{sclaim}"
  if s.check() == sat:
    print(message("counterexample for claim:"))
    print(s.model())
    return False
  elif s.check() == unknown:
    print(message("failed to prove"))
    return False
  else: # unsat
    print(message("proved"))
    if SHOW_PROOF:
      print(f"proof: {s.proof()}")
    return True

# https://github.com/Z3Prover/z3/blob/master/examples/python/socrates.py
# free variables used in forall must be declared Const in python
# Object = DeclareSort('Object')
# x = Const('x', Object)

x, y = Consts("x y", IntSort())
P = Function('P', IntSort(), BoolSort())
Q = Function('Q', IntSort(), BoolSort())

n, k = Ints('n k')

axioms = [
  P(0),
  ForAll([k], Implies(P(k), P(k+1)))
]

# prove_all(axioms, (P(i) for i in range(22))) 23 is too big

goal = ForAll([k], P(k))

# prove()

m, n = Ints('m n')
solve(x > 2, y < 10, x + 2 * y == 7)
print(simplify(Implies(x == 0, x == x)))

e1 = x + y + x + 1 + 2 - 42 - 43 >= 3
print(e1.sexpr())
# (>= (- (- (+ x y x 1 2) 42) 43) 3)

# https://www.cs.tau.ac.il/~msagiv/courses/asv/z3py/guide-examples.htm
def pp_expr(e):
  print("num args: ", e.num_args())
  print("children: ", e.children())
  print("1st child:", e.arg(0))
  print("2nd child:", e.arg(1))
  print("operator: ", e.decl())
  print("op name:  ", e.decl().name())

# pp_expr(e1)
# help_simplify()



