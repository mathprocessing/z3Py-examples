from z3 import *
from z3.z3util import get_models

def prove_all(axioms, claims: list, desc=None, print_claim=True, delimeter=': '):
  for claim in claims:
    status = prove(axioms, claim, desc=None, print_claim=True, delimeter=': ')
    if not status:
      return status
  return True

SHOW_PROOF = False
PRT_PROOF_STATUS = True
def prove(axioms, claim, desc=None, print_claim=True, delimeter=': '):
  s = Solver()
  s.add(axioms)
  s.add(Not(claim))

  sclaim = str(claim) if print_claim else ''
  delim = delimeter if print_claim else ''

  message = lambda prefix: f"{prefix} {sclaim}" if desc is None else f"{prefix} {desc}{delim}{sclaim}"
  if s.check() == sat:
    print(message("counterexample for claim:"))
    print(s.model())
    return False
  elif s.check() == unknown:
    print(message("failed to prove"))
    return False
  else: # unsat
    if PRT_PROOF_STATUS:
      print(message("proved"))
    if SHOW_PROOF:
      print(f"proof: {s.proof()}")
    return True

# Task: implement prover for induction strongly related to
# number theory, arithmetics, prime numbers, gcd, lcm

def gcd_definition():
  """
  function gcd(a b: pos): pos
  {
    if a == b then
      a
    else
      if a > b then
        gcd(b, a)
      else
        assert b - a > 0; // from a < b
        gcd(a, b - a) 
  }
  """

def list_of_theorems():
  """
  0 <> succ 0
  succ 0 = 1

  predicate prime'(n: nat)
  {
    n > 1 && forall d | 1 < d < n :: !divisor(d, n)
  }

  >>> N = DeclareSort('N') # nat
  >>> gcd = Function('gcd', N, N, N)
  >>> s = Solver()
  >>> s.set(auto_config=False, mbqi=False)
  

  predicate prime(n: int)
    requires n >= 0
    ensures prime(n) <==> prime'(n)
  {
    n >= 2 && forall d | d > 0 :: divisor(d, n) ==> (d == 1 || d == n)
  }
  prime x <=> x != 1 && divisors(d, x) = 2
    where divisors(d, x) requires d > 0

  predicate coprime(a: int, b: int)
    requires a > 0 && b > 0
  {
    forall d | d > 0 :: (divisor(d, a) && divisor(d, b)) ==> d == 1
  }

  function gcd(a: int, b: int): int
    requires a > 0 && b > 0
    ensures gcd(a, b) > 0
  {
    // Just a maximum common divisor
    if a == b then
      a
    else
      if a > b then
        gcd(b, a)
      else
        assert b - a > 0; // from a < b
        gcd(a, b - a) 
  }

  method test_gcd()
  {
    assert gcd(3, 5) == 1;
    assert gcd(2, 4) == gcd(4, 2) == 2;
    assert gcd(6, 10) == 2;
    assert gcd(6, 9) == 3;
    assert exists n | n > 0 :: gcd(1, n) == 1;
    assert !forall n | n > 0 :: gcd(1, n) != 1 by {
      assert gcd(1, 1) == 1;
    }
  }
  
  type pos = x: int | x > 0
  
  ********** GCD ************

  lemma GcdEqual(a: pos) : gcd(a, a) == a
  lemma gcd_one_left(a: pos) : gcd(1, a) == 1
  lemma gcd_one_imp_coprime(a: pos, b: pos)
    1 <= a <= b && gcd(a, b) == 1 ==> coprime(a, b)
  lemma gcd_succ_right(n: pos) : gcd(n, n+1) == 1

  lemma coprime_self_div(a: pos)
    a > 1 ==> divisor(a, a) ==> !coprime(a, a)

  ********** BOOLEAN ************

  lemma lt_bool_imp(a b: bool) : lt_bool(a, b) <==> !(b ==> a)

  ********** DIVISOR ************
  lemma divisor_add_right(x y: pos) :
    divisor(x, y) ==> divisor(x, y + x)

  ********** intersection(GCD, DIVISOR) ************

  lemma gcd_is_divisor_harder(a b: pos) :
    divisor(gcd(a, b), a)

  # Easier for induction
  lemma gcd_is_divisor(a b: pos)
    divisor(gcd(a, b), a) && divisor(gcd(a, b), b)
  
  lemma {:induction m, n} gcd_imp_divs(m n: pos)
    gcd(m, n) == 2 ==> divisor(2, m) && divisor(2, n)

  lemma coprime_imp_gcd_one(a b: pos)
    1 <= a <= b ==> coprime(a, b) ==> gcd(a, b) == 1

  lemma divisor_self(n: pos) : divisor(n, n)



  lemma n_div(n: nat):
    forall m | 1 <= n <= m :: m % n == 0 ==> divisor(n, m)

  forall m n :: gcd m n * lcm m n = m * n

  forall m n :: gcd m n * 

  OneDividesAnything(a: int)
    requires a > 0
    ensures divisor'(1, a)
  """


def my_quantifier_patterns():
  """
  >>> x, y, n, k = Consts("x y n k", IntSort())
  >>> P = Function('P', IntSort(), BoolSort())

  >>> axioms = [
  ...   P(0),
  ...   ForAll([k], Implies(P(k), P(k+1)))
  ... ]

  >>> axioms
  [P(0), ForAll(k, Implies(P(k), P(k + 1)))]

  prove_all(axioms, (P(i) for i in range(22))) 23 is too big

  >>> prove_all(axioms, (P(i) for i in range(3)))
  proved P(0)
  proved P(1)
  proved P(2)
  True

  >>> 
  goal = ForAll([k], P(k))
  """

def quantifier_patterns():
  """
  >>> f = Function('f', IntSort(), IntSort())
  >>> g = Function('g', IntSort(), IntSort())
  >>> a, b, c = Ints('a b c')
  >>> x = Int('x')


  In the following example, the first two options
  make sure that Model-based quantifier instantiation engine is disabled.    

  Z3 Fails:

  >>> s = Solver()
  >>> s.set(auto_config=False, mbqi=False)
  >>> s.add( ForAll(x, f(g(x)) == x, patterns = [f(g(x))]),\
        a == g(b),\
        b == c,\
        f(a) != c )

  >>> print(s.sexpr())
  (declare-fun f (Int) Int)
  (declare-fun g (Int) Int)
  (declare-fun b () Int)
  (declare-fun a () Int)
  (declare-fun c () Int)
  (assert (forall ((x Int)) (! (= (f (g x)) x) :pattern ((f (g x))))))
  (assert (= a (g b)))
  (assert (= b c))
  (assert (distinct (f a) c))
  <BLANKLINE>

  >>> s.check()
  unsat

  Z3 not Fails:

  >>> s = Solver()
  >>> s.set(auto_config=False, mbqi=False)
  >>> s.add( ForAll(x, f(g(x)) == x, patterns = [g(x)]),\
        a == g(b),\
        b == c,\
        f(a) != c )

  >>> s.check()
  unsat

  While E-matching is an NP-complete problem, the main sources
  of overhead in larger verification problems comes from matching
  thousands of patterns in the context of an evolving set of terms
  and equalities.

  Z3 integrates an efficient E-matching engine using term indexing
  techniques.
  """

def e_matching():
  """
  see https://leodemoura.github.io/files/ematching.pdf

  In our context, a substitution is a
    mapping from variables to ground terms.

  variables --> ground terms.
  """

def multipatterns():
  """
  >>> A = DeclareSort('A')
  >>> B = DeclareSort('B')
  >>> f = Function('f', A, B)
  >>> a1, a2 = Consts('a1 a2', A)
  >>> b      = Const('b', B)
  >>> x,  y  = Consts('x y', A)
  >>> s = Solver()

  In the following example,
    the quantified formula states that f is injective.

  >>> s.add(a1 != a2, \
          f(a1) == b, \
          f(a2) == b, \
          ForAll([x, y], Implies(f(x) == f(y), x == y), \
                 patterns=[MultiPattern(f(x), f(y))]) \
          )
  >>> s.check()
  unsat

  The quantified formula is instantiated for every pair of occurrences
  of f. 
  A simple trick allows formulating injectivity of f in such a way
    that only a linear number of instantiations is required.
  The trick is to realize that f is injective if and only if
    it has a partial inverse.

  >>> A = DeclareSort('A')
  >>> B = DeclareSort('B')
  >>> f = Function('f', A, B)
  >>> finv = Function('finv', B, A)
  >>> a1, a2 = Consts('a1 a2', A)
  >>> b      = Const('b', B)
  >>> x,  y  = Consts('x y', A)

  >>> s = Solver()
  >>> s.add(a1 != a2, \
            f(a1) == b, \
            f(a2) == b, \
            ForAll(x, finv(f(x)) == x) \
          )
  >>> s.check()
  unsat

  In Z3Py, the following additional attributes are supported:
  * qid 
    (quantifier identifier for debugging)
  * weight
    (hint to the quantifier instantiation module: 
    "more weight equals less instances")
  * no_patterns
    (expressions that should not be used as patterns
  * skid 
    (identifier prefix used to create skolem constants/functions.
  """

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.ELLIPSIS | doctest.FAIL_FAST)



