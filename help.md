# Manual merge
```bash
# https://stackoverflow.com/questions/4657009/how-to-merge-all-files-manually-in-git
git merge --no-commit --no-ff other_branch
git mergetool
git difftool HEAD
git commit
```

# doctest verbose
```bash
# https://docs.python.org/3/library/doctest.html
python -m doctest -v example.py
```
