"""
Let's search "0110" in MT:
0 (0, 1, 1, 0), 
2 1, 0, (0, 1, 1, 0),
2 0, 1, (0, 1, 1, 0),
4 1, 0, 0, 1, (0, 1, 1, 0),
0 (0, 1, 1, 0),
2 1, 0, (0, 1, 1, 0),
2 0, 1, (0, 1, 1, 0),
0 (0, 1, 1, 0),
4 1, 0, 0, 1, (0, 1, 1, 0),
2 1, 0, (0, 1, 1, 0),
2 0, 1, (0, 1, 1, 0),
4 1, 0, 0, 1, (0, 1, 1, 0),
0 (0, 1, 1, 0),
4 1, 0, 0, 1, (0, 1, 1, 0),
2 1, 0, (0, 1, 1, 0),
2 0, 1, (0, 1, 1, 0),
0 (0, 1, 1, 0),
  1, 0, 0, 1, 1, _, _, _, _, _, _, _, _, _, _, _, _,
                 ^^^^^^ How to calculate this if we don't know actual index (position)?

lemma no_three_repetitions: 
  forall n :: mt(n) == mt(n+1) == mt(n+2) ==> false

lemma no_three_repetitions_generalized:
  forall n k :: mt(n + 0 * 2^k) == mt(n + 1 * 2^k) == mt(n + 2 * 2^k) ==> false
"""

from still_life.expr import *

def mt(x):
  """
  >>> mt(0)
  0

  >>> [mt(0), mt(1), mt(2), mt(3)]
  [0, 1, 1, 0]

  """
  assert x >= 0
  if x <= 1:
    return x
  if x % 2 == 0:
    return mt(x // 2)
  else:
    return 1 - mt(x // 2)


def prepend_while(s: str, n, ch=' '):
  """
  n ~ target length
  >>> prepend_while('123', 4, ch='x')
  'x123'

  >>> prepend_while('123', 3, ch='x')
  '123'

  >>> prepend_while('123', 2, ch='x')
  '23'
  """
  if len(s) < n:
    return ch * (n - len(s)) + s
  if len(s) == n:
    return s
  return s[-n:]

def seq_to_str(f, n, space=4):
  """
  >>> seq_to_str(lambda i: i**2, 8, space=3)
  '0  1  4  9 16 25 36 49'

  >>> seq_to_str(lambda i: i**3, 8, space=3)
  '0  1  8 27 64125216343'

  >>> result = seq_to_str(lambda i: i**3, 8, space=4); result
  '0   1   8  27  64 125 216 343'

  TODO:
  > seq_to_str(lambda i: i**3, 8, compute_minimal_number_of_spaces=True) == result
  """
  return ''.join(prepend_while(str(f(i)), space) for i in range(n)).lstrip()

def show_first(n, oneline=True, space=4):
  """
  >>> show_first(8, oneline=True, space=3)
  0  1  2  3  4  5  6  7
  0  1  1  0  1  0  0  1
  """
  mt_dict = {x: mt(x) for x in range(n)}
  if not oneline:
    for k, v in mt_dict.items():
      print('{:3}: {}'.format(k, v))
  else:
    numbers_str = seq_to_str(lambda i: i, n, space)
    seq = seq_to_str(lambda i: mt(i), n, space)
    print(numbers_str)
    print(seq)


show_first(32, oneline=True, space=3)

class Seq():
  """
  >>> f = Seq(16, Const(0)); f
  Seq(16)

  >>> f[0]
  Const(0)

  >>> print(f[0])
  0

  >>> f.pp
  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  
  >>> hole = Var(); hole
  Var(_)
  >>> f = Seq(16, hole)
  >>> x, y = bool_vars('xy'); x
  Var(x)
  >>> f[1] = x; f[2] = 1; f[3] = y; f[4] = 0
  >>> f.pp
  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
  _   x   1   y   0   _   _   _   _   _   _   _   _   _   _   _

  >>> f.add(x == y)
  >>> f.context[0]
  Eq(x, y)

  >>> len(f.context)
  1

  >>> f.add(x == y)
  >>> len(f.context)
  1

  """
  def __init__(self, width, value):
    self.context = []
    assert is_expr(value)
    self.data = [value for _ in range(width)]

  def context_hashes(self):
    for e in self.context:
      yield e.get_hash()

  def add(self, hyp):
    assert is_expr(hyp)
    if hyp.get_hash() not in self.context_hashes():
      self.context.append(hyp)

  def __str__(self):
    line1 = seq_to_str(lambda i: i, n=len(self), space=4)
    line2 = seq_to_str(lambda i: self[i], n=len(self), space=4) 
    return line1 + '\n' + line2

  def __repr__(self):
    return f'Seq({len(self)})'

  def __len__(self):
    return len(self.data)

  def __getitem__(self, i):
    return self.data[i]

  def __setitem__(self, i, value):
    if not is_expr(value):
      assert value in range(2)
    self.data[i] = value

  @property
  def pp(self):
    print(self)

"""
see: 
Search: seq: 0, 1, 3, 2, 4, 5

One of results:

see: https://oeis.org/A234027
Self-inverse permutation of nonnegative integers, A054429-conjugate of blue code: a(n) = A054429(A193231(A054429(n))).
0, 1, 3, 2, 4, 5, 7, 6, 15, 14, 12, 13, 10, 11, 9, 8, 22, 23, 21, 20, 19, 18, 16, 17, 25, 24
"""

def proof_from_oeis():
  """
  Example:
  let k = 4 ==> then we have
  >>> k = 4

  First range `R1`
  >>> R1 = range(0, 2**k)
  >>> R1
  range(0, 16)
  >>> first = [mt(i) for i in R1]

  Second range `R2`
  >>> R2 = range(2**k + 2**(k-1), 2**(k+1) + 2**(k-1))
  >>> R2
  range(24, 40)
  >>> second = [mt(i) for i in R2]

  First subsequence
  >>> print(seq_to_str(lambda i: R1[i], n=len(first), space=3));\
      print(seq_to_str(lambda i: first[i], n=len(first), space=3))
  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  0  1  1  0  1  0  0  1  1  0  0  1  0  1  1  0

  Second subsequence
  >>> print(seq_to_str(lambda i: R2[i], n=len(second), space=3));\
      print(seq_to_str(lambda i: second[i], n=len(second), space=3))
  24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
  0  1  1  0  1  0  0  1  1  0  0  1  0  1  1  0

  Theorem: 
  For all positive integers k,
  the subsequence
    a(0) to a(2^k-1)
  is identical to the subsequence
    a(2^k+2^(k-1)) to a(2^(k+1)+2^(k-1)-1).

  That is to say, the first half of A_k is identical to the second half of B_k, and the
  second half of A_k is identical to the first quarter of B_{k+1}, which consists of
  the k/2 terms immediately following B_k.

  Proof: 
    The subsequence a(2^k+2^(k-1)) to a(2^(k+1)-1), the second half of B_k, is by
  definition formed from the subsequence a(2^(k-1)) to a(2^k-1), the second half of A_k,
  by interchanging its 0's and 1's. In turn, the subsequence a(2^(k-1)) to a(2^k-1), the
  second half of A_k, which is by definition also B_{k-1}, is by definition formed from
  the subsequence a(0) to a(2^(k-1)-1), the first half of A_k, which is by definition also
  A_{k-1}, by interchanging its 0's and 1's. Interchanging the 0's and 1's of a subsequence
  twice leaves it unchanged, so the subsequence a(2^k+2^(k-1)) to a(2^(k+1)-1), the second
  half of B_k, must be identical to the subsequence a(0) to a(2^(k-1)-1), the first half
  of A_k.

  Also, the subsequence a(2^(k+1)) to a(2^(k+1)+2^(k-1)-1), the first quarter of B_{k+1},
  is by definition formed from the subsequence a(0) to a(2^(k-1)-1), the first quarter of
  A_{k+1}, by interchanging its 0's and 1's. As noted above, the subsequence a(2^(k-1))
  to a(2^k-1), the second half of A_k, which is by definition also B_{k-1}, is by
  definition formed from the subsequence a(0) to a(2^(k-1)-1), which is by definition
  A_{k-1}, by interchanging its 0's and 1's, as well. If two subsequences are formed from
  the same subsequence by interchanging its 0's and 1's then they must be identical, so
  the subsequence a(2^(k+1)) to a(2^(k+1)+2^(k-1)-1), the first quarter of B_{k+1},
  must be identical to the subsequence a(2^(k-1)) to a(2^k-1), the second half of A_k.

  Therefore the subsequence 
    a(0), ..., a(2^(k-1)-1), a(2^(k-1)), ..., a(2^k-1)
  is identical to the subsequence
    a(2^k+2^(k-1)), ..., a(2^(k+1)-1), a(2^(k+1)), ..., a(2^(k+1)+2^(k-1)-1)
  QED
  """

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)