# requires, ensures (spec words)
This can be optional setup.
i.e in package manager you can specify:
```toml
[dependencies]
package-name = "1.2.3", verified = false
package-name = "1.2.3", verified = true
```

When we write code, compiler can have two modes:
1 mode: you can use some `spec words`  but it's do not strictly required.
2 mode: You must you it, you must write full checkable spec for each method.

# Automated design of project
Let's focus on:
If we already explored some set of solutions for task `T` then possible bad thing
is that we find not all solutions.

Also we have some "wishes" about project that can't be (in some cases can be) formalized
strictly. In such cases we can use ML approach to create "score viewers" of some 
"evolve-directions" of project.

Usually with this approach we must PROVE that some branches ("evolve-directions") is
"better"|"have less distance to target" than other branches of project.

If formal proof can be used => we use it
if can't => we can emulate whole behaviour of "process of finding proof" but not 
stricly formally. Let's call it `Soft proof`.

Soft proof can include just code examples or human commentaries as proof + some operators o it.
May be some examples can be mutated with others, or just can be extended in some "direction of thinking".

# Imports, type imports

## Type imports
For example, Typescript have type imports, may be this can be helpful in Carbon.

## Imports resolving
This can be great feature: resolving imports using sat-solvers(like z3).
In some hard cases we can specify algebraic constraints (or set constraints like union, intersection) on lists of imports.

```carbon
if import Data.Something then import Data.OtherThing

Define group of imports G {
  import Random
  import Time {
    Time::Date, Time::UTF
  }
}
```
Main idea: This can help to reduce size of boilerplate code.

