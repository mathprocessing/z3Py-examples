from z3 import *

x, y = Ints('x y')

pt = Tactic('propagate-values')
g = Goal()
# g.add(x + 1 + x >= 2)
g.add(Implies(x == 0, x >= 0))

s = Solver()
s.add(g)
s.check()

print(s.model())


# print(e3.arg(0).arg(0)) # lhs 
# print(e3.arg(0).arg(1)) # rhs
e3 = Implies(x == 0, x >= 0)
rule = (e3.arg(0).arg(0), e3.arg(0).arg(1))
print(rule)

print("e3: ", e3)
# print(substitute(e3.arg(1), (x, IntVal(0))))
e3_next = Implies(e3.arg(0), substitute(e3.arg(1), rule))
print("e3_next: ", e3_next)

print("\nusual simplify:\n")
print(simplify(e3))
print("\nsimplify after substitute:\n")
print(simplify(e3_next))



# print(pt(g))


# g2 = g.simplify()
# print(g2)

# line 7651 def apply(self, goal, *arguments, **keywords):
# t = Tactic('solve-eqs')

# p,q = Bools('p q')

# g3 = Goal()
# g3.add(10 == x + y)
# print(g3)
# print(t(g3))



# print(t.apply(And(x == 0, y >= x + 1)))
# <=>
# print(t(And(x == 0, y >= x + 1)))