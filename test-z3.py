from z3 import *

SHOW_PROOF = False
def my_prove(axioms, claim, desc=None, print_claim=True, delimeter=': '):
  s = Solver()
  s.add(axioms)
  s.add(Not(claim))

  sclaim = str(claim) if print_claim else ''
  delim = delimeter if print_claim else ''

  message = lambda prefix: f"{prefix} {sclaim}" if desc is None else f"{prefix} {desc}{delim}{sclaim}"
  if s.check() == sat:
    print(message("counterexample for claim:"))
    print(s.model())
  elif s.check() == unknown:
    print(message("failed to prove"))
  else: # unsat
    print(message("proved"))
    if SHOW_PROOF:
      print(f"proof: {s.proof()}")

"""
```dafny
predicate P(k: int)
predicate Q(k: int)

method test_exists_3_2(x: int, y: int)
{
  if exists k: nat :: P(k) && Q(k) { // Ok
    assert exists k:nat :: Q(k) && P(k); // Ok
  }
}

method test_forall_4_min(n: int)
{
  if !(forall k: nat :: !(n == 1 ==> k == 0)) { // Ok
    assert !(forall k:nat :: !(k != 0 ==> n != 1)); // Failed: assertion might not hold
  }
}
```
"""

x, y = Consts("x y", IntSort())
P = Function('P', IntSort(), BoolSort())
Q = Function('Q', IntSort(), BoolSort())

# axioms = [
#   ForAll( [k], f(0, y) == y ),
#   ForAll( [x, y], f(x, y) == f(x + 1, y - 1) ),
# ]

k = Int('k')

e1 = ForAll([k], And(P(k), Q(k)))
e2 = ForAll([k], And(Q(k), P(k)))

my_prove([e1], e2)

e1 = Not(ForAll([k], And(P(k), Q(k))))
e2 = Not(ForAll([k], And(Q(k), P(k))))

my_prove([e1], e2)

e1 = Not(ForAll([k], Not(And(P(k), Q(k)))))
e2 = Not(ForAll([k], Not(And(Q(k), P(k)))))

my_prove([e1], e2)

e1 = ForAll([k], Implies(x == 1, k == 0))
e2 = ForAll([k], Implies(k != 0, x != 1))

my_prove([e1], e2)

e1 = Not(ForAll([k], Not(Implies(x == 1, k == 0))))
e2 = Not(ForAll([k], Not(Implies(k != 0, x != 1))))

my_prove([e1], e2)
