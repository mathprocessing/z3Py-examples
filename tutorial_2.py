from z3 import *
from z3.z3util import get_models

to_bin = lambda i: bin(i)[2:]

def model_str(m,as_str=True):
    """
    Returned a 'sorted' model (so that it's easier to see)
    The model is sorted by its key, 
    e.g. if the model is y = 3 , x = 10, then the result is 
    x = 10, y = 3

    EXAMPLES:
    see doctest exampels from function prove() 
    """
    if z3_debug():
        assert m is None or m == [] or isinstance(m,ModelRef)

    if m :
        vs = [(v,m[v]) for v in m]
        # changed `lambda a,_` to `lambda a`
        vs = sorted(vs,key=lambda a: str(a)) 
        if as_str:
            return '\n'.join(['{} = {}'.format(k,v) for (k,v) in vs])
        else:
            return vs
    else:
        return str(m) if as_str else m

def prove_all(axioms, claims: list, desc=None, print_claim=True, delimeter=': '):
  for claim in claims:
    status = prove(axioms, claim, desc=None, print_claim=True, delimeter=': ')
    if not status:
      return status
  return True

SHOW_PROOF = False
PRT_PROOF_STATUS = True
def prove(axioms, claim=None, desc=None, print_claim=True, delimeter=': '):
  if claim is None:
    claim = axioms
    axioms = []
  s = Solver()
  s.add(axioms)
  s.add(Not(claim))

  sclaim = str(claim) if print_claim else ''
  delim = delimeter if print_claim else ''

  message = lambda prefix: f"{prefix} {sclaim}" if desc is None else f"{prefix} {desc}{delim}{sclaim}"
  if s.check() == sat:
    print(message("counterexample for claim:"))
    print(s.model())
    return False
  elif s.check() == unknown:
    print(message("failed to prove"))
    return False
  else: # unsat
    if PRT_PROOF_STATUS:
      print(message("proved"))
    if SHOW_PROOF:
      print(f"proof: {s.proof()}")
    return True

# https://github.com/Z3Prover/z3/blob/master/examples/python/socrates.py
# free variables used in forall must be declared Const in python
# Object = DeclareSort('Object')
# x = Const('x', Object)

def quantifier_patterns():
  """
  >>> x, y, n, k = Consts("x y n k", IntSort())
  >>> P = Function('P', IntSort(), BoolSort())

  >>> axioms = [
  ...   P(0),
  ...   ForAll([k], Implies(P(k), P(k+1)))
  ... ]

  >>> axioms
  [P(0), ForAll(k, Implies(P(k), P(k + 1)))]

  prove_all(axioms, (P(i) for i in range(22))) 23 is too big

  >>> prove_all(axioms, (P(i) for i in range(3)))
  proved P(0)
  proved P(1)
  proved P(2)
  True

  >>> 
  goal = ForAll([k], P(k))
  """

def solve_example():
  """
  Create variables:
  >>> x, y = Ints('x y')

  syntax of solve: solve(constraint1, constraint2, ...)

  x + 2 * y == 7 ==> x = 7 - 2y ==> 7 - 2y > 2 ==>
  ==> y < 5/2
  let y = 2
    And(3 > 2, 2 < 10, x == 3)

  let y = 1
    And(5 > 2, 1 < 10, x == 5)

  let y = ...

  Model M:
    (ite (y <= 2) (x == 7 - 2 * y) False)

  Below we check that M is correct:
  solve(x > 2, y < 10, x + 2 * y == 7, y > 2)
                                       ^^^^^ added to prove ite condition `y <= 2`
  >>> solve(x > 2, y < 10, x + 2 * y == 7, y > 2)
  no solution
  
  Another way to do same thing:

  >>> prove([x > 2, y < 10, x + 2 * y == 7], y <= 2)
  proved y <= 2
  True

  What if premise `y <= 2` can be more strong?
  >>> prove([x > 2, y < 10, x + 2 * y == 7], y <= 1)
  counterexample for claim: y <= 1
  [y = 2, x = 3]
  False

  Let's actually prove correctness if model M:
    (ite (y <= 2) (x == 7 - 2 * y) False)

  To prove correctness we must prove it using if and only if,
  we can prove it in two stages because:
    forall P Q : Prop :: ((P -> Q) and (Q -> P)) -> (P <-> Q)
    And we can check it right now!

  where <-> notation for "if and only if" (in Z3Py you can use `==`)
         -> notation for implication      (in Z3Py you can use `Implies`)

  >>> p, q = Bools('p q')

  From left to right
  >>> prove([Implies(p, q), Implies(q, p)], p == q)
  proved p == q
  True

  From right to left
  >>> e1 = And(Implies(p, q), Implies(q, p))
  >>> prove([p == q], e1)
  proved And(Implies(p, q), Implies(q, p))
  True
 
  Just for better understanding:
  * We can print z3 expressions as SMT LIB expressions

  >>> [(p == q).sexpr(), 'equivalent to', e1.sexpr()]
  ['(= p q)', 'equivalent to', '(and (=> p q) (=> q p))']

  Sometimes in z3py we need BoolVal and IntVal as contructors for expressions
  >>> BoolVal(True), IntVal(42)
  (True, 42)

  But now we don't need it (I think we need in `If(y <= 2, x == 7 - 2 * y, BoolVal(False))` but after some experiment realize that we don't need it)
  
  >>> left = And(x > 2, y < 10, x + 2 * y == 7)
  >>> right = If(y <= 2, x == 7 - 2 * y, False)

  From left to right
  >>> prove([left], right)
  proved If(y <= 2, x == 7 - 2*y, False)
  True

  From right to left
  >>> prove([right], left)
  proved And(x > 2, y < 10, x + 2*y == 7)
  True

  Cool! We are proved that model
  >>> right.sexpr()
  '(ite (<= y 2) (= x (- 7 (* 2 y))) false)'

  is correct for system of inequalities:
  >>> left
  And(x > 2, y < 10, x + 2*y == 7)
  """  

def simplify_example():
  """
  >>> x, y = Ints('x y')
  >>> simplify(Implies(x == y, y == x))
  Or(Not(x == y), y == x)

  TODO: how to make this equal to `True`?

  >>> z = Int('z')
  >>> simplify(Distinct(x, y, z), blast_distinct=True)
  And(Not(x == y), Not(x == z), Not(y == z))

  >>> simplify(x - 1 + 2*x + y*(1 - x))
  -1 + 3*x + y*(1 + -1*x)
  
  Options don't change result:
  >>> simplify(x - 1 + 2*x + y*(1 - x), arith_ineq_lhs=True, arith_lhs=True)
  -1 + 3*x + ...

  Bug: sometimes simplify return `-1 + 3*x + (1 + -1*x)*y`
    (sometimes= change `>>> solve(x << 2 == 24, x < 100, x >= 0)` to
      `>>> solve(x << 2 == 24, x != 6, x < 100, x >= 0)`)
  But usually: `-1 + 3*x + y*(1 + -1*x)`

  Help: Try to call `help_simplify()` to see more info about options of simplify

  Question:
    How to find example E that satisfies that conditions:
  `not simplify(E).eq(simplify(E, some_options=True))`?

  `expr.eq()` method used for comparing Ast's (expression trees)
  >>> (x == 1).eq(x == 1), (1 == x).eq(x == 1)
  (True, True)
  
  And remember: `1 == 1` and `True` is not structurally equal expressions

  Because we have two distinct equivalences:
  1. `e1 == e2`
  2. `e1.eq(e2)`

  Now we need to use IntVal and BoolVal:
  >>> (IntVal(1) == 1).eq(BoolVal(True))
  False
   
  Some simple proof of `I need to remember this two crazy things IntVal and BoolVal`:

    because this two not works:
    >>> (1 == 1).eq(BoolVal(True))
    Traceback (most recent call last):
    ...
    AttributeError: 'bool' object has no attribute 'eq'

    * first not works because `1 == 1` evaluated to `True` and `True` is not Ast.

    >>> (True).eq(BoolVal(True))
    Traceback (most recent call last):
    ...
    AttributeError: 'bool' object has no attribute 'eq'
    
    * second not works because it's a just `True` and again it not Ast.
  """

def bitvectors_example():
  """
  >>> x, y = BitVecs('x y', 32)
  >>> solve(x >> 2 == 3)
  [x = 12]

  >>> solve(x << 2 == 3)
  no solution

  >>> solve(x << 2 == 24, x < 100, x >= 0)
  [x = 6]

  It better to use get_models because solve just prints value `[x = 6]`

  >>> ms = get_models(And(x << 2 == 24, x < 100, x >= 0), 2); ms
  [[x = 6]]

  >>> x_values = [m[x].as_long() for m in ms]; x_values
  [6]

  >>> to_bin(x_values[0]), to_bin(24)
  ('110', '11000')

  i.e. '11000' = shift_left('110' by 2)

  >>> solve(x << y == y >> x, x >= 0, x < 1)
  [y = 0, x = 0]

  >>> solve(x << y == y >> x, x > 0, y >= 0, x < 262144, y < 15)
  no solution

  >>> solve(x << y == y >> x, x > 0, y >= 0, x <= 262144, y < 15)
  [x = 262144, y = 14]

  TODO: How to check this manually?
  >>> to_bin(262144), to_bin(14)
  ('1000000000000000000', '1110')

  Number of zeros for x:
  >>> x_number_of_zeros = len(to_bin(262144)) - 1; x_number_of_zeros
  18
  
  And we have y value:
  >>> bit_vec_size = 32; bit_vec_size - x_number_of_zeros
  14

  >>> min_bound = 27
  >>> solve(x << y == y >> x, x > 0, y >= 0, x <= min_bound, y <= min_bound)
  no solution

  >>> min_bound = 28
  >>> solve(x << y == y >> x, x > 0, y >= 0, x <= min_bound, y <= min_bound)
  [x = 16, y = 28]

  >>> to_bin(16), to_bin(28)
  ('10000', '11100')

  `BitVec` have size of 32 bits
  ... do some manipulations ...
  16 << 28 == 28 >> 16
  '10000' << 28 == '11100' >> 16
  '(27_0)10000' << 28 == 0

  '(27_0)11100' >> 16 == 0 because 11100 >> 5 == 0, 11100 >> 4 == 1
                      i.e. because lemma: x < 2^5 -> x >> 5 == 0

  Lemma shift_right_is_zero:
  >>> _ = prove([x >= 0], Implies(x < 2**5, x >> 5 == 0))
  proved Implies(x < 32, x >> 5 == 0)

  Lemma shift_left_is_zero:
  >>> _ = prove([x >= 0], Implies(x % 2**5 == 0, x << (32 - 5) == 0))
  proved Implies(x%32 == 0, x << 27 == 0)

  >>> assert 16 % 2**2 == 0

  i.e. for instance `16 << 28 == 0`
  works lemma 
  Implies(x % 2**2 == 0, x << (32 - 4) == 0)
  ==>
  Implies(16 % 4 == 0, 16 << 28 == 0)
    
  TODO: prove lemma below
  > statement = ForAll([y], Implies(And(x < pow(2, y), y >= 0), x >> y == 0))
  TypeError: unsupported operand type(s) for ** or pow(): 'int' and 'BitVecRef'

  > _ = prove([x >= 0], statement)
  proved ...
  
  Wrong way of thinking because rshift and lshoft just forgets values:
  16 << (28 % 32) ==> 16 << (-4) ==> 16 >> 4 ==> 16 / 2^4 ==> 1
  28 >> 16 ==> ...

  >>> _ = solve(x >> 1 == 1, x > 2), solve(x >> 1 == 1, x > 3)
  [x = 3]
  no solution

  If you want to rotate instead of shift use `RotateRight :: BitVec -> BitVec -> BitVec`
  >>> RotateRight(x, y)
  RotateRight(x, y)
  >>> simplify(RotateRight(x, 0))
  x
  >>> simplify(RotateRight(x, 32))
  x

  >>> simplify(RotateRight(x, 31))
  Concat(Extract(30, 0, x), Extract(31, 31, x))

  >>> simplify(Extract(2, 0, BitVecVal(13, 32)))
  5

  >>> to_bin(13)
  '1101'
    
    ^^^ extracted part

  >>> b5 = to_bin(13)[1:]; (b5 == to_bin(5), b5)
  (True, '101')
  """

def uninterpreted_functions():
  """
  >>> x, y = Ints('x y')
  >>> f = Function('f', IntSort(), IntSort())
  >>> solve(f(f(x)) == x, f(x) == y, x != y)
  [x = 0, y = 1, f = [1 -> 0, else -> 1]]

  >>> solve(f(f(x)) == x, f(x) == y, x != y, f(x) == 1 - x)
  [x = 1, y = 0, f = [0 -> 1, else -> 0]]

  Warning: returns are unstable (bad thing for doctest, but we can use
  equivalence classes, i.e. turn models to some classes + we can use ELLIPSIS)
  >>> solve(f(f(x)) == x, f(x) == y, x != y, f(f(f(x))) == x+1 )
  [x = ..., y = ..., f = [... -> ..., else -> ...]]

  [x = -1, y = 0, f = [0 -> -1, else -> 0]]
  or
  [x = 0, y = 1, f = [1 -> 0, else -> 1]]


  > d = 2
  > hyps2 = [Implies(i >= 0, And(-d <= f(i) - i, f(i) - i <= d, f(i) >= -10)) for i in range(5)]

  history:
    first i have 
    > hyps2 = [f(i) == i+1 for i in range(5)]
    then i shrink that precondition to (and lemma holds during each shrink)
    shrink 1: hyps2 = [f(i) == i+1 for i in range(4)]
    shrink 2: hyps2 = [f(i) == i+1 for i in range(3)]
    ...
    for
    > hyps2 = [f(i) == i+1 for i in range(0)]
    lemma not holds
    and I'm stop on
    > hyps2 = [f(i) == i+1 for i in range(1)]
    i.e.
    > hyps2 = [f(0) == 1]

  How to prove that lemma for 5 <= N <= +inf
  >>> N = 10 # for N = 1500 lemma holds
  >>> hyps1 = [f(f(i)) == i+2 for i in range(N)]
  >>> hyps2 = [f(0) == 1]
  >>> goal = [Or(*[f(i) != i+1 for i in range(0, N+1)])]
  >>> hs = hyps1 + hyps2 + goal

  >>> solve(*hs)
  no solution
  """

def eval_model():
  """
  >>> x, y = Ints('x y')
  >>> f = Function('f', IntSort(), IntSort())
  >>> s = Solver()
  >>> s.add(f(f(x)) == x, f(x) == y, x != y)
  >>> s.check()
  sat

  >>> m = s.model()
  >>> [val == m.evaluate(val) for val in [x, f(x), f(f(x))]]
  [0 == x, 1 == f(x), 0 == f(f(x))]
  """

def arrays_example():
  """
  Use I as an alias for IntSort()
  >>> I = IntSort()

  A is an array from integer to integer
  >>> A = Array('A', I, I)
  >>> x, y = Ints('x y')
  >>> A[x]
  A[x]

  >>> Select(A, x) # Select(A, x) eqivalent to A[x]
  A[x]

  >>> Store(A, x, 10)
  Store(A, x, 10)

  A[2] := x+1
  get(A[2])

  >>> simplify(Select(Store(A, 2, x+1), 2))
  1 + x

  Select and Store:
  
  Lemma:
    Suppose A is an array of integers, then the
    constraints A[x] == x, Store(A, x, y) == A
    are satisfable for an array that contains an
    index x that maps to x, and when x == y.

  We can get one model for example:

  >>> solve(A[x] == x, Store(A, x, y) == A)
  [A = Store(K(Int, 1), 0, 0), y = 0, x = 0]

  Or we can prove Lemma:

  1. Using `solve`

  >>> solve(A[x] == x, Store(A, x, y) == A, x != y)
  no solution

  no solution means that P -> Q

  2. Using `prove`

  >>> prove([A[x] == x, Store(A, x, y) == A], x == y)
  proved x == y
  True
  """


def solver_statistics_example():
  """
  >>> x, y = Reals('x y')
  >>> s = Solver()
  >>> s.add(x > 1, y > 1, Or(x + y > 3, x - y < 2))

  Get asserted constraints:
  >>> for c in s.assertions():
  ...   print(c)
  x > 1
  y > 1
  Or(x + y > 3, x - y < 2)

  >>> s.check()
  sat

  Get statistics:
  > s.statistics()
  (:arith-lower         1
   :arith-make-feasible 3
   :arith-max-columns   8
   :arith-max-rows      2
   :arith-upper         3
   :decisions           2
   :final-checks        1
   :max-memory          3.13
   :memory              2.74
   :mk-bool-var         6
   :num-allocs          634909625
   :num-checks          1
   :rlimit-count        25307
   :time                0.04)

  Traverse statistics:
  > for k, v in s.statistics():
  ...   print(f'{k} : {v}')
  mk bool var : 1
  decisions : 2
  final checks : 1
  num checks : 1
  mk bool var : 5
  arith-lower : 1
  arith-upper : 3
  arith-make-feasible : 3
  arith-max-columns : 8
  arith-max-rows : 2
  num allocs : 634909625
  rlimit count : 25307
  max memory : 3.13
  memory : 2.74
  time : 0.011
  """

def traverse_model_example():
  """
  >>> x, y, z = Reals('x y z')
  >>> s = Solver()
  >>> s.add(x > 1, y > 1, x + y > 3, z - x < 10)
  >>> s.check()
  sat

  >>> m = s.model()
  >>> f'x = {m[x]}'
  'x = 3/2'

  Traverse model:
  >>> for d in m.decls():
  ...   print(f'{d.name()} = {m[d]}')
  z = 0
  y = 2
  x = 3/2

  If order of [z,y,x] it is not what you want try to use `model_str(model)`

  >>> print(model_str(m))
  x = 3/2
  y = 2
  z = 0
  """

def machine_arithmetic_example():
  """
  Modern CPUs and main-stream programming languages use arithmetic over fixed-size
  bit-vectors.

  Machine arithmetic is available in Z3Pu as `Bit-Vectors`.
  They implement the precise semantics of unsigned and of signed two-components arithmetic.

  The following example demonstrates how to create bit-vector variables and constants.
  The function `BitVec('x', 16)` creates a bit-vector variable in Z3 named `x` and `16` bits.
  For convenience, integer constants can be used to create bit-vector expression in Z3Py.

  The function `BitVecVal(10, 32)` creates a 
    bit-vector of size `32` containing the value `10`.

  >>> x, y = BitVecs('x y', 16)
  >>> x + 2
  x + 2

  Internal representation:
  >>> (x + 2).sexpr()
  '(bvadd x #x0002)'

  For 16-bit integers: -1 == 65535 == 65536 - 1 == 2^16 - 1

  >>> simplify(x + y - 1)
  65535 + x + y

  Creating bit-vector constants:
  >>> a = BitVecVal(-1, 16)
  >>> b = BitVecVal(65535, 16)
  >>> simplify(a == b)
  True
  
  We can use prove if `simplify` not works:
  >>> prove(a == b)
  proved 65535 == 65535
  True

  >>> a = BitVecVal(-1, 32)
  >>> b = BitVecVal(65535, 32)

  For 32-bit integers: 65535 != -1 == 2^32 - 1

  >>> simplify(a == b)
  False

  n contrast to programming languages, such as C, C++, C#, Java, there is
  no distinction between signed and unsigned bit-vectors as numbers.

  Instead, Z3 provides special signed versions of arithmetical operations
  where it makes a difference whether the bit-vector is treated as signed
  or unsigned.

  In Z3Py, the operators <, <=, >, >=, /, % and >> correspond to the signed
  versions. The corresponding unsigned operators are
  ULT, ULE, UGT, UGE, UDiv, URem and LShR.

  Create two bit-vectors of size 32
  >>> x, y = BitVecs('x y', 32)

  >>> solve(x + y == 2, x > 0, y > 0)
  [y = 1, x = 1]

  Bit-wise operators:
  `&` bit-size and
  `|` bit-size or
  `~` bit-size not

  >>> solve(x & y == ~y)
  [y = 4294967295, x = 0]
  
  >>> pp_bin = lambda n: (to_bin(n), 'with length = ', len(to_bin(n)))
  >>> pp_bin(4294967295)
  ('11111111111111111111111111111111', 'with length = ', 32)

  Using signed version of '<'
  >>> get_models(x < 0, k=1)[0]
  [x = 4294967295]

  >>> assert (_[x]).as_long() == 2**32 - 1

  Using unsigned version of '<'
  >>> solve(ULT(x, 0))
  no solution
  """

def type_sorts_example():
  """
  Declare a List if integers
  >>> List = Datatype('List')

  Constructor cons: (Int, List) -> List
  >>> List.declare('cons', ('car', IntSort()), ('cdr', List))

  Constructor nil: List
  >>> List.declare('nil')

  Create the datatype
  >>> List = List.create()
  >>> is_sort(List)
  True

  >>> cons, car, cdr, nil =\
  (List.__dict__[name] for name in ['cons', 'car', 'cdr', 'nil'])

  function declarations: 
    'cons', 'car', 'cdr'
  constants:
    'nil'

  >>> assert all(is_func_decl(c) for c in [cons, car, cdr])
  >>> assert is_expr(nil)

  Let's create simple list instance:
  >>> l1 = cons(10, cons(20, nil)); l1
  cons(10, cons(20, nil))

  >>> simplify(cdr(l1)) # cdr <=> tail
  cons(20, nil)

  >>> simplify(car(l1)) # car <=> head
  10

  >>> simplify(l1 == nil)
  False

  Color datatype.

  >>> Color = Datatype('Color')
  >>> color_names = ['red', 'green', 'blue']
  >>> for name in color_names:
  ...   Color.declare(name)
  >>> Color = Color.create()
  >>> is_expr(Color.green)
  True

  >>> Color.green == Color.blue
  green == blue

  >>> simplify(Color.green == Color.blue)
  False

  Let `c` be a constant of sort `Color`
  >>> c = Const('c', Color)

  Then, `c` must be red, green or blue
  >>> prove(Or(c == Color.green,\
               c == Color.blue,\
               c == Color.red))
  proved Or(c == green, c == blue, c == red)
  True

  Z3Py also provides the following shorthand for declaring enumeration sorts.

  >>> Color, (red, green, blue) = EnumSort('Color', ('red', 'green', 'blue'))

  >>> green == blue
  green == blue

  >>> simplify(green == blue)
  False

  >>> c = Const('c', Color)
  >>> get_models(And(c != green, c != blue), k=1)[0]
  [c = red]
  """

def mutually_recursive_datatypes_example():
  """
  >>> TreeList = Datatype('TreeList')
  >>> Tree = Datatype('Tree')
  >>> Tree.declare('leaf', ('val', IntSort()))
  >>> Tree.declare('node', ('left', TreeList), ('right', TreeList))
  >>> TreeList.declare('nil')
  >>> TreeList.declare('cons', ('car', Tree), ('cdr', TreeList))

  To declare mutually recursive datatypes use `CreateDatatypes` instead of
  method `create()`.
  >>> Tree, TreeList = CreateDatatypes(Tree, TreeList)

  >>> t1 = Tree.leaf(10)
  >>> tl1 = TreeList.cons(t1, TreeList.nil)
  >>> t2 = Tree.node(tl1, TreeList.nil); t2
  node(cons(leaf(10), nil), nil)

  >>> simplify(Tree.val(t1))
  10

  >>> t1, t2, t3 = Consts('t1 t2 t3', TreeList)
  >>> ms = get_models(Distinct(t1, t2, t3), k=5)
  >>> for i, m in enumerate(ms):
  ...   print(f'{i})')
  ...   print(model_str(m))
  ...   print('-'*42) # added to hide <BLANKLINE>
  0)
  t1 = nil
  t2 = cons(leaf(0), nil)
  t3 = cons(leaf(1), nil)
  ------------------------------------------
  1)
  t1 = cons(leaf(2), nil)
  t2 = cons(leaf(2), cons(leaf(2), nil))
  t3 = cons(leaf(3), cons(leaf(2), nil))
  ------------------------------------------
  2)
  t1 = cons(leaf(4), cons(leaf(2), nil))
  t2 = cons(leaf(5), cons(leaf(2), nil))
  t3 = cons(leaf(6), cons(leaf(2), nil))
  ------------------------------------------
  3)
  t1 = cons(leaf(7), cons(leaf(2), nil))
  t2 = cons(leaf(8), cons(leaf(2), nil))
  t3 = cons(leaf(9), cons(leaf(2), nil))
  ------------------------------------------
  4)
  t1 = cons(leaf(10), cons(leaf(2), nil))
  t2 = cons(leaf(11), cons(leaf(2), nil))
  t3 = cons(leaf(12), cons(leaf(2), nil))
  ------------------------------------------

  Bad things:
  * 2), 3), 4) almost identical to each other
  * Where `node`? I only see: nil, cons, leaf
  """


if __name__ == '__main__':
  import doctest
  flags = {
    'optionflags': doctest.ELLIPSIS | doctest.FAIL_FAST
  }
  doctest.testmod(**flags)


