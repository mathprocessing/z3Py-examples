# Copyright (c) Microsoft Corporation 2015, 2016

# The Z3 Python API requires libz3.dll/.so/.dylib in the 
# PATH/LD_LIBRARY_PATH/DYLD_LIBRARY_PATH
# environment variable and the PYTHONPATH environment variable
# needs to point to the `python' directory that contains `z3/z3.py'
# (which is at bin/python in our binary releases).

# If you obtained example.py as part of our binary release zip files,
# which you unzipped into a directory called `MYZ3', then follow these
# instructions to run the example:

# Running this example on Windows:
# set PATH=%PATH%;MYZ3\bin
# set PYTHONPATH=MYZ3\bin\python
# python example.py

# Running this example on Linux:
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:MYZ3/bin
# export PYTHONPATH=MYZ3/bin/python
# python example.py

# Running this example on macOS:
# export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:MYZ3/bin
# export PYTHONPATH=MYZ3/bin/python
# python example.py


from z3 import *
from z3.z3poly import subresultants
from z3.z3util import get_models, prove, model_str


def test_solver():
  x = Real('x')
  y = Real('y')
  s = Solver()
  s.add(x + y > 5, x > 1, y > 1)
  print(s.check())
  print(s.model())

def mul_tuple(t, val):
  x, y, z = t
  return x * val, y * val, z * val

def add_tuples(t, v):
  return t[0]+v[0], t[1]+v[1], t[2]+v[2]

def gcd(a, b):
  """
  gcd(a=6, b=8) = gcd(b % a=2, a=6) =
  gcd(a=2, b=6) = gcd(b % a=0, a=2) =
  gcd(a=0, b=2) = 2
    
  """
  assert not (a == 0 and b == 0)
  a, b = abs(a), abs(b)
  if a > b:
    return gcd(b, a)
  assert a <= b
  if a == b:
    return a
  assert a < b
  if a == 0:
    return b
  assert a > 0 and b > 0 and b > a
  assert b % a < a
  return gcd(b % a, a)

def lcm(a, b):
  return (a * b) // gcd(a, b)

def test_poly():
  x, y = Reals('x y')
  a = (6, -2, 0)
  b = (-2, -2, 2) # not checks
  print(subresultants(a[0]*x + a[1]*y + a[2], b[0]*x + b[1]*y + b[2], x))
  
  # lowest common multiplier, lcm a b * gcd a b = a * b
  L = lcm(a[0], b[0])
  ma, mb = -L // a[0], L // b[0] # 3, 2
  newa, newb = mul_tuple(a, ma), mul_tuple(b, mb)
  print(newa, newb)
  print(add_tuples(newa, newb))
  # print(subresultants(7*x+3, 2*x - 8*y, x))


def test_get_models():
  x = Int('x')
  y = Int('y')
  k = Int('k')
  u = Int('u')
  expr = And(x >= 0, x + y >= 5, u*x - y < 3, x == k * y, k > 0)
  ms = get_models(And(x >= 0, x + y >= 5, u*x - y < 3, x == k * y, k > 0, u == -5), 5)
  # proved: u <= 0 by seeing `False` in output of function `get_models`
  # How to prove this fact using `prove` function?
  prove(u <= 0, assume=expr, verbose=0)
  # z3.z3types.Z3Exception: Symbolic expressions cannot be cast to concrete Boolean values.

  
  # 
  # how to make projection of u to X axis?
  # Example: 
  # a1 <= u <= a2 or b1 <= u <= b2 or ...

  # When we get models, we have comparing operation between two models:
  # if mA == mB then they can't be in result list:
  #   (mA in result) and (mB in result) ==> False
  #
  # But what if we want to change (==) operator for models?
  # For example to make projection of models stream to some really short list
  # Other (==) equiv operator also helps to decrease amount of unnesessary
  # computation in worst case.

  print(ms)


def test_groups_using_ints():
  """
  We can calculate group of order 2 using only int type
  """
  # encode inv function
  i0, i1 = Int('i0'), Int('i1')
  # encode mul function
  m00, m01, m10, m11 = Int('m00'), Int('m01'), Int('m10'), Int('m11')
  e = Int('e') # we can assume that e == 0
  hyps = []

  def inv_eq(x, res):
    return And(
      Implies(x == 0, i0 == res),
      Implies(x == 1, i1 == res))

  def mul_eq(x, y, res):
    """
    if x == 0 and y == 0: return m00
    if x == 0 and y == 1: return m01
    if x == 1 and y == 0: return m10
    if x == 1 and y == 1: return m11
    """
    return And(
      Implies(And(x == 0, y == 0), m00 == res),
      Implies(And(x == 0, y == 1), m01 == res),
      Implies(And(x == 1, y == 0), m10 == res),
      Implies(And(x == 1, y == 1), m11 == res),
    )

  inv_data = [i0, i1]
  mul_data = [m00, m01, m10, m11]
  # Type conditions for inv
  for v in inv_data:
    hyps.extend([0 <= v, v <= 1])

  # Type conditions for mul
  for v in mul_data:
    hyps.extend([0 <= v, v <= 1])

  # axiom: id_right : x * e = x
  # let e ~ 0, a ~ 1
  hyps.append(m00 == 0) # m00 == 0 <=> mul_eq(0, 0, 0)
  hyps.append(m10 == 1) # m10 == 1 <=> mul_eq(1, 0, 1)

  # axiom: inv_right : x * inv x = e
  # hyps.append(Implies(i0 == 0, m00 == 0))
  # hyps.append(Implies(i0 == 1, m01 == 0))
  hyps.append(Implies(i1 == 0, m10 == 0))
  hyps.append(Implies(i1 == 1, m11 == 0))
  # to fastly check minimal set of axiom instances i check:
  # Note: assumption is only one solution
  # Task find minimal set of axiom instances S, where |S| == 2
  # [1, 2, 3] -> False
  # After that we know [1, 2] and [1, 3] and [2, 3] is False
  # [1, 2, 4] -> False
  # After that we know [1, 4] and [2, 4] is False
  # Number of all posibble combinations of |S| = 2 is 6
  # 6 - 5 = 1 we have to check only one
  # [3, 4] -> True
  # to prove that is [3, 4] minimal set of axiom instances S
  # we need to check [3] and [4]
  # [3] -> False
  # [4] -> False
  # Qed. [3, 4] is minimal set of inv_right instances

  # Dominating_theorem:
  #   That means even if "good object axioms" is False
  #   axiom instances [3, 4] dominated on [1, 2]


  # axiom: assoc : (x * y) * z = x * (y * z)
  # or we can use more simpler version of assoc to G2:
  # axiom: G2_assoc : (1 * 1) * 1 = 1 * (1 * 1)
  #                   m11 * 1 = 1 * m11
  hyps.append(Implies(m11 == 0, m01 == m10))
  # hyps.append(Implies(m11 == 1, m11 == m11)) trivial

  # m00 = m01 = m10 = m11 not possible (bad object, |G| == 2)
  # Let's call them "good object axioms"
  hyps.append(Not(And(m00 == m01, m01 == m10, m10 == m11)))
  hyps.append(Not(i0 == i1))


  all_facts = And(*hyps)
  # Check for two solutions to prove that we have only one
  ms = get_models(all_facts, k=20)
  # assert len(ms) == 1 # we have only one solution
  for i, model in enumerate(ms):
    print(f'{i+1})--------')
    print(model_str(model, True))
    print()
  # i0 = 0
  # i1 = 1
  # m00 = 0
  # m01 = 1
  # m10 = 1
  # m11 = 0

test_groups_using_ints()

"""
if only first of id_right holds:
1)--------
i0 = 1
i1 = 0
m00 = 0
m01 = 0
m10 = 0
m11 = 1

2)--------
i0 = 0
i1 = 1
m00 = 0
m01 = 1
m10 = 1
m11 = 0

"""