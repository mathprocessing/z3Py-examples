```
Наш язык сам по себе (склеивает некоторые понятия | используя человеческий язык, графику, трудно разъединить некоторые понятия)
отсюда может возникать неполнота математических теорий.

Задача распознавания изображения:
  Как прочитать рукописный текст (или получить символьное ограничение на него) на обратной стороне листа?
```

# Задача про Ваню и Машу
1. Сколько в среднем и кто выигрывает при разных значениях параметра p?
Ответ:
if p in [0, 3/4] ==> Стратегия смешанная и U(p) = 8/3 p - 1
if p in [3/4, 1] ==> Чистая стратегия и U(p) = 1

2. Сколько стоит информация о карте Маши в задача Алексей Савватеева ("Игрушечный покер" про Ваню и Машу)?
Ответ: 
Выгода купить информацию
V(p) = U(p) - (2p - 1)
if p in [0, 3/4] ==> V(p) = 8/3 p - 1 - 2p + 1 = 2/3 p
if p in [3/4, 1] ==> V(p) = 1 - 2p + 1 = 2(1 - p)

Vmax (максимальная стоимость информации) = 2/3 * 3/4 = 1/2
pmax = 3/4

p = 4/9 in [0, 3/4] ==> V = 2/3 p = 8/27 шоколадки стоит информация о карте Маши (покупает Ваня)

# Пешечная дуэль

loss = чтобы ты не сделал будет loss (любое действие имеет маркер (надпиcь на ребре) loss т.е. ведет в состояние win)
win = есть хотя бы одно действие, которое имеет маркер win, т.е. ведет в состояние loss

A: (0, 0, 0) -> loss (читается: состояние (0, 0, 0) ведет к проигрышу)
B: (0, 0, >= 1) -> win
C: (0, 0, 1) -> loss
Connections: C -> B -> A
D1: (0, 1, >= 2) -> win, D2: (1, 1, >= 1) -> win
Connections: D1 -> C, D2 -> C

(0, 2, 2) -> action(-2) -> B: (0, 0, >= 1)
(0, 2, 2) -> action(-1) -> D1: (0, 1, >= 2)

## Пешечная дуэль extra
Как доказать, что доказательство|дерево состояний имеет максимально компактную форму для представления?
Это может помочь искать какие неоптимальности в рассуждениях людей, нлибо не обнаруженные полезные структуры/интересные ходы.

# Базовые алгоритмы
Алгоритмы сортировки + идеи анализа алгоритмов + оптимальные Oblivious sorting networks
https://algs4.cs.princeton.edu/21elementary/
```