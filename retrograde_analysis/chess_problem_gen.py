"""
# Ретроградный анализ
1. Поиск максимального мата.
  * Начинаем с любого мата
  * Движемся в прошлое
  * Не должно быть способа поставить мат за число шагов меньшее, чем мы "прошли" в прошлое
2. Генератор шахматных задач.
3. Поиск позиций, в которых выгодно сделать маловероятный выбор (например: заменить проходную пешку на слона)
4. Реализовать функцию, которая возвращает разные Scholar's Mate, отсортированные по убыванию сложности.
  6 Checkmate Traps | Chess Opening Tricks to Win Fast | Short Games, Moves, Tactics & Ideas
  https://www.youtube.com/watch?v=2eg7-Aq0uG4
  Детский мат
  https://www.youtube.com/watch?v=xa6ig7v9Rv8
5. Визуализация рассуждений игрока
  ЧИТЕР против ЧЕМПИОНА МИРА! Магнус Карлсен СИЛЬНЕЕ Компьютера?! Шахматы
  https://youtu.be/FKSGAYiqtCw?t=181

Ретроградный анализ - по сути является продуктом простой идеи:
  "Если имеет смысл рассмотреть некоторую функцию, значит имеет смысл рассмотреть и функцию обратную ей."
  P.S. Лучше может быть "движение в смысловом пространстве" одновременно и в сторону функции, и в сторону обратной ей функции.

Fen notation:
White pieces are designated using uppercase letters ("PNBRQK"), while black pieces use lowercase letters ("pnbrqk")
  from https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation

>>> b = Board(3, 3)
>>> b.from_str('K-k|R')
>>> assert b.turn == TURN_WHITE # Default turn is for white team
>>> b.turn = TURN_BLACK
>>> b.pp
K-k
R--
---

>>> pplist([b.move(m) for m in b.moves_forward()])
1)
K--
R-k
---

>>> b.turn = TURN_WHITE
>>> pplist([b.move(m) for m in b.moves_forward()])
1)
K-k
---
R--

2)
K-k
-R-
---

^^^
We don't have Stailmate case because `b.moves_forward()` pruned all "stupid" moves.
Stailmate case:
  K-k    K--
  --R -> --k
  ---    ---

Let's do moves beckward.
>>> b.turn = TURN_BLACK # But if we go one step back => we change positions of WHITE figures
>>> pplist([b.back_move(m) for m in b.moves_backward()])
1)
K-k
---
R--

2)
K-k
-R-
---

3)
...
"""