# Загадочная ошибка
"QWidget: Must construct a QApplication before a QWidget"
Возникает из-за Exception `file not found` или либо + он требует появления MessageBox(QWidget),
но MessageBox не может появиться раньше `QApplication a(argc, argv);`
т.е. нужно просто врубить `QApplication a(argc, argv); ... return a.exec()`
и увидеть ошибку, исправить и вырубить назад например

# Build release
https://doc.qt.io/qt-5/windows-deployment.html
make clean
qmake -config release
make

# Copy ot clipboard
```cpp
QGuiApplication::clipboard()->setText(QDir::toNativeSeparators(fileName));
```

# RButtonGroup
https://stackoverflow.com/questions/26319977/how-to-find-out-which-radio-button-chosen-by-the-user
```cpp
void MainWindow::on_pushButton_15_clicked(){
    QButtonGroup group;
    QList<QRadioButton *> allButtons = ui->groupBox->findChildren<QRadioButton *>();
    qDebug() <<allButtons.size();
    for(int i = 0; i < allButtons.size(); ++i)
    {
        group.addButton(allButtons[i],i);
    }
    qDebug() << group.checkedId();
    qDebug() << group.checkedButton();
}
```


# Json
https://evileg.com/en/post/419/
```cpp
void loadJSON() {
    QDir::setCurrent(pathToQuestions);
    QString openFileName = "questions.json";
    QFile jsonFile(openFileName);
    if (!jsonFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "File NOT opened";
        return;
    }
    qDebug() << "File opened";

    // Read the entire file
     QByteArray saveData = jsonFile.readAll();
     // Create QJsonDocument
     QJsonDocument jsonDocument(QJsonDocument::fromJson(saveData));
     // From which we select an object into the current working QJsonObject
     QString question = jsonDocument[0]["question"].toString();
     int id = jsonDocument[1]["id"].toInt();
     double ph = jsonDocument[1]["ph"].toDouble();
 }
```




```cpp
#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QLabel;
class QLineEdit;
class QTextEdit;
QT_END_NAMESPACE

//! [class definition]
class AddressBook : public QWidget
{
    Q_OBJECT

public:
    AddressBook(QWidget *parent = nullptr);

private:
    QLineEdit *nameLine;
    QTextEdit *addressText;
};
//! [class definition]

#endif
```

```cpp
#include <QtWidgets>
#include "addressbook.h"

//! [constructor and input fields]
AddressBook::AddressBook(QWidget *parent)
    : QWidget(parent)
{
    QLabel *nameLabel = new QLabel(tr("Name:"));
    nameLine = new QLineEdit;

    QLabel *addressLabel = new QLabel(tr("Address:"));
    addressText = new QTextEdit;
//! [constructor and input fields]

//! [layout]
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(nameLabel, 0, 0);
    mainLayout->addWidget(nameLine, 0, 1);
    mainLayout->addWidget(addressLabel, 1, 0, Qt::AlignTop);
    mainLayout->addWidget(addressText, 1, 1);
//! [layout]

//![setting the layout]
    setLayout(mainLayout);
    setWindowTitle(tr("Simple Address Book"));
}
//! [setting the layout]
```