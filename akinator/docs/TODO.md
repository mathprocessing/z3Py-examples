# Алгоритм
0. [ ] Выбор вопроса, который максимально уменьшает энтропию за один шаг


1. Сохранение оценки
[x] сохранение count персонажа (аналог априорной вероятности ph)
[ ] сохранение матриц ответов

2. [ ] Визуализация распределения вероятностей гипотез в виде таблицы
3. [ ] Список заданных вопросов/ответов

4. [ ] Возможность создать новую игру (перезапустить) в любое время

5. [ ] Остановка игры если вероятность гипотезы выше порога X

# Simple code

```text
TODO: Create model class that have 3 fields: this 3 json files
And we can just call model.load()
```

```cpp
  Game(Ui::MainWindow* ui) {
    QJsonParseError err;

    QJsonDocument charactersJson = loadJsonFromFile("characters.json", &err);
    if (err.error != QJsonParseError::NoError) showError(err);

    QJsonDocument questionsJson = loadJsonFromFile("questions.json", &err);
    if (err.error != QJsonParseError::NoError) showError(err);

    QJsonDocument answerLabelsJson = loadJsonFromFile("answers.json", &err);
    if (err.error != QJsonParseError::NoError) showError(err);

    if (anyJsonError) {
      exit(1);
    }
    ...
```