import numpy

questions = {
    1: 'Is your character yellow?',
    2: 'Is your character bald?',
    3: 'Is your character a man?',
    4: 'Is your character short?',
}

characters = [
    {
      'name': 'Homer Simpson',
      'description': 'Homer Simpson - is ...',
      'answers': {1: 1, 2: 1, 3: 1, 4: 0}
    },
    {'name': 'SpongeBob SquarePants', 'answers': {1: 1, 2: 1, 3: 1, 4: 0.75}},
    {'name': 'Sandy Cheeks',          'answers': {1: 0, 2: 0, 3: 0}},
]

questions_so_far = []
answers_so_far = []


def character_answer(character, question):
    if question in character['answers']:
        return character['answers'][question]
    return 0.5

def calculate_character_probability(character, questions_so_far, answers_so_far):
    # Prior
    ph = 1 / len(characters)

    # Likelihood
    P_answers_given_character = 1
    P_answers_given_not_character = 1
    for question, answer in zip(questions_so_far, answers_so_far):
        P_answers_given_character *= 1 - abs(answer - character_answer(character, question))

        P_answer_not_character = np.mean([
          1 - abs(answer - character_answer(not_character, question))
          for not_character in characters
          if not_character['name'] != character['name']
        ])

        P_answers_given_not_character *= P_answer_not_character

    # Evidence
    P_answers = ph * P_answers_given_character + \
        (1 - ph) * P_answers_given_not_character

    # Bayes Theorem
    P_character_given_answers = (
        P_answers_given_character * ph) / P_answers

    return P_character_given_answers


def index():
  probabilities = calculate_probabilites(questions_so_far, answers_so_far)
  print("probabilities", probabilities)
  
  # Новое множество ключей вопросов
  questions_left = list(set(questions.keys()) - set(questions_so_far))

def calculate_probabilites(questions_so_far, answers_so_far):
    probabilities = []
    for character in characters:
        probabilities.append({
            'name': character['name'],
            'probability': calculate_character_probability(character, questions_so_far, answers_so_far)
        })

    return probabilities

def check_phs(phs):
  assert abs(sum(phs) - 1) < 0.000000001

ANSWER_UNKNOWN = 2
ANSWER_YES = 1
ANSWER_NO = 0

phs = [0.1, 0.3, 0.6]
check_phs(phs)



def do_answer(question_id: int, answer_type: int):
  if answer_type == ANSWER_YES:
    # change state of akinator's brain
    pass
  elif answer_type == ANSWER_NO:
    pass
  elif answer_type == ANSWER_UNKNOWN:
    pass
  else: 
    raise("Error")

