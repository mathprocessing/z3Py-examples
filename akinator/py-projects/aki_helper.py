import math

EPS = 0.001

def uniform(n):
  return [1/n for _ in range(n)]

def log2(x):
  if x == 0:
    return 1
  return math.log2(x)

def entropy(pd):
  return -sum(map(lambda p: p * log2(p), pd))

answers_str = ["yes", "no", "dontknow", "nomeaning"]

characters = [
  {
    "counter": 1,
    "name" :"Giraffe"
  }, 
  {
    "counter": 1,
    "name" :"Tiger"
  }, 
  {
    "counter": 1,
    "name" :"Robot"
  }
]

questions = [
  {
    "text": "Травоядное?",
    "characters": [
      {
        "characterId": 0,
        "answers": { "yes": 4, "no": 1, "dontknow": 0, "nomeaning": 0}
      },
      {
        "characterId": 1,
        "answers": { "yes": 1, "no": 9, "dontknow": 0, "nomeaning": 0}
      },
      {
        "characterId": 2,
        "answers": { "yes": 1, "no": 1, "dontknow": 0, "nomeaning": 18}
      }
    ]
  },
  {
    "text": "Блестит?",
    "characters": [
      {
        "characterId": 0,
        "answers": { "yes": 2, "no": 2, "dontknow": 5, "nomeaning": 1}
      },
      {
        "characterId": 1,
        "answers": { "yes": 2, "no": 2, "dontknow": 5, "nomeaning": 1}
      },
      {
        "characterId": 2,
        "answers": { "yes": 8, "no": 1, "dontknow": 1, "nomeaning": 0}
      }
    ]
  }
]

def normalize(lst: list):
  total = sum(lst)
  if total == 0:
    return [1 / len(lst) for i in range(len(lst))]
  return list(map(lambda x: x / total, lst))

def product(pd1, pd2):
  assert len(pd1) == len(pd2)
  pd_res = []
  for i, _ in enumerate(pd1):
    pd_res.append(pd1[i] * pd2[i])
  return pd_res

def valid_answer_type(answer_type):
  return answer_type in ["yes", "no", "dontknow", "nomeaning"]

def norm_answers(answers: dict):
  res = []
  for s in answers_str:
    res.append(answers[s])
  return normalize(res)

def q_to_table(questions):
  qs = questions.copy()
  for i, q in enumerate(questions):
    for j, character in enumerate(q["characters"]):
      answers = character["answers"]
      qs[i]["characters"][j]["answers"] = norm_answers(answers)
  return qs

def question_answer_to_pd(qs, question_id: int, answer_type: str):
  assert valid_answer_type(answer_type)
  ans_ind = answers_str.index(answer_type)
  res = []
  for ch in qs[question_id]['characters']:
    res.append(ch['answers'][ans_ind])
  return res

def product_all(qs, hyps, qa_lst):
  pd = hyps.copy()
  for qa in qa_lst:
    q_id, ans_type = qa
    pd = product(pd, question_answer_to_pd(qs, q_id, ans_type))
  return normalize(pd)

# First step
hyps = normalize(list(map(lambda ch: ch["counter"], characters)))
qs = q_to_table(questions)
print("hyps", hyps)

# Second step

final_pd = product_all(qs, hyps, [(0, "no"), (1, "yes")])
print(final_pd)

print(entropy(final_pd))
print(entropy([0.3, 0.7]))

"""
Используя простую формулу, которая отображает 
набор пар вопрос/ответ и некоторую гипотезу в вероятность, 
что при данных ответах на вопросы, нужно принимать именно эту гипотезу.
  (quAnswer, character) -> prob
  пересчитать для всех гипотез

Пересчитав эту вероятность для всех гипотез в нашей базе знаний после 
ответа на новый вопрос можно видеть, какие из них более вероятны в настоящий момент.
(Это называется распределение вероятности гипотез на текущий момент)

Для каждой гипотезы(character) есть максимальная траектория ответов.


Информационная энтропия - это характеристика распределения случайной величины равная
тому количеству информации, которое нужно получить чтобы устранить неопределенность.
для распределений:
[0.1, 0.9] -> E < 1,  
-(0.1 * log(0.9, 2) + 0.9 * log(0.9, 2))
E = 
[0.5, 0.5] -> E = 1

Следует выбирать вопрос, ответ на который сильнее уменьшит энтропию (кол-во инф. для устранения неопределенности).
т.е. нужно максимизировать E(step) - E(next_step)

Найти Мат. ожидание (суммируя по ответам) уменьшения энротропии для current_q_id и next_q_id
Eaverage = E[a1] * P(answer = a1) + E[a2] * P(answer = a2) ...

где
E(P(Hi | S)) = - (P(H1 | S) * log(P(H1 | S), 2) + P(H2 | S) * log(P(H2 | S), 2) + ...)

"""

def near_eq(x, y, eps=EPS):
  return x - eps/2 < y and x + eps/2 > y

def test_entropy():
  assert entropy([0.5, 0.5]) == 1.0
  assert entropy([0, 1]) == 0

  assert near_eq(entropy((lambda x: [x, 1-x])(0.45)), 0.993)

  assert near_eq(entropy((lambda x: [x, 1-x])(0.1)), 0.469)
  assert near_eq(entropy((lambda x: [x, 1-x])(0.0001)), 0.00147303, eps=1e-8)

  for i in range(0, 10):
    assert entropy(uniform(2**i)) == i


test_entropy()