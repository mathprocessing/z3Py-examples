# Provers
```json
[
  {
    "filename":"Example.pdf",
    "name": "Example name",
    "author": "Example author",
    "additional": "Some additional info for better recognition of book"
  }
]
```


# Other

## Graph theory
## TODO
Create some checker for resource properties: Images, books, big files.
* Just simple check that json is correct
* create `md` file from json
* Create Verifier interface
```py
# Warning: This is esquisse
v = Verifier()
v.verify(json_object)
model = create_json_model(types={
  Pair: ["int", "int"],
},
contraints={
  target=Pair: [Var("x"), Var("y")],
  hyps=...
  interface=(Interface_Generator(), Interface_constraints())
  # Interface_constraints() helps to (semi-)automatically (interactively) extend interface
})
v.load_json_model()
```
* Try to glue some interfaces like "Verifier can work not only with files, it can works with lambdas, some objects generated on the fly..."

