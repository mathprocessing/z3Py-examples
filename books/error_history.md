# Just idea about errors
```json
{
  "error_message": "ValueError: not enough values to unpack (expected 2, got 1)",
  "solution": "Change `def first(): for e in iterable: yield e; return` to `for e in iterable: return e`"
}
```
Why we can't have some tool to auto-store|resolve using z3| errors?
Hypothesis: In some cases it's possible to just eliminate some cases or group of actions.



