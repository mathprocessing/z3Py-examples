"""
# How to run?

in vscode:
use key `F6`

in termimal:
python3 books/check_book_list.py
"""
import json

FILE_NAMES = [
  'books/book_list.json'
]
MAX_STR_LEN = 200

def gen_files(filenames):
  for filename in filenames:
    with open(filename, 'r') as json_file:
      data = json.load(json_file)
      yield (filename, data)

def test_book_list_is_correct():
  """
  TODO: Create tool that Automatically copy/paste json to doctest like below
  Without it user|developer need to manually check that 
  `lambda contents: lambda body` of some `.json` equal to
  contents of `lambda contents: lambda body` of some python script.
  * Subgoal: parse `def func_name` to some "list of tests".

  TODO: Additional
  Think about why we need to use python to match contents of files?
  What is the best tool for this?
  What we need to solve some problems with contents like with mathematical
  objects? (Some tools can be eliminated by that WISH)
  Thinking forward, what a tool people will need in future?

  Example json of book
  {
    "filename":"bollobas-1998-modern-graph-theory-graduate-texts-in-mathematics-184-springer-1998.pdf",
    "name": "Modern Graph Theory",
    "author": "Bela Bollobas",
    "additional": "With 188 Figures",
    "maximally_interesting_page": 411,
    "number_of_pages": 411,
  }

  >>> json_filename, data = first(gen_files(FILE_NAMES))
  >>> json_filename
  'books/book_list.json'

  Get type of book file:
    for example: some 'Almost-boring-book.pdf'

  >>> book = data[0]
  >>> type(book.get('filename'))
  <class 'str'>

  We can check just type but...
    What if we can check more porperties 
      (with probabilities? No. with scores? May be yes, but why real numbers?)
      TODO: Learn about orders, preorders, some things like that can help to
      solve rating|scoring stuff.

  Fact: filename can't have length < 3 because it contains extension.

  >>> str_constraint = lambda s: len(s) in range(3, MAX_STR_LEN)
  >>> int_constraint = lambda i: i > 0
  >>> book_type_model = [ \
        (       "filename", OptionalType(str), str_constraint),
  ...   (           "name",               str, str_constraint),
  ...   (         "author",               str, str_constraint),
  ...   ("number_of_pages", OptionalType(int), int_constraint),
  ... ]

  Idea of types checking with doctest: (really strange but interesting)

  Interface (test ofr types):
  runintypesfield> check_model(model)
  Test:
  > check()
  """

class OptionalType:
  """
  see: https://wiki.haskell.org/Maybe
  What is optinal type:
  Haskell: Maybe a
    can have two cases:
  ```haskell
  data Maybe a = Just a | Nothing
      deriving (Eq, Ord)
  ```

  see: https://doc.rust-lang.org/rust-by-example/std/option.html
  Rust:
  ```pseudocode
    Optional<T> = None | Some(T)
  ```
  >>> opt = OptionalType(str); opt
  Optional(str)

  Interesting: all ~ forall
  Reflection on line above:
    What if in general "find a link|equivalence relation" is not easy problem.
    Because we have wround so many objects, and try to glue all combinations
      of it.
      Or not all combinations...
      > get_my_mind_map()
      Strategy order: [bruteforce, strategy1, strategy2, xor_logic, analogies, ...]
      
  >>> assert all([opt.check(None), opt.check("hello")])
  """
  def __init__(self, base_type):
    self.base_type = base_type
    
  def __repr__(self):
    return f'Optional({str(self.base_type.__name__)})'

  def check(self, obj) -> bool:
    if obj is not None:
      return type(obj) == self.base_type
    return True

def first(iterable):
  """
  >>> first('hello')
  'h'
  >>> first([1,2,3])
  1
  >>> first([])
  """
  for e in iterable:
    return e
  else:
    return None # can be deleted

if __name__ == '__main__':
  import doctest
  flags = {
    'optionflags': doctest.FAIL_FAST | doctest.ELLIPSIS
  }
  doctest.testmod(**flags)
