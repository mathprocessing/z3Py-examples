"""
Name of video: New Wikipedia sized proof explained with a puzzle
Link to video: https://www.youtube.com/watch?v=pFHsrCNtJu4

Main vector|direction of project:
Show process of finding solutions to task.
With comments, generated automatically.
Create interface in that novices can check
logic of yourselves, test new (in some predefined set) ideas.

Plan:
° Do it without z3
° Do it using z3

s = abs(1 + (-1) + 1 + 1 + ...)
        ^^^^^^^^^^^^^^^^^^^^^^
        arbitrary sequence of ones and minus ones.
Constraint:
  for any integer step > 0 and n <= maxn:
    abs(sum(f(i) in range(0, n, step))) <= value

H_symmetry: we can set a[0] to +1

s <= 1:
  +-
  +-+ rejected by each 2 [+-+]
                          ^ ^ => s = 2 > 1
                           
  +--- rej by s = 2
  1012
  
  bfs
  {
    +--++ rej from each 2 +-+ rej by s = 2
    10101
    +--+- 2th +-- 3th ++ ==> we can't reject this because we not rejected sibling case. This follows from Hyp: "all actions must be equally distributed in all directions." (might be wrong, if you think precisely)
    10101
  }
  
  [User's brain debug begin]
  I forget to check step = 3:
    Possible next options:
      * [OPT 1] Redo same calculations because we need shortest proof
      * [OPT 2] "You have a proof... what you need more asshole?"
        => do not redo caculations,
           just move forward [SELECTED OPT 2]
  
  if [SELECTED OPT 1]:
    bad thing: in that branch we must delete
      some abstract tricks like "bfs{}"
  else
     assume for the rest that we selected OPT 2
  [User debug end]
  
  main = [SELECTED OPT 1]

-----------------------------------------
Examples of solution search:

>>> glob = dict()
>>> glob['MAXSUM'] = 0

TODO: refactor this hell!
Target is help to user create beatiful proofs without backtracking (using beatiful tactics, and beatiful ideas..)
>>> next_assume(glob)
assume s <= 1:
  
  >>> p = Path(glob['MAXSUM'], '+')
  >>> pplist(p.solve(2))
  1) +-
  
  >>> p.check(), p.set('++').check()
  (True, False)

  >>> p.pp
  ++ rejected by step=1

    > p.pp
    ++ rejected by step=1, pos=2
     ^

  >>> p.set('+-').pp
  +- accepted

  # Possible short syntax:
  # > p = '++'
  # 
  # Syntax using p.set(str)
  # > p.set('+-+').pp
  # +-+ rejected by step=2 <= 1
  # 
  # Also possible pretty print:
  # +-+ rejected by each 2 [+-+]
  #                         ^ ^ => s = 2 > 1
  # >>> p.set('+---').pp
  # +--- rej by s = 2 <= 1
  # 1012

#   >>> p.set('+--+')
#   +--+
#   1010
#   1.0.
#   1..1 rej by s = 2 <= 1

# >>> next_assume()
# assume s <= 2:

#   >>> problem.next(skip=True, step=1)
#   +++ rej by s = 3 (marked as skipped)
#   123
  
#   ++--
#   1210
#   1.0.
#   1..0
  
"""

# This "constant" dictionary used only in `path.__str__()`
symb = dict()
symb[-1] = '-'
symb[1] = '+'

class Path(list):
  def __init__(self, maxsum, seq=None, phase_from_step=None):
    super().__init__()
    # Context of assumptions
    # ----------------------
    self.maxsum = maxsum
    if phase_from_step is None:
      self.phase_from_step = lambda step: step - 1
    else:
      self.phase_from_step = phase_from_step
    
    check_type(self.maxsum, int)
    assert type(self.phase_from_step).__name__ == 'function'
    # ----------------------
    if seq is not None:
      assert type(seq) == str
      self.set(seq)

  @property
  def pp(self):
    result, contr = self.check(get_contradiction=True)
    if result:
      print(f'{self} accepted')
    else:
      print(f'{self} rejected by {contr[5:]}')

  def copy(self):
    """
    >>> p = Path(1, '+-+')
    >>> u = p.copy()
    >>> u, p
    (Path(len=3), Path(len=3))
    >>> u[0] = -1
    >>> print(p)
    +-+
    >>> print(u)
    --+
    """
    c = Path(self.maxsum, phase_from_step=self.phase_from_step)
    assert len(c) == 0
    for i, elem in enumerate(self):
      c.append(elem)
    return c
    
  def __repr__(self):
    return f'Path(len={len(self)})'
    
  def __str__(self):
    return ''.join(symb[i] for i in self)
  
  def set(self, seq: str, return_self=True):
    self.clear()
    for ch in seq:
      assert ch in '+-' and len(ch) == 1
      if ch == '+':
        self.append(1)
      else:
        self.append(-1)
    if return_self:
      return self
    else:
      return None
    
  def sum(self, step=1, phase=0):
    assert step > 0
    assert phase in range(0, step)
    return sum(self[i] for i in range(phase, len(self), step))
  
  def check_sum_bound(self, max_bound, step=1, phase=0) -> bool:
    assert step > 0
    assert phase in range(0, step)
    acc = 0
    for i in range(phase, len(self), step):
      acc += self[i]
      if abs(acc) > max_bound:
        return False
    return True

  def check(self, get_contradiction=False):
    """
    s = abs(a0 + a1 + a2 + ...)
      where ai in {-1, +1}

    # >>> q = Path(1, '---')
    # >>> q.check()
    # False
    # >>> assert q.set('+').check()
    # >>> assert q.set('+-').check()
    # >>> assert q.set('-+').check()
    # >>> assert not q.set('-+-').check()
    # >>> assert not q.set('+-+').check()
    # >>> q.maxsum = 2
    # >>> assert not q.set('+++').check()
    # >>> assert q.set('++').check()
    # >>> q.maxsum = 5
    # >>> assert q.set('+++++').check()
    # >>> assert not q.set('------').check()

    # Test contradictions:
    # >>> q.maxsum = 1
    # >>> q.set('--').check(True)
    # (False, 'F by step=1')

    # >>> q.set('-+-').check(True)
    # (False, 'F by step=2')

    # >>> q.set('-++-').check(True)
    # (False, 'F by step=3')

    # >>> q.maxsum = 2
    # >>> q.set('+++---').check(True)
    # (False, 'F by step=1')

    # >>> q.set('--++--++--++').check(True)
    # (False, 'F by step=4')

    # >>> q.set('--++--++--+++++').check(True)
    # (False, 'F by step=1')
    """
    s = self.maxsum
    assert s >= 0
    if get_contradiction:
      for step in range(1, len(self)):
        if not self.check_sum_bound(self.maxsum, step, phase=self.phase_from_step(step)):
          return False, f'F by step={step}'
      return True, 'no contradiction'
    else:
      # We can't use this code because we need to check cases like '+++|---'
      #                                                                ^ in this step we already have contradiction
      # all(abs(self.sum(step)) <= s for step in range(1, len(self)))
      for step in range(1, len(self)):
        if not self.check_sum_bound(self.maxsum, step, phase=self.phase_from_step(step)):
          return False
      return True

  def solve(self, max_depth, return_contradictions=False, depth=None):
    """
    Let's solve puzzle from the video:

    >>> initial_instructions = '+'
    >>> maximum_absolute_value_of_sum = 1
    >>> phase_lambda = lambda step: step-1

    * Remember James Grime (in the video) assumed that first instruction is `+1`
    * Maximum possible absolute value of sum is `1`
    * We count instructions like that:
      >>> print_scheme_offset_counting(step_in=range(1, 4), example_path='+--+-++-', phase_from_step=phase_lambda)
      [+--+-++-]
       ^^^^^^^^  when step = 1
       01234567
      #
      [+--+-++-]
        ^ ^ ^ ^  when step = 2
        1 3 5 7
      #
      [+--+-++-]
         ^  ^    when step = 3
         2  5

      i.e. `phase` is just leftmost index: (TODO: It may be better if it's renamed to `offset`)
        for step = 1: phase = 0
        for step = 2: phase = 1
        for step = 3: phase = 2
        ...
        for step = N: phase = N - 1

    >>> p = Path(maxsum=maximum_absolute_value_of_sum, seq=initial_instructions, phase_from_step=phase_lambda)

    >>> def show_solutions_incrementally(path, rng):
    ...   for i in rng:
    ...     print(str(i) + ') ', end='')
    ...     pplist(p.solve(i), oneline=True)

    >>> show_solutions_incrementally(p, range(1, 13))
    1) [+]
    2) [+-]
    3) [+--, +-+]
    4) [+--+]
    5) [+--+-, +--++]
    6) [+--+-+]
    7) [+--+-+-, +--+-++]
    8) [+--+-++-]
    9) [+--+-++--, +--+-++-+]
    10) [+--+-++--+]
    11) [+--+-++--+-, +--+-++--++]
    12) []

    Old tests for phase=0:

    How it counts?
      >>> print_scheme_offset_counting(step_in=list(range(1, 4)) + [7, 8], example_path='+--+-++-', phase_from_step=lambda _: 0)
      [+--+-++-]
       ^^^^^^^^  when step = 1
       01234567
      #
      [+--+-++-]
       ^ ^ ^ ^   when step = 2
       0 2 4 6
      #
      [+--+-++-]
       ^  ^  ^   when step = 3
       0  3  6
      #
      [+--+-++-]
       ^      ^  when step = 7
       0      7
      #
      [+--+-++-]
       ^         when step = 8
       0

    >>> p = Path(1, '', phase_from_step=lambda _: 0)
    >>> assert p.set('+-').check()
    >>> assert p.set('-+').check()
    >>> p.set('')
    Path(len=0)

    >>> pplist(p.solve(1), oneline=True)
    [-, +]

    >>> pplist(p.solve(2), oneline=True)
    [-+, +-]

    >>> pplist(p.solve(3), oneline=True)
    [-++, +--]

    >>> assert all(sol.check() for sol in p.solve(2))

    >>> pplist(p.solve(4), oneline=True)
    []

    >>> pplist(p.solve(1, return_contradictions=True)[1])
    Empty

    >>> pplist(p.solve(2, return_contradictions=True)[1])
    1) --| F by step=1
    2) ++| F by step=1

    >>> pplist(p.solve(3, return_contradictions=True)[1])
    1) --| F by step=1
    2) -+-| F by step=2
    3) +-+| F by step=2
    4) ++| F by step=1

    We use symmetry of '+' and '-'
    >>> p.set('+')
    Path(len=1)

    >>> pplist(p.solve(1, return_contradictions=True)[1])
    Empty

    >>> pplist(p.solve(2, return_contradictions=True)[1])
    1) ++| F by step=1

    >>> pplist(p.solve(3, return_contradictions=True)[1])
    1) +-+| F by step=2
    2) ++| F by step=1

    >>> p.set('++')
    Path(len=2)

    >>> any_number = 5

    Check that '++...' have no solutions
    >>> assert all(len(p.solve(i)) == 0 for i in range(2, any_number + 1))

    >>> assert p.maxsum == 1
    >>> p.maxsum = 2

    Let's check our pattern (that already we see in the video)
    >>> p.set('+--+-++-', False)

    No solution?
    >>> pplist(p.solve(8), oneline=True)
    []

    >>> upper_bound = 0

    Let's make it shorter, but how much?
    >>> for i in reversed(range(1, 8)):
    ...   _ = p.pop() # `_ = ..` Helps to hide returned values
    ...   if len(p.solve(len(p))) > 0:
    ...     upper_bound = i
    ...     print(p, upper_bound)
    ...     break
    +--+-+ 6

    >>> p.set_length(upper_bound)
    >>> print(p)
    +--+-+

    This functional can be useful to simplify `range(len(p), len(p) + 2)`:
    > range(1, 42) + 2
    range(3, 44)

    Ok, it works. Let's check more details:
    >>> show_solutions_incrementally(p, range(len(p), len(p) + 7))
    6) [+--+-+]
    7) [+--+-+-]
    8) [+--+-+--, +--+-+-+]
    9) [+--+-+--+, +--+-+-++]
    10) [+--+-+--+-, +--+-+--++, +--+-+-++-, +--+-+-+++]
    11) [+--+-+--++-, +--+-+-++--, +--+-+-+++-]
    12) [+--+-+--++--, +--+-+--++-+, +--+-+-++---, +--+-+-++--+, +--+-+-+++--, +--+-+-+++-+]

    We can see that [+--+-++] not exists (or check it right now):
    TODO: implement that thing
    > path_not_in_solutions('+--+-++', p, range(len(p), len(p) + 7))
    True

      Also long sequence can be "squashed":
      > show_solutions_incrementally(p, [12], squash_if_possible=True)
      12) [+--+-+--++--, 
                ..-++-+, 
                ..++---,
                ..++--+,
                ..+++--,
                ..+++-+]
    """
    if depth is None:
      depth = len(self)
    check_type(max_depth, int)
    check_type(depth, int)
    check_type(return_contradictions, bool)
    copied_path = self.copy()
    return copied_path.mut_solve(max_depth, depth, return_contradictions)

  def set_length(self, target_length):
    assert target_length in range(0, len(self) + 1), "Not implemented"
    while len(self) > target_length:
      self.pop()

  def mut_solve(self, max_depth, depth, return_contradictions=False):
    # store variable inside instance
    self.max_depth = max_depth
    self.return_contradictions = return_contradictions
    if return_contradictions:
      self.contradictions = []
    self.solutions = []
    self.mut_solve_aux(depth)
    if return_contradictions:
      return self.solutions, self.contradictions
    else:
      return self.solutions

  def mut_solve_aux(self, depth) -> bool:
    assert depth <= self.max_depth
    if self.return_contradictions:
      checked, contradiction = self.check(get_contradiction=True)
    else:
      checked = self.check()
    if not checked:
      if self.return_contradictions:
        self.contradictions.append(str(self) + '| ' + contradiction)
      return False
    # Checked

    if depth == self.max_depth:
      self.solutions.append(self.copy())
      return True

    # push value
    # self.rec(state, depth + 1)
    # pop value

    self.append(-1)
    self.mut_solve_aux(depth + 1)
    # self.pop()
    # self.append(1)
    # <=>
    self[depth] = 1
    self.mut_solve_aux(depth + 1)
    self.pop() # I forget this
    return False

def check_type(variable, expected_type):
  assert type(variable) == expected_type, f"Wrong type, expected {expected_type}"

def pplist(xs, oneline=False):
  if oneline:
    s = '['
    for i, x in enumerate(xs):
      if i > 0:
        s += ', '
      s += str(x)
    s += ']'
    print(s)
    return
  printed_something = False
  for i, x in enumerate(xs):
    printed_something = True
    print(f'{i+1}) {str(x)}')
  if not printed_something:
    print('Empty')

  def to_binary(self):
    """
    TODO:
    > p = Path('++--+')
    > p.to_binary()
    [1, 1, 0, 0, 1]
    """
    raise Exception("Not implemented")


def next_assume(glob):
  glob['MAXSUM'] += 1
  print(f"assume s <= {glob['MAXSUM']}:")

def test_path():
  """
  >>> p = Path(2, '++++--')
  >>> p[0], p[-1]
  (1, -1)
  
  >>> p
  Path(len=6)
  
  >>> print(p)
  ++++--
  
  >>> p.sum(step=1, phase=0)
  2
  >>> p.sum(step=2, phase=0)
  1
  >>> p.sum(step=2, phase=1)
  1
  >>> p.sum(step=3, phase=2)
  0
  >>> p.sum(step=3, phase=0)
  2
  """

def _cliffs(n: int, step: int, phase_lambda):
  """
  >>> _cliffs(n=3, step=1, phase_lambda=lambda _: 1)
  ' ^^'

  >>> _cliffs(n=8, step=2, phase_lambda=lambda step: step - 1)
  ' ^ ^ ^ ^'

  >>> _cliffs(n=8, step=3, phase_lambda=lambda step: step - 1)
  '  ^  ^  '

  >>> _cliffs(n=7, step=3, phase_lambda=lambda _: 0)
  '^  ^  ^'
  """
  assert is_function(phase_lambda), "step_from_phase must be a lambda function"
  assert step > 0
  return ''.join('^' if b else ' ' for b in (i in range(phase_lambda(step), n, step) for i in range(n)))

def _numbers(n: int, step: int, phase_lambda):
  """
  >>> _numbers(n=8, step=3, phase_lambda=lambda step: step-1)
  '  2  5  '
  """
  assert is_function(phase_lambda), "step_from_phase must be a lambda function"
  return ''.join(str(j) if b else ' ' for j, b in enumerate(i in range(phase_lambda(step), n, step) for i in range(n)))

def print_scheme_offset_counting(step_in, example_path: Path, phase_from_step):
  """
  >>> print_scheme_offset_counting(step_in=range(1, 4), example_path='+--+-++-', phase_from_step=lambda step: step - 1)
  [+--+-++-]
   ^^^^^^^^  when step = 1
   01234567
  #
  [+--+-++-]
    ^ ^ ^ ^  when step = 2
    1 3 5 7
  #
  [+--+-++-]
     ^  ^    when step = 3
     2  5
  """
  assert len(example_path) <= 10, "Too long example"
  for v, step in enumerate(step_in):
    if v > 0:
      print('#')
    assert step > 0
    print(f'[{example_path}]')
    args = (len(example_path), step, phase_from_step)
    print(' ' + _cliffs(*args) + 2*' '+ f'when step = {step}')
    print(' ' + _numbers(*args).rstrip())
    

def is_function(f):
  return type(f).__name__ == 'function'

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)

