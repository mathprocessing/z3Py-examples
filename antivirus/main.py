"""
--------------------------------------------------------------
Optimality goals feature:
  # >>> n.get_optimal_move()
  # {L,R,U,D}
  # >>> n.assume_optimal_move(0, U)
  # >>> n.proof_goals()
  # {Move(0, L), Move(0, R), Move(0, D), ..}
  
  # >>> n.mut_relax_goals()
  
  # returns possible unoptimal moves that we must prove in future
  # >>> n.proof_goals()
  # {Move(0, R), Move(1, L), Move(2, L)}


Possible human mistakes:
  * Interface not full (when we have push we may forget about pop ::generalize->:: when we have function we may forget about it's inverse)
    + Example: see `plus_minus.py::Path.solve` -> then think about {push, pop}
    + How to prevent?
      @ use BFS when you build/solve interfaces
  * Thinking about some A <==> to B, but actually truth: A not <==> to B for all states 
    + Example: see comments in `Node.can_move` body
    + How to prevent?
      @ focus on more substates
      @ use solver because in general number of equivalence classes can be very big.
      {Comments, Ideas}: We don't know where to split, i.e. we need a vector pointing (pointer) to the place where to split
      Distant (Blurred) idea: N-dimentional discrete sphere (like [2d: triangle, 3d: tetrahedron, ...]) + Information sets (GTO) can help
      to create/find in web "formalization of equivalances" or some interesting problems related to it.
  * Assumption that order of some actions don't matters (commutativity assumption)
--------------------------------------------------------------

OST: Star Cruiser - {Desert Zone, Space Flight}
Snoop Dogg Feat. The Doors - Riders On The Storm (Fredwreck Remix)
Christopher Lawrence - Rush Hour
Sin - Hard EBM
Command and Conquer: Red Alert - Dense
Command & Conquer 3: Kane's Wrath - Mechanical Mind
Command & Conquer 3: Kane's Wrath - Act On Instinct
Tiberian Sun Soundtrack - Killing Machine
Indra - Games Of Psy
Indra - Is't Good Again
Terminator 2: Judgment Day (Suite) [<= 0:30] ++ [>= 2:27 and <= _] ++ [from 8:25 to 9:52] or TimeRange(10:02, 13:16, time_format='minsec')
                                                                                                     version_before: TIME_FORMAT::(min, sec)
                                                        version_after(of version_before keyword): branch(state, -1): TIM...
  Theorem: If exists 'before' than exists 'after'.
  - Question: What if `If exists 'after' than exists 'before'.` don't have proof? I.e. maybe True, maybe False
  - Question2

  ++ [21:00, 22:37]
  @Mario Ortega: At 26:08, every grown up man becomes a weeping grown up man. 
  Link: https://www.youtube.com/watch?v=GJvhE0jJ41w

  Main Theme Judgement Day Remastered by Gilles Nuytens: https://www.youtube.com/watch?v=ZpfAmY11-BU

  Brad Fiedel - The Terminator Theme [Extended by Gilles Nuytens] NEW EDIT

### ###
##22 ##
# 21  #
  1 3# 
#  303#
##  0##
### ###

### ###
##22 ##
# 21  #
  1  # 
#  3  #
##303##
###0###

>>> a = np.zeros(shape=(2,3), dtype=int); a
array([[0, 0, 0],
       [0, 0, 0]])

>>> a[1]=5; a
array([[0, 0, 0],
       [5, 5, 5]])

>>> a[:,2]=7; a
array([[0, 0, 7],
       [5, 5, 7]])

>>> np.ndarray(shape=(2,1), dtype=int, buffer=np.array([1,2]))
array([[1],
       [2]])

>>> n = Node(3, 2)
>>> n.m
array([[-2, -2, -2],
       [-2, -2, -2]])
       
>>> n[0, 1] = -1
>>> n[1] = [0,1,2]
>>> n.pp
-#-
012

>>> n[0, 1], n[1]
(-1, array([0, 1, 2]))

>>> n = Node(7, 7)

todo: in from_str write assert that ensures:
  set of all figure numbers == range(n)
  
>>> n.from_str('###0###|##-0-##|#1----#|--1-#--|#---2-#|##-22##|###-###').pp
###0###
##-0-##
#1----#
--1-#--
#---2-#
##-22##
###-###

>>> assert n.is_win()
>>> [n.fig_pos(i) for i in range(n.figures_counter)]
[(0, 3), (2, 1), (4, 4)]

>>> assert n.fig_pos(3) is None
>>> assert list(dirs()) == [L, R, U, D]
>>> [n.can_move(0, d) for d in dirs()]
[False, False, False, True]

>>> [n.can_move(1, d) for d in dirs()]
[False, True, False, True]

>>> [n.can_move(2, d) for d in dirs()]
[True, False, False, False]

>>> n.mut_move(0, D); n.mut_move(1, R)
>>> n.pp
###-###
##-0-##
#-10--#
---1#--
#---2-#
##-22##
###-###

>>> assert not n.is_win()
>>> {key: n.can_move(1, d) for key, d in DIRS_DICT.items()}
{'L': True, 'R': False, 'U': False, 'D': True}

From that case we can conclude:
  We must implement simultanious moving of two figures to ensure that our
  `Node` have right behavour.

  i.e. we can't do that move step by step (2 steps). Only as one step.

>>> n = Node(7, 5)

>>> n.from_str('''\
#-----#\
---1---\
#-101-#\
##-0-##\
###-###\
''').no

>>> n.mut_move(0, R)
>>> n.pp
#-----#
----1--
#--101#
##--0##
###-###

>>> {key: n.can_move(0, d) for key, d in DIRS_DICT.items()}
{'L': True, 'R': False, 'U': True, 'D': False}

>>> {key: n.can_move(1, d) for key, d in DIRS_DICT.items()}
{'L': True, 'R': False, 'U': True, 'D': False}

>>> n.mut_move(1, L)
#-----#
---1---
#-101-#
##-0-##
###-###

>>> {key: n.can_move(0, d) for key, d in DIRS_DICT.items()}
{'L': True, 'R': True, 'U': True, 'D': True}

>>> {key: n.can_move(1, d) for key, d in DIRS_DICT.items()}
{'L': True, 'R': True, 'U': True, 'D': True}

Lemma one_imp_two: if it movable by 1 step ==> it movable by 2 steps.
Proof := by using compositions of functions.
Tests are equivalent to equivalent to partial proof of some Lemma.

Other test case, let's move ahead and think about it's properties:
* must be     movable by 2 steps but not movable by 1 step [?]
* must be not movable by 2 steps but not movable by 1 step [?]
* must be not movable by 2 steps but     movable by 1 step
  [Can help partially prove lemma one_imp_two]
* must be     movable by 2 steps but     movable by 1 step [?]

If lemma `one_imp_two` is true: 4 variants can be reduced to:

* must be not movable by 2 steps but movable by 1 step

Example movable by two steps: [Move(0, L), Move(1, L)]
        and of course by one: [Move(1, L)]
>>> n.from_str('''\
#--0- #\
---01--\
#--1#-#\
''')

Result:
  >>> n2 = n.moves([(0, L), (1, L)]); n2.pp
  #-0-- #
  --01---
  #-1-#-#

  >>> n3 = n.move(1, L)
  >>> assert n3 == n2

Example movable only by one step: [Move(0, L)]
>>> n.from_str('''\
#--1#-#\
--101--\
#--1#-#\
''')

Result:
  >>> n.move(1, L).pp
  #-1-#-#
  -101---
  #-1-#-#

  >>> list(n.possible_moves())
  [(0, L)]

>>> {key: n.can_move(0, d) for key, d in DIRS_DICT.items()}
{'L': False, 'R': False, 'U': False, 'D': False}

>>> {key: n.can_move(1, d) for key, d in DIRS_DICT.items()}
{'L': False, 'R': False, 'U': False, 'D': False}

"""
import numpy as np

L, R, U, D = (0, -1), (0, 1), (-1, 0), (1, 0)
def dirs(): yield L; yield R; yield U; yield D
DIRS_DICT = {'L': L, 'R': R, 'U': U, 'D': D}
EMPTY_CELL, WALL_CELL = -2, -1
symb = {EMPTY_CELL: '-', WALL_CELL: '#'}
CHAR_SET = '-#0123456789'
assert len(set(CHAR_SET) & set('\n\t\r')) == 0

class Node:
  def __init__(self, w, h):
    self.w, self.h = w, h
    self.reset()

  def reset(self):
    self.figures_counter = 0
    self.m = np.zeros(shape=(self.h, self.w), dtype=int)
    self.m[:,:] = -2

  def copy(self):
    pass

  def figure_cells(self, figure_id):
    return (position for position in self.cells() if self[position] == figure_id)

  def possible_moves(self) -> "Iterator<Move>":
    """ Returns generator object """
    pass


  def in_bounds(self, position: (int, int)) -> bool:
    i, j = position
    return i in range(self.h) and j in range(self.w)

  def can_move(self, figure_id: int, direction: (int, int)) -> bool:
    """
    U   
    1UU 1R   L1   1
     11  11R  L11 D11
                   DD
    """
    # figure_cells_set = set(self.figure_cells(figure_id))
    for position in self.figure_cells(figure_id):
      new_pos = add_positions(position, direction)
      if not self.in_bounds(new_pos):
        return False
      # print(position, new_pos, to_char(self[new_pos]), self[new_pos] == figure_id)                                         ^^^^^^^^^^^^^ version_before: new_pos
      cell_value = self[new_pos] # Digging to deep (bad because it takes time) happens because: I assumed self[new_pos] <==> new_pos, but it's wrong, but why i assumed this?
      # Possible answers:
      # * Maybe some preassumptions, preactions are wrong
      # * i'm bad developer and just stupid, slow response
      # * selected bad way to do something at the beginning, beginning of what?
      if cell_value not in {EMPTY_CELL, figure_id}:
        return False
    return True

  def mut_move(self, figure_id: int, direction: (int, int)):
    assert self.can_move(figure_id, direction)
    cells = set(self.figure_cells(figure_id))
    new_cells = set()
    for pos in cells:
      assert self[pos] == figure_id
      new_pos = add_positions(pos, direction)
      if not self.in_bounds(new_pos):
        continue
      new_cells.add(new_pos) # Because `order of setting values matters`
      # self[new_pos], self[pos] = figure_id, EMPTY_CELL
      self[pos] = EMPTY_CELL
    for new_pos in new_cells:
      self[new_pos] = figure_id

  def move(self, figure_id: int, direction: (int, int)):
    node_copy = self.copy()
    node_copy.mut_move(figure_id, direction)
    return node_copy


  def fig_pos(self, figure_id: int) -> "Option<(int, int)>":
    """ Get position (i, j) of top left cell of figure or first cell that we see when we enumerate matrix cells in order: (left to right, up to down).
            row index-^  ^-column index
    
    Invariants (always true regardless of methods calling):
      assert i in range(self.w)
      assert j in range(self.h)

    ij0123
    0 ####
    1    1
    2   11
    3 ####
    assert fig_pos(1) == (1, 3)
    """
    for i, j in self.cells():
      if self[i, j] == figure_id:
        return i, j
    return None

  def is_win(self) -> bool:
    return self[0, self.w // 2] == 0

  def cells(self) -> "Iterator<(int, int)>":
    for i in range(self.h):
      for j in range(self.w):
        yield i, j

  @property
  def no(self):
    """ Disable output during doctest execution """
    return None
  
  @property
  def pp(self):
    print(self.__str__())

  def __str__(self):
    s = ''
    for i in range(self.h):
      if i > 0:
        s += '\n'
      for j in range(self.w):
        cell_value = self.m[i, j]
        s += to_char(cell_value)
    return s

  def __repr__(self):
    return f'Node(w={self.w}, h={self.h}, figs={self.figures_counter})'

  def __setitem__(self, ind, value):
    if type(ind) == tuple:
      i, j = ind
      self.m[i, j] = value
    elif type(ind) == int:
      self.m[ind] = value
    else:
      assert False
      
  def __getitem__(self, ind):
    if type(ind) == tuple:
      i, j = ind
      return self.m[i, j]
    elif type(ind) == int:
      return self.m[ind]
    else:
      assert False

  def from_str(self, s, delim='|'):
    assert len(delim) == 1, f"Delimeter is just single char, but it have length={len(delim)}"
    delimeter_set = delim + '\r\t\n'
    assert delim not in CHAR_SET, f"Delimeter intersects with CHAR_SET: {CHAR_SET}"
    figures_set = set()
    i, j = 0, 0
    for ch in s:
      assert i in range(0, self.h)
      assert j in range(0, self.w)
      if ch in delimeter_set:
        continue
      ch_index = CHAR_SET.index(ch)
      assert ch_index >= 0, f"char: {ch} not in CHAR_SET: {CHAR_SET}"
      cell_value = ch_index - 2
      self[i, j] = cell_value
      if cell_value >= 0:
        # Add figure to set
        figures_set.add(cell_value)
      j += 1
      if j == self.w:
        i, j = i+1, 0
    if not self.correct_figures(figures_set):
      # Undo setting values (But not we do reset because it simpler)
      self.reset()
      assert False, "Please change numbers of figures in input string to arithmetic progression [0, 1, 2, ..]"
    self.figures_counter = len(figures_set)
    return self

  def correct_figures(self, figures_set):
    return len(figures_set) == 0 or max(figures_set) == len(figures_set) - 1


def add_positions(p1, p2):
  return p1[0] + p2[0], p1[1] + p2[1]

def to_char(cell_value: "-2..=9"):
  if cell_value < 0:
    assert cell_value >= -2
    return symb[cell_value]
  else:
    assert cell_value <= 9
    return str(cell_value)

def mrange(vi, vj):
  for i in range(vi):
    for j in range(vj):
      yield i, j

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
    
  