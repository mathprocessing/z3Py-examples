# About
You in the main directory.

In this dir you can find some examples of using z3Py.

# Test
```bash
# Warning: don't use pytest in this dir. (TODO: Can pytest view z3?)

# Example of testing one file:
python3 -m doctest -v simplify.py

# Without output
python3 simplify.py
```