from z3 import *
from z3.z3util import get_models, model_str
from tutorial_2 import prove, prove_all

def pp_models(ms):
  if ms == False:
    print("no solutions")
    return
  for i, m in enumerate(ms):
    print(f'{i})')
    print(model_str(m))
    print('-'*42)

def still_life_rule(central_cell: "0|1|2", sum_around_low: int, sum_around_high: int) -> int:
  """Rule that can work with 3-state cells: {0, 1, ?}
  """
  assert sum_around_low <= sum_around_high
  assert central_cell in range(3)
  false, true, unknown = range(3)
  # false means local distinct solutions/models = 0
  # true means local distinct solutions/models = 1
  # unknown means local distinct solutions/models > 1
  # local means `in set of cells in square 3x3`

  if sum_around_high == 2:
    # assert sum_around_low <= 2
    # assert sum_around == 2
    return true
  if sum_around_low == 3:
    if central_cell == false:
      return false
    if central_cell == true:
      return true
    if central_cell == unknown:
      # In that case we can assume that central_cell == false
      # and realize contradiction
      # we have only 1 local solution
      return true

  if sum_around_low > 3 or sum_around_high < 2:
    if central_cell == false:
      return true
    if central_cell == true:
      return false
    if central_cell == unknown:
      # return {true, false}
      return unknown


"""
  assert sum_around_low <= sum_around_high

  if sum_around_high == 2:
  if sum_around_low == 3:
  if sum_around_low > 3 or sum_around_high < 2:
"""


# sum_around_low
slow = Int('slow')

# sum_around_high
shigh = Int('shigh')

assumptions = [
  slow <= shigh,
  And(slow >= 0, slow <= 8),
  And(shigh >= 0, shigh <= 8),
]

goal = Or(
  shigh == 2,
  slow == 3,
  slow > 3,
  shigh < 2,
  And(shigh >= 3, slow <= 2)
)


ms = get_models(And(And(*assumptions), Not(goal)), k=10)
pp_models(ms)


# prove(goal, And(*assumptions))

